import React,{Component} from 'react'
import { Router, Route,} from "react-router-dom";
import history from './history'
import AppContainer from '../containers/appContainer'
// import CreateAccount from '../component/registration/createAccount/createAccount'
// import SignInAccount from '../component/registration/signIn/signInAccount'
// import ResetPassword from '../component/registration/resetPassword/resetPassword'



class AppRouters extends Component {
    render(){
        return(
            <div>
            <Router history = {history}>
                <Route
                    path = '/' 
                    component= {AppContainer} 
                /> 
            </Router>
            </div>
        )
    }
}
export default AppRouters;