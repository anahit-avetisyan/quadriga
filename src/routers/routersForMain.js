import React,{Component,Fragment} from 'react'
import { Router, Route} from "react-router-dom";
import history from './history'
import ProductsContainer from '../components/main/product/productsContainer'
import UserProfileMain  from '../components/main/user/userProfileMain'
import UserProfileEdit from '../components/main/user/userProfileEdit'
import About from '../components/main/about/about'
import Contact from '../components/main/contact/contact'
import TermOfUse  from '../components/main/termOfUse/termOfUse'
import PrivacyPolicy from '../components/main/privacyPolicy/privecyPolicy'
import CartContainer from '../components/main/cart/cartContainer'
import CastomizedProductContainer from '../components/main/customizedOrders/customizedProductContainer'
import OrderReviewContainer from '../components/main/orderReview/orderReviewContainer'
import OpenOrdersContainer from '../components/main/openOrders/openOrdersContainer'
import OrdetDetailsContainer from '../components/main/orderDetails/orderDetailsContainer'
import ArchivedOrdersContainer from '../components/main/archivedOrders/archivedOrdersContainer'
import OrderEdit from '../components/main/orderEdit/orderEditContainer'
import ShippingAddressesContainer from '../components/main/shippingAddresses/shippingAddressesContainer'
import HomeContainer from '../components/main/homePage/homeContainer'
import UserInformation from '../components/main/user/userInformationEdit'
import CartORderDetails from '../components/main/cart/cartOrder'
// import CreateAccount from '../components/registration/createAccount/createAccount'
// import SignInAccount from '../components/registration/signIn/signInAccount';
// import ResetPassword from '../components/registration/resetPassword/resetPassword';
 

class RoutersForMain extends Component {
    render(){
        return(
            <Fragment>
                <Router  history = {history}>
                    <Route 
                        exact
                        path = '/' 
                        component= {HomeContainer} 
                    />
                    <Route 
                        path = '/products' 
                        component= {ProductsContainer} 
                    />
                    <Route 
                        exact
                        path ='/user-profile' 
                        component = {UserProfileMain}
                    />
                     <Route 
                        path ='/user-profile/edit' 
                        component = {UserProfileEdit}
                    />
                    <Route
                        path ='/user-profile/edit-information' 
                        component = {UserInformation}
                    />
                    <Route 
                        exact
                        path ='/about' 
                        component = {About}
                    />
                    <Route 
                        exact
                        path ='/contact' 
                        component = {Contact}
                    />
                    <Route 
                        path ='/term-of-use' 
                        component = {TermOfUse}
                    />
                    <Route 
                        path ='/privacy-policy' 
                        component = {PrivacyPolicy}
                    />
                    {/* <Route 
                        exact
                        path ='/orders-create' 
                        component = {ProductDetailsContainer}
                    /> */}
                    <Route 
                        exact
                        path ='/cart' 
                        component = {CartContainer}
                    />
                    <Route 
                        path ='/orders-create' 
                        component = {CastomizedProductContainer}
                    />
                    <Route 
                        path ='/review/:id' 
                        component = {OrderReviewContainer}
                    />
                    <Route 
                        path ='/open-orders' 
                        component = {OpenOrdersContainer}
                    />
                    <Route 
                        path ='/order-details/:id' 
                        component = {OrdetDetailsContainer}
                    />
                    <Route 
                        path ='/archive' 
                        component = {ArchivedOrdersContainer}
                    />
                    <Route 
                        path ='/orders/edit/:id' 
                        component = {OrderEdit}
                    />
                      <Route 
                        path ='/cart/order-edit/:id' 
                        component = {CartORderDetails}
                    />
                    <Route 
                        path ='/addresses' 
                        component = {ShippingAddressesContainer}
                    /> 
                     
                </Router>
             
            </Fragment>
        )
    }
}
export default RoutersForMain;