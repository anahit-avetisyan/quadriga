import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import history from './routers/history';
import { Router} from "react-router-dom";  
import { Provider } from 'react-redux';
import reducer from   './reducers/reducers'
import { createStore,applyMiddleware } from 'redux';
import thunk  from 'redux-thunk';   
 

const store=createStore(reducer,applyMiddleware(thunk))
ReactDOM.render(
    <Provider store={store} >
        <Router history = {history}>
            <App />
        </Router>
    </Provider>
    , document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
