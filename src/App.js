import React,{Component} from 'react';
import AppRouters from './routers/appRouters' 
import "bootstrap-css-only/css/bootstrap.min.css";
import {FetchProducts,FetchMaterialTypes,FetchOrderQuantity,FetchShape,FetchSurfaceFinishing,FetchColor,FetchStampingOptions,FetchVarnishOptions,FetchVariableDataOptions,FetchSize} from './reducers/moduls/fetchProduct';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import './App.scss';
 class App extends Component {
    state ={
      isLoading: true
    }
    componentDidMount = () => {
      const {FetchProducts,FetchMaterialTypes,FetchOrderQuantity,FetchShape,FetchSurfaceFinishing,FetchColor,FetchStampingOptions,FetchVarnishOptions,FetchVariableDataOptions,FetchSize} = this.props
      FetchProducts()
      FetchMaterialTypes()
      FetchShape()
      FetchOrderQuantity()
      FetchSurfaceFinishing()
      FetchColor()
      FetchStampingOptions()
      FetchVarnishOptions()
      FetchVariableDataOptions()
      FetchSize()
      this.setState({isLoading: false})
    }
    render(){
      return(
        <div className="App">
          {this.state.isLoading ? <div>...Loading</div> :
          <AppRouters/>}
        </div>
      )
    }
 }
 function mapDispatchToProps(dispatch) { 
  return bindActionCreators(
      {
          FetchProducts,
          FetchMaterialTypes,
          FetchShape,
          FetchOrderQuantity,
          FetchSurfaceFinishing,
          FetchColor,
          FetchStampingOptions,
          FetchVarnishOptions,
          FetchVariableDataOptions,
          FetchSize

      },
      dispatch
  );
};

export default connect(null,mapDispatchToProps)(App);
