export function SelectedValues(value){
    return {
        type:"SELECTED_VALUES",
        value 
    }
};
export function SelectedValuesFailure(){
    return {
        type: "SELECTED_VALUES_FAILURE",
    }
};
export function SelectedProductId(value){
    return{
        type:"SELECTED_ID",
        value 
    }
};
export function CartProduct(value){
    return{
        type:"CARTPRODUCT",
        value 
    }
};
export function UserInformation(value){
    return{
        type:"USER_INFORMATION",
        value 
    }
};
// Get main product
export function FetchProductsPending() {
    return {
        type: "FETCH_PRODUCTS_PENDING"
    }
};
export function FetchProductsSuccess(products) {
    return {
        type: "FETCH_PRODUCTS_SUCCESS",
        products 
    }
}
export function FetchProductsError(error) {
    return {
        type: "FETCH_PRODUCTS_ERROR",
        error 
    }
}
export function ResetInitialState(){
    return {
        type: "RESET_INITIAL_STATE",
    }
}
// Get material types product
export function FetchMaterialTypesPending() {
    return {
        type: "FETCH_MATERIAL_TYPES_PENDING"
    }
};
export function FetchMaterialTypesSuccess(products) {
    return {
        type: "FETCH_MATERIAL_TYPES_SUCCESS",
        products 
    }
}
export function FetchMaterialTypesError(error) {
    return {
        type: "FETCH_MATERIAL_TYPES_ERROR",
        error 
    }
}
export function ResetInitialStateMaterialTypes(){
    return {
        type: "RESET_INITIAL_STATE_MATERIAL_TYPES",
    }
}
//get order quantity
export function FetchOrderQuantityPending() {
    return {
        type: "FETCH_ORDER_QUANTITY_PENDING"
    }
};
export function FetchOrderQuantitySuccess(products) {
    return {
        type: "FETCH_ORDER_QUANTITY_SUCCESS",
        products 
    }
}
export function FetchOrderQuantityError(error) {
    return {
        type: "FETCH_ORDER_QUANTITY_ERROR",
        error 
    }
}
export function ResetOrderQuantity(){
    return {
        type: "RESET_INITIAL_ORDER_QUANTITY",
    }
}
//get shape of product
export function FetchShapePending() {
    return {
        type: "FETCH_SHAPE_PENDING"
    }
};
export function FetchShapeSuccess(products) {
    return {
        type: "FETCH_SHAPE_SUCCESS",
        products 
    }
}
export function FetchShapeError(error) {
    return {
        type: "FETCH_SHAPE_ERROR",
        error 
    }
}
export function ResetShape(){
    return {
        type: "RESET_SHAPE_QUANTITY",
    }
}
//get surface finishing
export function FetchSurfaceFinishingPending() {
    return {
        type: "FETCH_SURFACE_FINISHING_PENDING"
    }
};
export function FetchSurfaceFinishingSuccess(products) {
    return {
        type: "FETCH_SURFACE_FINISHING_SUCCESS",
        products 
    }
}
export function FetchSurfaceFinishingError(error) {
    return {
        type: "FETCH_SURFACE_FINISHING_ERROR",
        error 
    }
}
export function ResetSurfaceFinishing(){
    return {
        type: "RESET_SURFACE_FINISHING_QUANTITY",
    }
}
//get color data
export function FetchColorPending() {
    return {
        type: "FETCH_COLOR_PENDING"
    }
};
export function FetchColorSuccess(products) {
    return {
        type: "FETCH_COLOR_SUCCESS",
        products 
    }
}
export function FetchColorError(error) {
    return {
        type: "FETCH_COLOR_ERROR",
        error 
    }
}
export function ResetColor(){
    return {
        type: "RESET_COLOR_QUANTITY",
    }
}
//get stamping options  data
export function FetchStampingOptionsPending() {
    return {
        type: "FETCH_STAMPING_OPTIONS_PENDING"
    }
};
export function FetchStampingOptionsSuccess(products) {
    return {
        type: "FETCH_STAMPING_OPTIONS_SUCCESS",
        products 
    }
}
export function FetchStampingOptionsError(error) {
    return {
        type: "FETCH_STAMPING_OPTIONS_ERROR",
        error 
    }
}
export function ResetStampingOptions(){
    return {
        type: "RESET_STAMPING_OPTIONS_QUANTITY",
    }
}
// get varnish options data
export function FetchVarnishOptionsPending() {
    return {
        type: "FETCH_VARNISH_OPTIONS_PENDING"
    }
};
export function FetchVarnishOptionsSuccess(products) {
    return {
        type: "FETCH_VARNISH_OPTIONS_SUCCESS",
        products 
    }
}
export function FetchVarnishOptionsError(error) {
    return {
        type: "FETCH_VARNISH_OPTIONS_ERROR",
        error 
    }
}
export function ResetVarnishOptions(){
    return {
        type: "RESET_VARNISH_OPTIONS_QUANTITY",
    }
}
// get  variable data options 
export function FetchVariableDataOptionsPending() {
    return {
        type: "FETCH_VARIABLE_DATA_OPTIONS_PENDING"
    }
};
export function FetchVariableDataOptionsSuccess(products) {
    return {
        type: "FETCH_VARIABLE_DATA_OPTIONS_SUCCESS",
        products 
    }
}
export function FetchVariableDataOptionsError(error) {
    return {
        type: "FETCH_VARIABLE_DATA_OPTIONS_ERROR",
        error 
    }
}
export function ResetVariableDataOptions(){
    return {
        type: "RESET_VARIABLE_DATA_OPTIONS_QUANTITY",
    }
}
//size action
export function FetchSizePending() {
    return {
        type: "FETCH_SIZE_PENDING"
    }
};
export function FetchSizeSuccess(products) {
    return {
        type: "FETCH_SIZE_SUCCESS",
        products 
    }
}
export function FetchSizeError(error) {
    return {
        type: "FETCH_SIZE_ERROR",
        error 
    }
}
export function ResetSize(){
    return {
        type: "RESET_SIZE_QUANTITY",
    }
}