import {FetchProductsPending, FetchProductsSuccess, FetchProductsError,
    FetchMaterialTypesPending, FetchMaterialTypesSuccess,FetchMaterialTypesError,
    FetchOrderQuantityPending, FetchOrderQuantitySuccess, FetchOrderQuantityError,
    FetchShapePending, FetchShapeSuccess, FetchShapeError,
    FetchSurfaceFinishingPending, FetchSurfaceFinishingSuccess, FetchSurfaceFinishingError,
    FetchColorPending, FetchColorSuccess, FetchColorError,
    FetchStampingOptionsPending, FetchStampingOptionsSuccess, FetchStampingOptionsError,
    FetchVarnishOptionsPending, FetchVarnishOptionsSuccess, FetchVarnishOptionsError,
    FetchVariableDataOptionsPending, FetchVariableDataOptionsSuccess, FetchVariableDataOptionsError,
    FetchSizePending, FetchSizeSuccess, FetchSizeError
} from '../action'

//get all products
export   function FetchProducts() {
    return dispatch => {
        dispatch(FetchProductsPending());
         fetch('https://sparik.ml/api/v1/product/label_type/')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchProductsSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchProductsError(error));
        })
    }
}
//get product material types
export   function FetchMaterialTypes() {
    return dispatch => {
        dispatch(FetchMaterialTypesPending());
         fetch('https://sparik.ml/api/v1/product/material_type/')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchMaterialTypesSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchMaterialTypesError(error));
        })
    }
}
//get product quantity 
export   function FetchOrderQuantity() {
    return dispatch => {
        dispatch(FetchOrderQuantityPending());
         fetch('https://sparik.ml/api/v1/product/order_quantity')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchOrderQuantitySuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchOrderQuantityError(error));
        })
    }
}
// get shape of product
export   function FetchShape() {
    return dispatch => {
        dispatch(FetchShapePending());
         fetch('https://sparik.ml/api/v1/product/die/shape')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchShapeSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchShapeError(error));
        })
    }
}
//get surface finishing 
export   function FetchSurfaceFinishing() {
    return dispatch => {
        dispatch(FetchSurfaceFinishingPending());
         fetch('https://sparik.ml/api/v1/product/options/surface_finishing')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchSurfaceFinishingSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchSurfaceFinishingError(error));
        })
    }
}
//get color of products
export   function FetchColor() {
    return dispatch => {
        dispatch(FetchColorPending());
         fetch('https://sparik.ml/api/v1/product/label_color/')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchColorSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchColorError(error));
        })
    }
}
//get stamping options of products
export   function FetchStampingOptions() {
    return dispatch => {
        dispatch(FetchStampingOptionsPending());
         fetch('https://sparik.ml/api/v1/product/options/foil_stamping')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchStampingOptionsSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchStampingOptionsError(error));
        })
    }
}
//get  varnish  options of products
export   function FetchVarnishOptions() {
    return dispatch => {
        dispatch(FetchVarnishOptionsPending());
         fetch('https://sparik.ml/api/v1/product/options/spot_uv_varnish')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchVarnishOptionsSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchVarnishOptionsError(error));
        })
    }
}
//get numbering/variable data options of products
export   function FetchVariableDataOptions() {
    return dispatch => {
        dispatch(FetchVariableDataOptionsPending());
         fetch('https://sparik.ml/api/v1/product/options/variable_data')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchVariableDataOptionsSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchVariableDataOptionsError(error));
        })
    }
}
//get   size of products
export   function FetchSize() {
    return dispatch => {
        dispatch(FetchSizePending());
         fetch('https://sparik.ml/api/v1/product/die?limit=0')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(FetchSizeSuccess(res.data));
            return res.data;
        })
        .catch(error => {
            dispatch(FetchSizeError(error));
        })
    }
}