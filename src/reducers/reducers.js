import { combineReducers } from 'redux';

function selectedValues(state = {}, action) {
    switch (action.type){
        case "SELECTED_VALUES":
            return action.value
        case "SELECTED_VALUES_FAILURE":
            return state
        default :
        return state 
    }
};
function selectedProductId(state = '', action) {
    switch (action.type){
        case "SELECTED_ID":
        return action.value
        default :
        return state 
    }
};
function cartProduct(state = {}, action) {
    switch (action.type){
        case "CARTPRODUCT":
        return action.value
        default :
        return state 
    }
};
function userInformation(state = {}, action) {
    switch (action.type){
        case "USER_INFORMATION":
            return action.value
            default :
            return state 


    }
};
const initialState = {
    pending: false,
    products: [],
    error: null
}

function productsReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_PRODUCTS_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_PRODUCTS_SUCCESS":
            return {
                ...state,
                pending: false,
                products: action.products
            }
        case "FETCH_PRODUCTS_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE":
            return {
                state
            }
        default: 
            return state;
    }
};
function materialTypeReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_MATERIAL_TYPES_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_MATERIAL_TYPES_SUCCESS":
            return {
                ...state,
                pending: false,
                materialTypes: action.products
            }
        case "FETCH_MATERIAL_TYPES_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE_MATERIAL_TYPES":
            return {
                initialState
            }
        default: 
            return state;
    }
}
function orderQuantityReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_ORDER_QUANTITY_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_ORDER_QUANTITY_SUCCESS":
            return {
                ...state,
                pending: false,
                quantity: action.products
            }
        case "FETCH_ORDER_QUANTITY_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE_ORDER_QUANTITY":
            return {
                initialState
            }
        default: 
            return state;
    }
}
function shapeReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_SHAPE_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_SHAPE_SUCCESS":
            return {
                ...state,
                pending: false,
                shape: action.products
            }
        case "FETCH_SHAPE_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE_SHAPE":
            return {
                initialState
            }
        default: 
            return state;
    }
}
function surfaceFinishingReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_SURFACE_FINISHING_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_SURFACE_FINISHING_SUCCESS":
            return {
                ...state,
                pending: false,
                surfaceFinishing: action.products
            }
        case "FETCH_SURFACE_FINISHING_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE_SURFACE_FINISHING":
            return {
                initialState
            }
        default: 
            return state;
    }
}
function colorReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_COLOR_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_COLOR_SUCCESS":
            return {
                ...state,
                pending: false,
                color: action.products
            }
        case "FETCH_COLOR_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE_COLOR":
            return {
                initialState
            }
        default: 
            return state;
    }
}
//stampingOptions
function stampingOptionsReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_STAMPING_OPTIONS_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_STAMPING_OPTIONS_SUCCESS":
            return {
                ...state,
                pending: false,
                stampingOptions: action.products
            }
        case "FETCH_STAMPING_OPTIONS_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE_STAMPING_OPTIONS":
            return {
                initialState
            }
        default: 
            return state;
    }
}
//VARNISH_OPTIONS
function varnishOptionsReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_VARNISH_OPTIONS_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_VARNISH_OPTIONS_SUCCESS":
            return {
                ...state,
                pending: false,
                varnishOptions: action.products
            }
        case "FETCH_VARNISH_OPTIONS_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE_VARNISH_OPTIONS":
            return {
                initialState
            }
        default: 
            return state;
    }
}
//VARIABLE_DATA_OPTIONS
function variableDataOptionsReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_VARIABLE_DATA_OPTIONS_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_VARIABLE_DATA_OPTIONS_SUCCESS":
            return {
                ...state,
                pending: false,
                variableDataOptions: action.products
            }
        case "FETCH_VARIABLE_DATA_OPTIONS_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_INITIAL_STATE_VARIABLE_DATA_OPTIONS":
            return {
                initialState
            }
        default: 
            return state;
    }
}
//Size
function sizeReducer(state = initialState, action) {
    switch(action.type) {
        case "FETCH_SIZE_PENDING": 
            return {
                ...state,
                pending: true
            }
        case "FETCH_SIZE_SUCCESS":
            return {
                ...state,
                pending: false,
                size: action.products
            }
        case "FETCH_SIZE_ERROR":
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case "RESET_SIZE":
            return {
                initialState
            }
        default: 
            return state;
    }
}
const reducer = combineReducers({selectedValues,selectedProductId,cartProduct,userInformation,
    productsReducer,materialTypeReducer,shapeReducer,orderQuantityReducer,
    surfaceFinishingReducer,colorReducer,stampingOptionsReducer,varnishOptionsReducer,variableDataOptionsReducer,sizeReducer});
      
export default reducer;