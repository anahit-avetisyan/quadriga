import React,{Component} from 'react'
import {Link } from 'react-router-dom'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import {CartProduct} from '../../reducers/action'
import ls from 'local-storage'


class HeaderNavbar extends Component {
    componentDidMount = () => {
        let cartDataFromLocalStorage = ls.get('selectedProduct') ? ls.get('selectedProduct') : {}
        this.props.CartProduct(cartDataFromLocalStorage)  
    }
    render(){
        const {cartProduct} = this.props.state
        return(
            <div className='linkContainer'>
                <Link to='/products'>Standard Order</Link>
                <Link to='/customized'>Customized Orders</Link>
                <div>
                    <Link to='/cart'>Cart</Link>
                  
                    <span>{!cartProduct ? {} : Object.keys(cartProduct).length }</span>
                </div>
                <div>
                    <Link to='/open-orders'>Open Orders</Link>
                    <span>3</span>
                </div>
                <Link to='/archive'>Archived Order</Link> 
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        state,
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            CartProduct
        },
        dispatch
    );
};
export default connect(mapStateToProps,mapDispatchToProps)(HeaderNavbar);
 