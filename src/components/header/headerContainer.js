import React,{Component} from 'react'
// import HeaderNavBar from './headerNavBar'
import {NavLink } from 'react-router-dom';
import shoppingCart from '../../assets/icons/shopping-cart.png'
import history from '../../routers/history'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ls from 'local-storage';
import {FaUser } from "react-icons/fa";
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import {FaMapMarkerAlt} from "react-icons/fa";
import {FaSignOutAlt} from "react-icons/fa";
import {CartProduct} from '../../reducers/action'


class HeaderContainer extends Component {
    state={
        dropdownOpen: false
    } 
    showProfileDetails = ()=>{
        history.push('/user-profile')
    }
    toggle=()=>{
        this.setState(prevState => ({
          dropdownOpen: !prevState.dropdownOpen
        }));
    }
    onMouseEnter=()=> {
        this.setState({dropdownOpen: true});
    }
      onMouseLeave=()=> {
        this.setState({dropdownOpen: false});
      }
    showShippingAddresses = () => {
        history.push('/addresses')
    }
    logOut = () => {
        ls.remove('login')
      history.push('/')
    }
    basket = () => {
        history.push('/cart')
    }
    componentDidMount = () => {
        let cartProductFromLs = ls.get('selectedStandardProduct') ? ls.get('selectedStandardProduct') : {}
        this.props.CartProduct(cartProductFromLs)

    }
    render(){
        const {cartProduct} = this.props
        return(
            <div className='headerWrapper'>
                <div className='headerContent'>
                    <NavLink 
                        to='/about'
                        activeStyle={{ fontWeight: 'bold' }}
                        > 
                        About us
                    </NavLink>
                    <NavLink 
                        to='/contact'
                        activeStyle={{ fontWeight: 'bold' }}
                    >
                        Contuct
                    </NavLink>
                    <Dropdown   className="d-inline-block" onMouseOver={this.onMouseEnter}  isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                        <DropdownToggle  className='toggle'>
                            <span className='profileToggle'>Profile</span>
                          
                        </DropdownToggle>
                        <DropdownMenu  className='dropDownMenu' onMouseLeave={this.onMouseLeave} onMouseOver={this.onMouseEnter} >
                        <DropdownItem onClick={this.showProfileDetails}   className="headerHover"><FaUser style={{marginRight:'10px'}}/> Profile</DropdownItem>
                        <DropdownItem  onClick={this.showShippingAddresses} className="headerHover"><FaMapMarkerAlt style={{marginRight:'10px'}}/>Shipping Addresses </DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem className="headerHover" onClick={this.logOut}><FaSignOutAlt style={{marginRight:'10px'}}/>Log out</DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                    <div>
                        <img className='shoppingCart' onClick={this.basket} src={shoppingCart} alt='shopping cart'/>
                        <div className='cartQuantity'><span>{cartProduct && Object.keys(cartProduct).length!==0 ? Object.keys(cartProduct).length : 0}</span></div>
                    </div>
                    {/* <div className='languageAndPolygonContainer'>
                        <span >EN </span>
                        <img src={Polygon} alt='img'/>
                    </div> */}
                    
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    cartProduct:state.cartProduct ? state.cartProduct : {}
   
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            CartProduct
        },
        dispatch
    );
};
export default connect(mapStateToProps,mapDispatchToProps)(HeaderContainer);