import React,{Component} from 'react'
import {Link } from 'react-router-dom';
import './sideBar.scss'
import Logo from '../../assets/images/QuadrigaUSA-logo (1).svg';
import history from '../../routers/history'
import SignInAccount from '../registration/signIn/signInAccount'
import CreateAccount from '../registration/createAccount/createAccount'
import MainModal from '../helper/mainModal.js'
import 'react-router-modal/css/react-router-modal.css';
import RegistrationWithFacebookAndGoogle from '../registration/registrationWithFacebookAndGoogle';
import ls from 'local-storage'

class SideBar extends Component {
    state = {
        modalSignIn: false,
        modalSignUp:false
    }
  toggleSignIn= () => {
    this.setState(prevState => ({
      modalSignIn: !prevState.modalSignIn,
 
    }));
  }
  toggleSignUp= () => {
    this.setState(prevState => ({
     
      modalSignUp: !prevState.modalSignUp,
    }));
  }
  registrationPage = () => {
    this.toggleSignIn()
    this.toggleSignUp()
    
  }
  homePage = () => {
      history.push('/')
  }

    render(){
        let login=ls.get('login') ? ls.get('login'):false
        const pathName = window.location.pathname
        return(
           <div className='sideBarContainer'>
               <div  className='mainLogo'>
                   <img onClick={this.homePage} style={{cursor:'pointer'}} src={Logo} alt='logo'/>
               </div>
               <div className='ordersContainer'>
                   <span className='sideBarHeader'>Orders</span>
                    <div className='standartProduct'>
                        {pathName !=='/products'? <div   className='circle'></div> : <div   className='circleActive'></div>}
                        <Link to='/products'>Label</Link>
                    </div>
                    {/* <div className='customizedProduct'>
                    {pathName !=='/customized' ? <div   className='circle'></div> : <div   className='circleActive'></div>}
                        <Link to='/customized'>Customized </Link>
                    </div> */}
                    <div className='inProcessProduct'>
                    {pathName !=='/open-orders' ? <div   className='circle'></div> : <div   className='circleActive'></div>}
                        <Link to='/open-orders'>In Process</Link>
                    </div>
                    <div className='archivedProduct'>
                    {pathName !=='/archive' ? <div   className='circle'></div> : <div   className='circleActive'></div>}
                        <Link to='/archive'>Archived </Link> 
                    </div>
               </div>
               {login ? null:
               <div   className='registrationButtonsContainer'>
                      
                   <MainModal
                        toggleName='Sign up'
                        toggleClassName='signUpButton'
                        mainDivClassName='signUpButtonContainer'
                        toggle={this.toggleSignUp}
                        modal={this.state.modalSignUp}
                        className='signUpModalContainer'
                        wrapClassName='signUpwrapClassName'
                        modalClassName='signUpmodalClassName'
                        backdropClassName= 'signUpbackdropClassName'
                        contentClassName='signUpcontentClassName'
                    >
                       <CreateAccount
                            toggle={this.toggleSignUp}

                       />
                   </MainModal>
                   <div className='socialIconsContainer'>
                        <RegistrationWithFacebookAndGoogle
                            facebookButton='facebookButtonInSideBar'
                            googleButton='googleButtonInSideBar'
                        />
                   </div>
                   <MainModal
                        toggleName='Sign in'
                        toggleClassName='signInButton'
                        mainDivClassName='signInButtonContainer'
                        // save ='Sign In'
                        // editData={this.signIn}
                        toggle={this.toggleSignIn}
                        modal={this.state.modalSignIn}
                        className='signInModalContainer' 
                        wrapClassName='signInwrapClassName'
                        modalClassName='signInmodalClassName'
                        backdropClassName= 'signInbackdropClassName'
                        contentClassName='signIncontentClassName'
                   
                    >
                        <SignInAccount
                            registrationPage={this.registrationPage}
                            toggle={this.toggleSignIn}
                        />
                   </MainModal>
                   
               </div>
               }
           </div> 
        )
    }
}
export default SideBar