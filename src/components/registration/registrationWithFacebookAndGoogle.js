import React, {Component} from 'react'
import FacebookLogin from 'react-facebook-login'
import GoogleLogin from 'react-google-login'
import Facebook from '../../assets/icons/facebook.png'
import Google from '../../assets/icons/google+.png'
import ls from 'local-storage'
// import {request} from '../helper/utils'

class RegistrationWithFacebookAndGoogle extends Component {
   responseFacebook = (response) => {
    ls.set('FacebookData',response)
    // request('aaa','POST',response)
        //.then(response => {
        //console.log(response)   
        // })
        // .catch(error => error);
  }

    responseGoogle = (response) => {
    // request('aaa','POST',response)
        //.then(response => {
        //console.log(response)   
        // })
        // .catch(error => error);
  }

    render(){
      const img = <img src={Facebook} alt='facebook'/>
          return (
            <div className="facebookAndGoogleSignInContainer">
             
      
            <FacebookLogin
              appId="1264531930372438"
              fields="name,email,picture"
              callback={this.responseFacebook}
              cssClass={this.props.facebookButton}
              textButton = ""       
              icon={img}
            />
        
            <GoogleLogin
              clientId="153974370834-qurls8r5ggusolhmkd4p9rnqek8t7t15.apps.googleusercontent.com" //CLIENTID NOT CREATED YET
              buttonText=""
              onSuccess={this.responseGoogle}
              onFailure={this.responseGoogle}
              icon={false}
              className={this.props.googleButton}
            >
             <img src={Google} alt='goggle+'/>
            </GoogleLogin>
            </div>
          );
    }
}
export default RegistrationWithFacebookAndGoogle