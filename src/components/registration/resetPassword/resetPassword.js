import React,{Component} from 'react'
import Input from '../../helper/input'
import Button from '../../helper/button'
import {request} from '../../helper/utils'
import history from '../../../routers/history'
import Logo from '../../../assets/images/QuadrigaUSA-logo (1).svg'
import './resetPasswordStyle.scss'

class ResetPassword extends Component {
    state={
        resetPasswordResponse:{}
    }
    handleChange = (value,name) => {
        this.setState({[name]:value})
    }
    resetPassword = () => {
        const {email}=this.state
        request('url','POST',email)
        .then (response =>{
            this.setState({resetPasswordResponse:response})
        })
        this.props.toggle()
    }
    cancel = () => {
        history.push('/signin-account' )
    }
    homePage = () => {
        history.push('/')
        this.props.toggle()
    }
    render(){
        return(
            <div className='passwordResetContainer'>
                <img src={Logo} alt='logo' className='resetPasswordLogo' onClick={this.homePage}/>
                <p>Please enter the email that you used to log on to the site</p>
                <Input 
                    type='email'
                    inputName='email'
                    changeCallback={this.handleChange} 
                    placeholder="Email" 
                    autocomplete="off"
                />
                <Button
                    className='resetPasswordButton'
                    name='Send'
                    callback={this.resetPassword}
                />
            </div>
        )
    }
}
export default ResetPassword;