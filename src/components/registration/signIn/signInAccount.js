import React,{Component,Fragment} from 'react'
import Input from '../../helper/input'
import Button from '../../helper/button'
import './signInAccountStyle.scss'
import history from '../../../routers/history'
import Logo from '../../../assets/images/QuadrigaUSA-logo (1).svg'
import ResetPassword from '../resetPassword/resetPassword';
import RegistrationWithFacebookAndGoogle from '../registrationWithFacebookAndGoogle';
import ls from 'local-storage'
class SignInAccount extends Component {
    state = {
        userInformation:{},
        checked:false,
        resetPassword:false,
        signIn:true,
        error:''

    }
    handleChange = (value,name) => {
        this.setState({[name]:value})
    }
    changeCheckBox =()=>{
        this.setState(prevState => ({
            checked: !prevState.checked
          }));
     
        
    }
    signIn = () => {
        const {email,password,checked} =this.state
        
        let signInData={
            email:email,
            password:password,
        }

        if(checked===true){
            sessionStorage.setItem('key',checked)
        }

        if(email && password){
            fetch('https://sparik.ml/auth/login',{
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(signInData)
            })
            .then(res => res.json())
            .then(response => {
                if(response.success===false){
                    
                  return   this.setState({error:response.error.message})
                }
            ls.set('login',response.success)
            this.props.toggle()
            history.push('/')  
            })
            .catch(error => error);
        }
        else if(!email || !password){
            this.setState({error:"Please fill all fileds",checked:false})
        }
    }

    homePage = () => {
        history.push('/')
        this.props.toggle()
    }
    changePassword = () => {
        this.setState({signIn:false,resetPassword:true})
    }
    registrationPage = () => {
        this.setState({signIn:false})
    }
    render(){
        console.log(this.error)
        return(
            <Fragment>
                {this.state.signIn ? <div className='signInWrapper'>
                    <img  src={Logo} alt='logo' className="mainLogoSignIn" onClick={this.homePage}/>
                    <div className='signInInputsContainer'>
                        <p className='error'>{this.state.error}</p>
                        <Input
                            placeholder='Email address'
                            inputName='email'
                            type='text'
                            changeCallback={this.handleChange}
                            autocomplete="off"
                        /> 
                        <span>{this.emailError}</span>                   
                        <Input
                            autocomplete="off"
                            placeholder='Password'
                            inputName='password'
                            type='password'
                            changeCallback={this.handleChange}   
                        /> 
                        <span>{this.passwordError}</span>
                        <Button
                            className='buttonSignIn'
                            name='Sign in'
                            callback={this.signIn}
                        />
                        <label className='checkboxContainer'>
                        <input 
                            className='checkbox' 
                            name='checkbox' 
                            type='checkbox' 
                            checked={this.checked}
                            onChange={this.changeCheckBox}
                        />
                        Keep me signed in
                        </label>
                            <span onClick={this.changePassword} className='forgotPassword' >Forgot password?</span>
                            <RegistrationWithFacebookAndGoogle 
                                title='Sign in with'
                                facebookButton='facebookButton'
                                googleButton='googleButton'
                            />
                    </div>
                </div> : null}
                    {this.state.resetPassword ? <ResetPassword toggle={this.props.toggle}/> : null}
            </Fragment>
     
        )
    }
}
export default SignInAccount