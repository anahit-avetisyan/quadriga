import React,{Component} from 'react';
import Input from '../../helper/input'
import  Button  from '../../helper/button';
import Logo from '../../../assets/images/QuadrigaUSA-logo (1).svg';
import history from '../../../routers/history';
import './createAccountStyle.scss';
import Files from 'react-files';
import { FaTrashAlt } from "react-icons/fa";
import ls from 'local-storage'

class CreateAccount extends Component {
    state = {
        selected: '',
        files:{},
        disabled:true,
        checked:false,
        error:'',
        errorPassword:''
    }
    handleChange = (value,name) => {
        this.setState({[name]:value})
        
    }
    changeRadio = (event ) => {
    
            this.setState({selected:event.target.value})
        
    }
    homePage = () => {
        history.push('/')
        this.props.toggle()
    }
    cancelRegistration = () => {
        this.props.toggle()
    }
    onFilesChange = (file) => {
        this.setState({
          files:file
        })
    }
    
    onFilesError = (error) => {
        console.log('error code ' + error.code + ': ' + error.message)
    }
    filesRemoveOne = () => {
        this.refs.files.removeFiles()
    }
    onClickHandler = () => {
        const formData = new FormData()
        Object.keys(this.state.files).forEach((key) => {
          const file = this.state.files[key]
          formData.append(key, new Blob([file], { type: file.type }), file.name || 'file')
        })
    
        // axios.post(`/files`, formData)
        // .then(response => {
        //     console.log(response,"sssssss")
        //      window.alert(`${this.state.files.length} files uploaded succesfully!`)}
        // )
        // .catch(err => window.alert('Error uploading files :('))
        // fetch(url, {
        //     method: method,
        //     body: data,
        //     headers: {
        //         'Accept': 'application/json',
        //         ...authHeader(authToken)
        //     }
        // })
    }
    signUp = () => {
        const {companyName,loginEmail,password,phoneNumber,contactperson,website,rePassword,additionalEmail}=this.state
        
        if(password!== rePassword){
            this.setState({backgroundColor:'#f1bbbb',errorPassword:'The password and confirmation password do not match.',error:''})

        }else{
            this.setState({passwordError:'',backgroundColor:'',errorPassword:''})
            if(loginEmail && password){
                this.setState({error:" "})
            let registrationData = {
                company_name:companyName,
                email:loginEmail,
                password:password,
                phone_number:phoneNumber,
                full_name:contactperson,
                company_website:website,
                backup_email:additionalEmail

            }
            fetch('https://sparik.ml/auth/register',{
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(registrationData)
                })
                .then(res => res.json())
                .then(response => {
                    if(response.success===false){
                     return   Object.values(response.error.details).map(data=>{
                        return this.setState({error:data})
                     })
                    }
                ls.set('login',response.success)
                this.props.toggle()
                history.push('/')  
                })
                .catch(error => error);
        }else{
            this.setState({error:"Please fill required fields - email and password"})
        }
        }
    }
    termOfUsePage = () => {
        history.push('/term-of-use')
        this.props.toggle()
    }
    privacyPolicy= () => {
        history.push('/privacy-policy')
        this.props.toggle()
    }
    changeCheckBox=()=>{
        this.setState(prevState => ({
            checked: !prevState.checked
          }));
    }
    componentDidUpdate(prevProps,prevState){
        if(this.state.checked!==prevState.checked){
              if(this.state.checked===true){
              this.setState({disabled:false,})
          }else{
            this.setState({disabled:true})
          }
        }
    }
    render(){
        const {files,selected,disabled} = this.state
        return(
            <div className='createAccountMain'>
                 <img src={Logo} alt='logo' onClick={this.homePage}/>
                <p className='error'>{this.state.error}</p>
               <div className='formContainer'>
                   <div className='inputsContainer'>
                       <div className='firstDivForInputs'>
                           <span>Company Name</span>
                           <Input
                                placeholder='Company Name'
                                inputName='companyName'
                                type='text'
                                changeCallback={this.handleChange}
                           />
                           <span>Contact Person</span>
                            <Input
                                 placeholder='Contact Person'
                                 inputName='contactperson'
                                 type='text'
                                 changeCallback={this.handleChange} 
                            />
                           <span>Phone Number</span>
                           <Input   
                                placeholder='Phone Number'
                                inputName='phoneNumber'
                                type='text'
                                changeCallback={this.handleChange}   

                           />
                            <span>Website</span>
                            <Input 
                               placeholder='http://'
                               inputName='website'
                               type='text'
                               changeCallback={this.handleChange}   
                            />
                       </div>
                       <div className='secondDivForInputs'>
                            <span>Login Email</span>
                           <Input
                                placeholder='Enter Email Address'
                                inputName='loginEmail'
                                type='text'
                                changeCallback={this.handleChange}   
                           />
                            <span>Additional Email  (optional)</span>
                            <Input 
                                placeholder='Enter Email Address'
                                inputName='additionalEmail'
                                type='text'
                                changeCallback={this.handleChange}  
                                
                                 
                            />
                            <span>Password</span>
                           <Input
                                placeholder='Between 4 and 16 characters'
                                inputName='password'
                                type='password'
                                changeCallback={this.handleChange}
                                autocomplete="off"

                           />
                           <p className='error'>{this.state.errorPassword}</p>
                            <span>Re-enter the Password</span>
                            <Input
                                placeholder='Must be the same password'
                                inputName='rePassword'
                                type='password'
                                changeCallback={this.handleChange} 
                                style={{backgroundColor:this.state.backgroundColor}}
                                autocomplete="off"
                            />
                            {/* <p>{this.state.passwordError}</p> */}
                           
                       </div>
                   </div>
                   <div className='formOtherDetails'>
                        <div className='permition'>
                        <span>Do you have seller's permit?</span>
                        <div className='radioInputsContainer'> 
                            <label>
                            <input 
                                type='radio' 
                                id='radio-1' 
                                name='myRadio' 
                                value='Yes'
                                checked={this.state.selected === 'Yes'} 
                                onChange={ this.changeRadio }/>
                            Yes</label>
                            <label>
                                <input 
                                    type='radio' 
                                    name='myRadio' 
                                    value='No' 
                                    checked={this.state.selected === "No"} 
                                    onChange={this.changeRadio} />
                           No</label>
                        </div>
                        {selected!=="Yes" ? null : <div >
                        <p className='uploadFileSpan'>Upload the Copy of Your Seller's Permit</p>
                            <div className='containerFile'>
                                
                                {files.length > 0
                                ? <div className='files-list'>
                                    <div >{this.state.files.map((file,index) =>
                                        <div className='files-list-item' key={file.id}>
                                        <div className='file-list-item-content'>    
                                            <div  className='files-list-item-name files-list-item-content-item-1'>
                                            <span className='file-list-item-content-name'>{file.name}</span></div>  
                                        </div>
                                        </div>
                                    )}</div>
                                </div>
                                : null            
                                }        
                                <Files
                                    ref='files'
                                    className='files-dropzone'
                                    // style={{ height: '100px' }}
                                    multiple={false}
                                    onChange={this.onFilesChange}
                                    onError={this.onFilesError}
                                    maxFiles={10000000}
                                    maxFileSize={10000000}
                                    minFileSize={0}
                                    clickable
                                > 
                                <span className="chooseFile">Choose File</span>      
                              
                                </Files>     
                                {files.length > 0 ? <div className='files-remove' onClick={()=>this.filesRemoveOne(files)}><FaTrashAlt/></div> : null}
                            </div>          
                            </div>}  
                        </div>
                        <div className='checkAgreement'>
                        <input
                            name='checkBox'
                            type="checkbox"
                            checked={this.state.checked}
                            onChange={this.changeCheckBox}
                            />
                        <span>I agree to Quadriga USA Enterprises' </span>
                        <span  className='termOfUsePage' onClick={this.termOfUsePage}> Term of Use,</span>
                        <span  className='privacyPolicy' onClick={this.privacyPolicy} >Privacy Policy</span>
                        </div>
                        <div className='buttonContainer'>
                            <Button 
                                className='cancel'
                                callback={this.cancelRegistration}
                                // callback={()=>this.props.cancelRegistration()}
                                name='Cancel'
                            />
                            <Button
                                disabled={this.state.disabled}
                                className={!disabled ? 'signUp': 'signUpDisabled'}
                                callback={this.signUp}
                                name='Sign up'
                            />
                        </div>
                   </div>
               </div>
            </div>
        )
    }
}
export default CreateAccount