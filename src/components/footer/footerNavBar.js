import React,{Component} from 'react';
import {Link } from 'react-router-dom';
import Logo from '../../assets/images/QuadrigaUSA-logo (1).svg';
class FooterNavBar extends Component {
    render(){
        return(
            <div className='footerContainer'> 
                <div className='footerContent'>
                    <span>© 2019 Quadriga USA Enterprises All Rights Reserved</span>
                    <div className='footerLinksWraper'>
                        <Link to='/privacy-policy' >Privacy Policy</Link>
                        <Link to='/term-of-use' >Term of Use</Link>
                        <Link to='/contact'>Contact</Link>
                        <Link className='lastLink' to='/about'>About us</Link>
                    </div>
                    <div className='footerLogoContainer'>
                        <img src={Logo} alt='logo'/>
                    </div>
                 
                </div>
            </div>
        )
    }
}
export default FooterNavBar;