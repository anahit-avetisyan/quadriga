import React,{Component} from 'react';
 
import DivWithSpans from '../../helper/divWithSpans';
 
import ls from 'local-storage';
import history from '../../../routers/history';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedProductId,CartProduct} from "../../../reducers/action";
 

class CartOrderDetails extends Component {
    state= {
        productDetails:{},
        materialTypeData: {},
        labelProductData: {},
        maxRollDiameter:'',
        labelsPerRoll:'',
    }
    componentDidMount=()=>{
        let id = this.props.match.params.id
        let dataFromLs = ls.get("selectedProductData") ? ls.get("selectedProductData") : {}
        Object.values(dataFromLs).forEach((data,index)=>{
            if(data.specificId ===  parseInt(id)){
               this.setState({productDetails:data}) 
            }
        })
    }
    
    
    
    
   
      makeOrder=()=> {
      }
    render(){
        const {productDetails}=this.state
        return(
            <div className="orderDetailsContainer">
                {/* <p className="orderDetailsHeader">Cart</p> */}
                <div className="orderMainDetails">
                    <span className="headerOfMain"> {productDetails.product}</span>
                    <DivWithSpans 
                        divClassName='editDataContainer'
                        firstSpanClassName="headerForCartData"
                        secondSpanClassName="resultheaderForCartData"
                        firstData="Job name: "
                        secondData={productDetails.jobName}
                    />
                    <DivWithSpans
                        divClassName='editDataContainer'
                        firstSpanClassName="headerForCartData"
                        secondSpanClassName="resultheaderForCartData"
                        firstData="Order #: "
                    />
                    <DivWithSpans
                        divClassName='editDataContainer'
                        firstSpanClassName="headerForCartData"
                        secondSpanClassName="resultheaderForCartData" 
                        firstData="Ordered on: "
                        secondData={productDetails.date}
                        />
                    <DivWithSpans
                        divClassName='editDataContainer'
                        firstSpanClassName="headerForCartData"
                        secondSpanClassName="resultheaderForCartData" 
                        firstData="Finel price: "
                    />        
                </div>
                <div>
                    <p className="parametersHeader">Order detiles</p>
                    <div className="ordersDetailsFirstPartContainer">
                        <div className="orderLabelDetails">
                            <span className="componentsHeader">Label Specifications</span>
                            <DivWithSpans
                                divClassName='editDataContainer'
                                firstSpanClassName="headerForCartData"
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Product: "
                                secondData={productDetails.product}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Material type: "
                                secondData={productDetails.materialType}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData" 
                                firstData="Shape: "
                                secondData={productDetails.shape}
                                />
                            <DivWithSpans 
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Size: "
                                secondData={productDetails.size}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Quantity: "
                                secondData={productDetails.quantity}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData" 
                                firstData="Color: "
                                secondData={productDetails.color}
                            /> 
                        </div>
                        <div className="orderOptionsDetails">
                            <span className="componentsHeader">Options</span>
                            <DivWithSpans 
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Surface Finishing: "
                                secondData={productDetails.surfaceFinishing}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Foil Stamping: "
                                secondData={productDetails.foilStamping}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Numbering: "
                                secondData={productDetails.numbering}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData" 
                                firstData="Spot UV Varnish: "
                                secondData={productDetails.spotUvVarnish}
                                /> 
                        </div>
                    </div>
                    <div className="ordersDetailsSecondPartContainer">
                        <div className="orderRollDetails">
                                <span className="componentsHeader">Roll Parameters</span>
                                <DivWithSpans 
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData"
                                    firstData="Label Orientation: "
                                    secondData={productDetails.labelOrientation}
                                />
                                <DivWithSpans
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData"
                                    firstData="Gap Between Labels: "
                                    secondData={productDetails.gapBetweenLabels}
                                />
                                <DivWithSpans
                                    divClassName='editDataContainer'
                                    firstSpanClassName="headerForCartData"
                                    secondSpanClassName="resultheaderForCartData" 
                                    firstData="Core Diameter: "
                                    secondData={productDetails.coreDiameter}
                                    />
                                <DivWithSpans 
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData"
                                    firstData="Max Roll Diameter: "
                                    secondData={productDetails.maxRollDiameter}
                                />
                                <DivWithSpans
                                    firstSpanClassName="headerForCartData"
                                    secondSpanClassName="resultheaderForCartData"
                                    divClassName='editDataContainer'
                                    firstData="Labels Per Roll: "
                                    secondData={productDetails.labelsPerRoll}
                                />
                                <DivWithSpans
                                    firstSpanClassName="headerForCartData"
                                    secondSpanClassName="resultheaderForCartData"
                                    divClassName='editDataContainer'
                                    firstData="Perforation: "
                                    secondData={productDetails.perforation}
                                    /> 
                            </div>
                            <div className="orderOtherDetails">
                                <span className="componentsHeader">Other</span>
                                <DivWithSpans 
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData"
                                    firstData="Number Of Artworks: "
                                    secondData={productDetails.materialType}
                                />
                                <DivWithSpans
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData"
                                    firstData="Layout Fee: "
                                />
                                <DivWithSpans
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData" 
                                    firstData="Production Proof: "
                                    /> 
                            </div>
                    </div>
                    <div className="notesData">
                        <span className="componentsHeader">Notes: </span><span className="headerForCartData">{productDetails.textAreaValue}</span>
                        

                    </div>
                    <div>
                        <span className="componentsHeader">Shipping Method: </span>
                        <span className="headerForCartData">{productDetails.shipping}</span>
                        
                    </div>
                    <div className="priceContainer">
                        <DivWithSpans 
                            firstSpanClassName="headerForCartData"
                            divClassName='editDataContainer'
                            secondSpanClassName="resultheaderForCartData"
                            firstData="Price: "
                        />
                         <DivWithSpans 
                            firstSpanClassName="headerForCartData"
                            divClassName='editDataContainer'
                            secondSpanClassName="resultheaderForCartData"
                            firstData="Price Per Label: "
                        />
                         <DivWithSpans 
                            firstSpanClassName="headerForCartData"
                            divClassName='editDataContainer'
                            secondSpanClassName="resultheaderForCartData"
                            firstData="Layout Fee: "
                        />
                         <DivWithSpans 
                            firstSpanClassName="headerForCartData"
                            divClassName='editDataContainer'
                            secondSpanClassName="resultheaderForCartData"
                            firstData="Sales Tax: "
                        />
                         <DivWithSpans 
                            firstSpanClassName="resultheaderForCartData "
                            divClassName='editDataContainer'
                            secondSpanClassName="resultheaderForCartData"
                            firstData="Total: "
                        />
                             
                    </div>
                </div>
                {/* <div className='orderDetailsButtonContainer'>
                    < MainModal
                        mainDivClassName = "confirmation"
                        className='deleteModal'
                        modalTitle="CONFIRMATION"
                        editData={(toggle)=>this.deleteFromCart(toggle)}
                        toggleName={<FaTrashAlt className="trashIcon"/>}
                        save="YES"
                        cancel="NO"
                        saveButtonClassName="deleteButtonConformation"
                    >
                        <span className="textAboutConformation">Do You Want To Delete This Order?</span>
                    </ MainModal>
                    <Button
                        name="Pay this order"
                        className="makeOrder"
                        callback={this.makeOrder}
                    />
                </div> */}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
    state,
    products : state.productsReducer ? state.productsReducer.products :{},
    materialTypes : state.materialTypeReducer ? state.materialTypeReducer.materialTypes : {},
    quantity: state.orderQuantityReducer ? state.orderQuantityReducer.quantity : {},
    shape: state.shapeReducer ? state.shapeReducer.shape : {},
    surfaceFinishing: state.surfaceFinishingReducer ? state.surfaceFinishingReducer.surfaceFinishing : {},
    color: state.colorReducer ? state.colorReducer.color : {},
    stampingOptions: state.stampingOptionsReducer ? state.stampingOptionsReducer.stampingOptions : {},
    varnishOptions: state.varnishOptionsReducer ? state.varnishOptionsReducer.varnishOptions: {},
    variableDataOptions: state.variableDataOptionsReducer ? state.variableDataOptionsReducer.variableDataOptions : {}
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedProductId,
            CartProduct,
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(CartOrderDetails);