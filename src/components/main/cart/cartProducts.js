import React,{Component,Fragment} from "react"
import DivWithSpans from "../../helper/divWithSpans"
import Button from "../../helper/button"
import history from "../../../routers/history"
import ls from "local-storage"
// import {getRequest} from "../../helper/utils"
import MainModal from "../../helper/mainModal.js";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedProductId,CartProduct} from "../../../reducers/action";
import { FaTrashAlt } from "react-icons/fa"


class CartProducts extends Component {
    state={
        date:new Date().toLocaleString(),
        basketDataFromLs:{}
    }
  
    componentDidMount=()=> {
        let selectedData = ls.get("selectedProductData") ? ls.get("selectedProductData") : {}
        this.props.CartProduct(selectedData)
        this.setState({basketDataFromLs:selectedData})
        
        //for customized product
        // let selectedData = ls.get("selectedProduct") ? ls.get("selectedProduct") : {}
        // this.props.CartProduct(selectedData)
        // this.setState({basketDataFromLs:selectedData})
        // this.getDate();
    }
    reviewOrder = (data) => {
        let id = data.specificId
        history.push(`/cart/order-edit/${data.specificId}`)
    //    history.push(`/review/${id}`)
    }
    changeParameters = (data) => {
        let selectedData={
            product:data.name,
            specificId:data.specificId,
            id:data.id,
            
        }
        ls.set("selectedData",selectedData)
        this.props.SelectedProductId(data.id)
        history.push('/orders-create')
        // history.push(`/orders/edit/${name}`)
         
    }
    deleteFromCart = (closeModal,data) => {
        let id=data.specificId
        let dataFromLs = ls.get("selectedProductData") ? ls.get("selectedProductData") : {}
         Object.values(dataFromLs).forEach((data,index)=>{
            if(data.specificId===id){
                delete dataFromLs[data.specificId]
            }
             
        })
        ls.set("selectedProductData",dataFromLs)
        this.props.CartProduct(ls.get("selectedProductData") ? ls.get("selectedProductData") : {})
        this.props.CartProduct(dataFromLs)
        this.setState({basketDataFromLs:dataFromLs})
        closeModal()
    }
    render(){
        const {basketDataFromLs} = this.state
        console.log(this.state.basketDataFromLs)
        return(
            <Fragment>
                 {basketDataFromLs && Object.values(basketDataFromLs).length===0 ? 
                <div >
                    <p className="cartInformation">Your Cart is empty.</p> 
                </div>
           :Object.values(basketDataFromLs).map((data,index)=>{
                return(
            <div key={index} className="cartProductDetailsWrapper">
                <div className="actionContainer">
                    <p className="productName">{index+1}. {data.product}</p>
                    <div className="line"></div>
                    <Button
                        className="orderDetailsButton"
                        name="Order details"
                        callback={()=>this.changeParameters(data)}
                    />
                    <Button
                        className="orderButton"
                        name="Order"
                        callback={()=>this.reviewOrder(data)}
                    />
                     < MainModal
                            mainDivClassName = "confirmation"
                            className='deleteModal'
                            modalTitle="CONFIRMATION"
                            editData={(toggle)=>this.deleteFromCart(toggle,data,index)}
                            toggleName={<FaTrashAlt className="trashIcon"/>}
                            save="YES"
                            cancel="NO"
                            saveButtonClassName="deleteButtonConformation"
                            
                        >
                            <span className="textAboutConformation">Do You Want To Delete This Order?</span>
                        </ MainModal>
                </div>
                <div className="cartProductDetails">
                        <DivWithSpans 
                            firstSpanClassName="headerForCartData"
                            secondSpanClassName="resultheaderForCartData"
                            firstData="Job name: "
                            secondData= {data.jobName}
                        />
                        <DivWithSpans
                            firstSpanClassName="headerForCartData"
                            secondSpanClassName="resultheaderForCartData"
                            firstData="Order #: "
                        />
                        <DivWithSpans
                            firstSpanClassName="headerForCartData"
                            secondSpanClassName="resultheaderForCartData" 
                            firstData="Ordered on: "
                            secondData= {data.date}
                        />
                        <DivWithSpans
                            firstSpanClassName="headerForCartData"
                            secondSpanClassName="resultheaderForCartData" 
                            firstData="Finel price: "
                            secondData= {data.price}
                        />
                </div>
            </div>
            )
        })}
            </Fragment>
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
   
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedProductId,
            CartProduct,
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(CartProducts);
 