import React,{Component} from 'react'
import CartProducts from './cartProducts'
import './cartStyle.scss'
 

class CartContainer extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }
    render(){
        return(
            <div className='cartProductContainer'>
                <p className="headerOfCart">Cart</p>
                <CartProducts/>
            </div>
        )
    }
}
export default CartContainer;