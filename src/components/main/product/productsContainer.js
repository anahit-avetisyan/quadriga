import React,{Component} from 'react'
import Product from './product'
import './productStyle.scss'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {FetchProducts} from "../../../reducers/moduls/fetchProduct";

class ProductsContainer extends Component {
    componentDidMount=()=>{
        const {FetchProducts} = this.props
        FetchProducts()
        //add to all pages
        window.scrollTo(0, 0);
    };
    render(){
         const {products} = this.props
        return(
            <div className='productPageContainer'>
              <div className='productHeaderField'>
                  <span>All Roll Label Products</span>
                </div>
                <div className='productsContainer'>
                {products && Object.keys(products)!==0 ? Object.values(products).map((data,index)=>{
                    return  <Product key = {index} dataAboutProduct={data}/> 
                }):<Product dataAboutProduct={{}} />}
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    products : state.productsReducer ? state.productsReducer.products :{}
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            FetchProducts
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(ProductsContainer);