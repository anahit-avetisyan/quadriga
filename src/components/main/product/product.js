import React,{Component,Fragment} from 'react'
import history from '../../../routers/history'
import ls from 'local-storage'
// import Arrow from '../../../assets/images/Group 89.png'
import Paper from '../../../assets/images/lb_paper.png'

class Product extends Component {
    selectData = (data ) => {
        let selectedData={
            id:data.id,
            product:data.long_name
        }
        ls.set('selectedData',selectedData)
        history.push('/orders-create')
    }
    render(){
        const {dataAboutProduct}=this.props
        return(
            <Fragment>  
                        <div  onClick={()=>this.selectData(dataAboutProduct)} className='mainProduct '>
                            <div className='fieldForName'><p>{dataAboutProduct.long_name}</p></div>
                            <img src={Paper} className='productImage'  alt = "img"  />
                            <div className="">
                            <div   className='selectButtonContainer'  > 
                                <span  >More Details</span>
                            </div>
                            </div>
                        </div>
                        
                
                    
            </Fragment>
        )
    }
}
export default Product