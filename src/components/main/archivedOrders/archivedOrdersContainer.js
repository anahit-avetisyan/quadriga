import React,{Component} from 'react'
import './archivedOrdersStyle.scss'
import ArchiveFilterField from './component/archiveFilterField';


class ArchivedOrdersContainer extends Component{
    render(){
        return(
            <div className='archivedOrderContainer'>
                <p className='archivedOrderHeader'>Archived</p>
                <ArchiveFilterField/>
            </div>
        )
    }
}
export default ArchivedOrdersContainer;