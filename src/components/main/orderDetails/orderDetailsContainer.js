import React,{Component} from 'react'
import OpenOrderDetail from '../openOrders/components/openOrderDetail'
import ls from 'local-storage'
import history from '../../../routers/history'
import OrderBody from '../orderReview/component/orderBody';
import Button from '../../helper/button';
import MainModal from '../../helper/mainModal.js';
import './orderDetailsStyle.scss'



class OrderDetailsContainer extends Component {
    state = {
        datafromLs:{}
    }
    componentDidMount = () =>{
        let id = this.props.match.params.id
        let lsData = ls.get('selectedProduct') ? ls.get('selectedProduct') : {}
        Object.values(lsData).forEach((data) => {
            if(data.id=== parseInt(id)){  
                this.setState({datafromLs:data})
            }
       })
    }
    handleChangeLink = () => {
        history.push('/open-orders' )
    }
    render(){
        const {datafromLs} = this.state
         
        return(
            <div className='openOrderContainer'>
            <h1>Order Details</h1>
            <OpenOrderDetail 
                handleChangeLink={this.handleChangeLink}
                buttonName='Back to Open Orders'
                data={datafromLs}
              
            />
            <OrderBody>
                 <a target='blank'  className='linkStyle' style={{ pointerEvents: this.state.disabledLink }} href='https://www.paypal.com/webapps/'>Pay this order</a>
               
                 <Button
                    name = 'Invoice'
                />
                 <MainModal
                    toggleName='Upload'
                    mainDivClassName='uploadModal'
                 >

                 </MainModal>
            </OrderBody>
        </div>
        )
    }
}
export default OrderDetailsContainer