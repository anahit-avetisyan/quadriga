import React,{Component} from 'react';
import MainInformationAboutProduct from '../customizedOrders/component/informationAboutProduct';
import SpecificationsOfProduct from './component/specificationsOfProduct';
import ProductPriceDetails from './component/detailsOfSpecificationsProduct/productPriceDetails';
import './productDetailsStyle.scss';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedValues} from '../../../reducers/action';
import {FetchProducts} from '../../../reducers/moduls/fetchProduct';
import ls from 'local-storage';

class ProductDetailsContainer extends Component {
    state = {
        allData:{},
        data:{},
        choosenProduct:{},
        date:new Date().toLocaleDateString(),
    }
    displayData = [];
    componentDidMount() {
        const {products,SelectedValues,selectedValues} = this.props
        let selectedDataFromLs = ls.get("selectedData") ?  ls.get("selectedData") : {}
        let id = selectedDataFromLs.id
        if(products){
            Object.values(products).forEach(data => {
                if(data.id===id){
                    this.setState({choosenProduct:data,productDefaultValue:data.long_name})
                }
            })
        }else {
            this.setState({choosenProduct:{},productDefaultValue:''})
        }
        let selectedData = Object.assign({},selectedValues)
        selectedData = {
            product: selectedDataFromLs.product,
            id: id,
            date:this.state.date
        }
        SelectedValues(selectedData)
       
        window.scrollTo(0, 0);
    }
    style = {
        control: (base, state) => ({
          ...base,
          background: "#e4e4e4",
          border: state.isFocused ? 0 : 0,
          // This line disable the blue border
          boxShadow: state.isFocused ? 0 : 0,
          "&:hover": {
            border: state.isFocused ? 0 : 0
          }
        }),
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
              ...styles,
              backgroundColor:isSelected ? 'white':  "#e4e4e4",
              color: 'black',
              cursor:'pointer',
              "&:hover": {
                backgroundColor: '#B1B1B1'
              } 
            };
          },
      };
    componentDidUpdate = (prevProps) => {
        if(this.props.products !== prevProps.products){
            let selectedData = ls.get("selectedData") ?  ls.get("selectedData") : {}
            let id = selectedData.id
            const {products } = this.props
            if(products){
                Object.values(products).forEach(data => {
                    if(data.id===id){
                        this.setState({choosenProduct:data,productDefaultValue:data.long_name})
                        }
                        
                })
            }else {
                this.setState({choosenProduct:{},productDefaultValue:''})
            }
         
        }
    }
    changeInputValue =(value,name)=> {
        let {selectedValues,SelectedValues,products,FetchProducts} = this.props
        FetchProducts()
        if(products){
            Object.values(products).forEach(data => {
                if(data.long_name===value){
                    this.setState({choosenProduct:data})
                    let productIdName = Object.assign({},selectedValues)
                    productIdName ={
                        id:  data.id,
                        [name]:value,
                        date:this.state.date
                    }
                    ls.set("selectedData",productIdName)
                    SelectedValues(productIdName)
                }
            })
        } 
    }
    render(){
        const {choosenProduct} = this.state
        const {products,materialTypes,quantity,shape,surfaceFinishing,color,selectedValues,stampingOptions,varnishOptions,variableDataOptions} = this.props
        return(
            <div className='productDetailsPage'>
                <div >
                    <p className="productDetailsHeader">Product Details</p>
                </div>
                <div className='productDetailsContainer'>
                    <div className='wrappProductDetails'>
                        <MainInformationAboutProduct
                            style={this.style}
                            allData={products ? products : {} }
                            handleSelect={this.changeInputValue}
                            product={choosenProduct}
                            productDefaultValue={selectedValues.product ? selectedValues.product  : ""}
                        />
                        <SpecificationsOfProduct
                        style={this.style}
                        materialTypeData={materialTypes}
                        dataOfQuantity = {quantity} 
                        shapeData = { shape}
                        surfaceFinishingData={surfaceFinishing}
                        colorData={color}
                        stampingOptionsData={stampingOptions}
                        varnishOptionsData={varnishOptions}
                        variableDataOptionsData={variableDataOptions}

                        />    
                    </div>
                    <div>
                        <ProductPriceDetails/>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    selectedValues : state.selectedValues ,
    products : state.productsReducer ? state.productsReducer.products :{},
    materialTypes : state.materialTypeReducer ? state.materialTypeReducer.materialTypes : {},
    quantity: state.orderQuantityReducer ? state.orderQuantityReducer.quantity : {},
    shape: state.shapeReducer ? state.shapeReducer.shape : {},
    surfaceFinishing: state.surfaceFinishingReducer ? state.surfaceFinishingReducer.surfaceFinishing : {},
    color: state.colorReducer  ? state.colorReducer.color : {},
    stampingOptions: state.stampingOptionsReducer ? state.stampingOptionsReducer.stampingOptions : {},
    varnishOptions: state.varnishOptionsReducer ? state.varnishOptionsReducer.varnishOptions: {},
    variableDataOptions: state.variableDataOptionsReducer ? state.variableDataOptionsReducer.variableDataOptions : {}
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedValues,
            FetchProducts
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(ProductDetailsContainer);