import React,{Component} from 'react';
import MultipleSelectInput from '../../../../helper/multipleSelectInput';
import Data from '../../../../helper/productData';
import SelectWithInput from '../../../../helper/selectwithInput';

class ProductRollParametrs extends Component{
    render(){
        return(
            <div className='labelForSpecificationsContainer'>
                <p className ={this.props.headerClassName}>{this.props.header}</p>
                <div  className='labelSpecificationContent'>
                    <div className='rollParametersDiv'>  
                        <MultipleSelectInput
                            name="Roll Finishing"
                            selectName = "rollFinishing"
                            defaultValue = {this.props.rollFinishing}
                            multiple = {false}
                            productData = {Data} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)}  
                            formClassName = "selectStyle"
                            styles={this.props.style}
                            selectClassName="selectInput"
                        />                    
                        <MultipleSelectInput
                             name="Label Direction"
                             selectName = "labelDirection"
                             defaultValue = {this.props.labelDirection}
                             multiple = {false}
                             productData = {Data} 
                             handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                             formClassName = "selectStyle"  
                             styles={this.props.style}
                            selectClassName="selectInput"
                             
                         />       
                            <SelectWithInput
                            inputInsideSelect={this.props.inputInsideSelect}
                            mainInputInsideSelect={this.props.mainInputInsideSelect}
                            name='Labels per Roll '
                            onKeyPress={(value,name,e,closeButton)=>this.props.selectValue(value,name,e,closeButton)}
                            changeCallback={(value,name) => this.props.handleChange(value,name)}
                            inputName='labelsPerRoll'
                            type='text'
                            selectedData={this.props.selectedDataForPerRoll ? this.props.selectedDataForPerRoll : "Not important" }
                            nameAfterInput='labels' 
                            option = "Not important"
                            changeToNotImportent = {(closeButton)=>this.props.changeToNotImportentForPerRoll(closeButton)}
                            inputInsideSelectFormContainer={this.props.inputInsideSelectFormContainer}        
                        /> 
                    </div>
                    <div className='rollParametersDiv'>
                    <SelectWithInput
                            inputInsideSelect={this.props.inputInsideSelect}
                            mainInputInsideSelect={this.props.mainInputInsideSelect}
                            name='Labels Per Stack'
                            onKeyPress={(value,name,e,closeButton)=>this.props.selectValue(value,name,e,closeButton)}
                            changeCallback={(value,name,e) => this.props.handleChange(value,name,e)}
                            inputName='labelsPerStack'
                            type='text'
                            selectedData={this.props.selectedDatalabelsPerStack ? this.props.selectedDatalabelsPerStack : "Not important" }
                            nameAfterInput='labels'
                            option = "Not important"
                            changeToNotImportent = {(closeButton)=>this.props.changeToNotImportentlabelsPerStack(closeButton)} 
                            inputInsideSelectFormContainer={this.props.inputInsideSelectFormContainer}    
                        />
                        <SelectWithInput

                            inputInsideSelect={this.props.inputInsideSelect}
                            mainInputInsideSelect={this.props.mainInputInsideSelect}
                            name='Maximum Outer Diameter'
                            onKeyPress={(value,name,e,closeButton)=>this.props.selectValue(value,name,e,closeButton)}
                            changeCallback={(value,name,e) => this.props.handleChange(value,name,e)}
                            inputName='maxRollDiameter'
                            type='text'
                            selectedData={this.props.selectedDataForMaxRoll ? this.props.selectedDataForMaxRoll : "Not important" }
                            nameAfterInput='inch'
                            option = "Not important"
                            changeToNotImportent = {(closeButton)=>this.props.changeToNotImportentForMaxRoll(closeButton)} 
                            inputInsideSelectFormContainer={this.props.inputInsideSelectFormContainer}    
                        />
                        < MultipleSelectInput
                            name="Core Diameter"
                            selectName = "coreDiameter"
                            defaultValue = {this.props.coreDiameter}
                            multiple = {false}
                            productData = {Data} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                            formClassName = "selectStyle"
                            styles={this.props.style}
                            selectClassName="selectInput"
                        />             
                    </div>
                </div>
            </div>  
        )
    }
}
export default ProductRollParametrs;