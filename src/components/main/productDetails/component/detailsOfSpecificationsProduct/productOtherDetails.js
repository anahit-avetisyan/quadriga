import React,{Component} from 'react'
import MultipleSelectInput from '../../../../helper/multipleSelectInput'
import Data from '../../../../helper/productData'
class ProductOtherDetails extends Component {
    render(){
        return(
            <div >
                 <h5>{this.props.header}</h5>
                    <div  className='labelForSpecifications'>
                    <div>
                             
                        < MultipleSelectInput
                            name = "Digital Proof"
                            selectName = "digitalProof"
                            defaultValue = {this.props.digitalProof}
                            multiple = {false}
                            productData = {Data} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                            formClassName = "selectStyle"  
                            styles={this.props.style}
                            selectClassName="selectInput"    
                        />
                    </div>
                    <div>
                    </div>
                </div>       
                <div className='textareaContainer'>
                    <textarea name='textAreaValue' placeholder='Please,write your notes here' onChange={(event)=>this.props.handleChange(event.target.value,event.target.name)}/> 
                </div>       
            </div>
        )
    }
}
export default ProductOtherDetails;