import React,{Component} from 'react'
import MultipleSelectInput from '../../../../helper/multipleSelectInput'
import Data from '../../../../helper/productData'


class ProductShipping extends Component {
    render(){
        return(
            <div >
                <div  className='labelForSpecifications'>
                    <div>    
                        < MultipleSelectInput
                            name="Shipping"
                            selectName = "shipping"
                            multiple = {false}
                            productData = {Data} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                            formClassName = "selectStyle" 
                            styles={this.props.style}
                            selectClassName="selectInput"
                            defaultValue = {this.props.shipping}
                        />
                    </div>
                    <div>          
                    </div>
                </div>
            </div>
        )
    }
}
export default ProductShipping;