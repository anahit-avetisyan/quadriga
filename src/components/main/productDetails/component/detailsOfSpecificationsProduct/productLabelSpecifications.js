import React,{Component} from 'react'
import Input from '../../../../helper/input'
import MultipleSelectInput from '../../../../helper/multipleSelectInput'
import Data from '../../../../helper/productData'
// import ls from 'local-storage'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedProductId} from '../../../../../reducers/action'
import {DropdownMenuWithFilter} from './dropdownMenuWithFilter' 
class ProductLabelSpcifications extends Component{
    render(){
        return(
            <div className='labelSpecification'>
                <p className='labelHeader'>LABEL SPECIFICATIONS</p>
                <div  className='labelSpecificationContent'>
                    <div className='labelDiv'>
                        <Input  
                            name="Job Name"
                            inputName='jobName'
                            placeholder ='Job Name' 
                            changeCallback = {(value,name)=>this.props.handleChange(value,name)}  
                            type = 'text'
                            defaultState = {this.props.jobName}
                            className='inputSyle'
                        />
                        < MultipleSelectInput
                            styles={this.props.style}
                            name='Shape'
                            selectName = "shape"
                            defaultValue = {this.props.shape}
                            multiple = {false}
                            productData = {this.props.shapeData} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                            selectClassName="selectInput"
                            multiselectSHeaderClassName="multiselectSHeaderClassName"
                        />
                        <span className='headerOfFilter'>Quantity</span>
                        <DropdownMenuWithFilter
                            name="quantity"
                            productData = {this.props.dataOfQuantity} 
                            btnTitle = {this.props.quantity}
                            selectQuantity={(value,name)=>this.props.handleSelect(value,name)}
                        />                     
                                {/* < MultipleSelectInput
                                    selectName = "Quantity"
                                    defaultValue = {dataForEditing.quantity}
                                    multiple = {false}
                                    productData = {Data} 
                                    handleClick = {(value,name)=>this.props.handleSelect(value,name)}  
                                    formClassName = "" 
                                    
                                /> */}
                    </div>
                    <div className='labelDiv'>
                        < MultipleSelectInput
                            name = "Material Type"
                            selectName = "materialType"
                            styles={this.props.style}
                            selectClassName="selectInput"
                            defaultValue = {this.props.materialType}
                            multiple = {false}
                            productData = {this.props.materialTypeData} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)} 
                            formClassName = "typeFormStyle"
                            multiselectSHeaderClassName="multiselectSHeaderClassName"
                        />
                        < MultipleSelectInput
                            styles={this.props.style}
                            name = "Size"
                            selectName = "size"
                            defaultValue = {this.props.size}
                            multiple = {false}
                            productData = {Data} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)} 
                            selectClassName="selectInput"
                            multiselectSHeaderClassName="multiselectSHeaderClassName"
                        />
                        < MultipleSelectInput
                            name = "Color"
                            selectName = "color"
                            styles={this.props.style}
                            defaultValue = {this.props.color}
                            multiple = {false}
                            productData = {this.props.colorData} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)} 
                            selectClassName="selectInput"
                            multiselectSHeaderClassName="multiselectSHeaderClassName"
                        />
                    </div>
                </div>
            </div>  
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    selectedProductId:state.selectedProductId  
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedProductId
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(ProductLabelSpcifications);
 