import React,{Component} from 'react'
import { FormControl,Dropdown } from 'react-bootstrap'
import { FaChevronDown } from "react-icons/fa";

export class DropdownMenuWithFilter extends Component {
    render(){
        const {productData} = this.props
        return(
    <div className="dropDownFilterContainer">
        <Dropdown name={this.props.name} id="dropdown-custom-components"  onSelect={(eventKey)=>this.props.selectQuantity(eventKey,this.props.name)} >
        <Dropdown.Toggle   as={CustomToggle}  class="dropdownToggle" >
            <span>{this.props.btnTitle}</span><FaChevronDown className="iconDown "/>
        </Dropdown.Toggle>
        <Dropdown.Menu   as={CustomMenu}  id='dropdown-button-drop-down' >
        {!this.props.productData ? [] :Object.values(productData).map((data,index)=>{
            return(
            <Dropdown.Item key={index} eventKey={data.quantity}>{data.quantity}</Dropdown.Item>
            )
        })}
        </Dropdown.Menu>
    </Dropdown>
  </div>
        )
    }
}
 
class CustomToggle extends React.Component {
  
    handleClick=(e)=> {
      e.preventDefault();
  
      this.props.onClick(e);
    }
  
    render() {
      return (
        <span  className='containerValuOfFilter'onClick={this.handleClick}>
          {this.props.children}
        </span>
      );
    }
  }
  
  class CustomMenu extends React.Component {
    
  
    state = { value: '' };
    
  
    handleChange=(e)=> {
      this.setState({ value: e.target.value.trim() });
    }
  
    render() {
      const {
        children,
        style,
        className,
        'aria-labelledby': labeledBy,
      } = this.props;
  
      const { value } = this.state;
      return (
        <div style={style} className={className} aria-labelledby={labeledBy}>
          <FormControl
            autoFocus
            className="filterField"
            placeholder="Type to filter..."
            onChange={this.handleChange}
            value={value}
          />
          <ul className="list-unstyled">
            {React.Children.toArray(children).filter(
              child =>
               
                 !value || child.props.children.toString().startsWith(value),
              
            )}
          </ul>
        </div>
      );
    }
  }
   