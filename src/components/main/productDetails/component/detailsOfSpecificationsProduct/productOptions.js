import React,{Component} from 'react'
import MultipleSelectInput from '../../../../helper/multipleSelectInput'


class ProductOptions extends Component{
    render(){
        return(
            <div className='optionContainer '>
                <p className={this.props.headerClassName} >{this.props.header}</p>   
                <div className='optionInputsContainer' >
                    <div className='optionDiv'>        
                        <MultipleSelectInput
                            name="Surface Finishing"
                            selectName = "surfaceFinishing"
                            defaultValue = {this.props.surfaceValue}
                            multiple = {false}
                            productData = {this.props.surfaceFinishingData} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                            formClassName = {this.props.surFaceClassName}
                            styles={this.props.style}
                            selectClassName="selectInput"
                        />   
                        <MultipleSelectInput
                            name="Spot UV Varnish"
                            selectName = "spotUvVarnish"
                            defaultState = {this.values}
                            multiple = {false}
                            productData = {this.props.varnishOptionsData} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                            formClassName = {this.props.spotClassName}
                            defaultValue = {this.props.spotUvVarnish}
                            styles={this.props.style}
                            selectClassName="selectInput"     
                        /> 
                         
                    </div>
                    <div className='optionDiv'> 
                    <MultipleSelectInput
                            name="Foil Stamping"
                            selectName = "foilStamping"
                            multiple = {false}
                            productData = {this.props.stampingOptionsData} 
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                            formClassName = {this.props.foilClassName}
                            defaultValue = {this.props.foilStamping}
                            styles={this.props.style}
                            selectClassName="selectInput"
                        />                    
                        <MultipleSelectInput
                            selectName = "numbering"
                            name="Variable Date"
                            defaultState = {this.values}
                            multiple = {false}
                            productData = {this.props.variableDataOptionsData}
                            handleClick = {(value,name)=>this.props.handleSelect(value,name)} 
                            formClassName = {this.props.numberingClassName}
                            defaultValue = {this.props.numbering}  
                            styles={this.props.style}
                            selectClassName="selectInput"
                        />               
                        
                    </div> 
                </div>                 
            </div>      
        )
    }
}
export default ProductOptions;