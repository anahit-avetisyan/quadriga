import React,{Component} from 'react'
import Button from '../../../../helper/button'
import DivWithSpans from '../../../../helper/divWithSpans'
import { connect } from "react-redux";
import ls from 'local-storage'
import { ToastContainer, toast } from 'react-toastify';
import { bindActionCreators } from "redux";
import {CartProduct,ResetInitialState,SelectedValues,SelectedValuesFailure} from '../../../../../reducers/action';
import {FetchProducts} from '../../../../../reducers/moduls/fetchProduct'

class ProcustPriceDetails extends Component {
    addToCart = () => {
        let {selectedValues,ResetInitialState,FetchProducts,SelectedValues,CartProduct} = this.props  
        let id = selectedValues.id
        let selectedDataForCart= ls.get('selectedStandardProduct') ? ls.get('selectedStandardProduct') : {}
        if(selectedValues.product){
            if(!selectedDataForCart[id]){
                selectedDataForCart[id]=selectedValues
                ls.set('selectedStandardProduct',selectedDataForCart)
            }else{
                toast.warning('You Have already had this product in cart')
                selectedDataForCart[id].name = selectedValues.name ? selectedValues.product :  selectedDataForCart[id].name ? selectedDataForCart[id].name : ''
                selectedDataForCart[id].jobName = selectedValues.jobName ? selectedValues.jobName :  selectedDataForCart[id].jobName ? selectedDataForCart[id].jobName : ''
                selectedDataForCart[id].quantity = selectedValues.quantity ? selectedValues.quantity :  selectedDataForCart[id].quantity ? selectedDataForCart[id].quantity : ''
                selectedDataForCart[id].shape = selectedValues.shape ? selectedValues.shape :  selectedDataForCart[id].shape ? selectedDataForCart[id].shape : ''
                selectedDataForCart[id].color = selectedValues.color ? selectedValues.color :  selectedDataForCart[id].color ? selectedDataForCart[id].color : ''
                selectedDataForCart[id].coreDiameter = selectedValues.coreDiameter ? selectedValues.coreDiameter :  selectedDataForCart[id].coreDiameter ? selectedDataForCart[id].coreDiameter : ''
                selectedDataForCart[id].digitalProof = selectedValues.digitalProof ? selectedValues.digitalProof :  selectedDataForCart[id].digitalProof ? selectedDataForCart[id].digitalProof : ''
                selectedDataForCart[id].foilStamping = selectedValues.foilStamping ? selectedValues.foilStamping :  selectedDataForCart[id].foilStamping ? selectedDataForCart[id].foilStamping : ''
                selectedDataForCart[id].gapBetweenLabels = selectedValues.gapBetweenLabels ? selectedValues.gapBetweenLabels :  selectedDataForCart[id].gapBetweenLabels ? selectedDataForCart[id].gapBetweenLabels : ''
                selectedDataForCart[id].height = selectedValues.height ? selectedValues.height :  selectedDataForCart[id].height ? selectedDataForCart[id].height : ''
                selectedDataForCart[id].labelOrientation = selectedValues.labelOrientation ? selectedValues.labelOrientation :  selectedDataForCart[id].labelOrientation ? selectedDataForCart[id].labelOrientation : ''
                selectedDataForCart[id].labelsPerRoll = selectedValues.labelsPerRoll ? selectedValues.labelsPerRoll :  selectedDataForCart[id].labelsPerRoll ? selectedDataForCart[id].labelsPerRoll : ''
                selectedDataForCart[id].materialType = selectedValues.materialType ? selectedValues.materialType :  selectedDataForCart[id].materialType ? selectedDataForCart[id].materialType : ''
                selectedDataForCart[id].maxRollDiameter = selectedValues.maxRollDiameter ? selectedValues.maxRollDiameter :  selectedDataForCart[id].maxRollDiameter ? selectedDataForCart[id].maxRollDiameter : ''
                selectedDataForCart[id].numbering = selectedValues.numbering ? selectedValues.numbering :  selectedDataForCart[id].numbering ? selectedDataForCart[id].numbering : ''
                selectedDataForCart[id].perforation = selectedValues.perforation ? selectedValues.perforation :  selectedDataForCart[id].perforation ? selectedDataForCart[id].perforation : ''
                selectedDataForCart[id].spotUvVarnish = selectedValues.spotUvVarnish ? selectedValues.spotUvVarnish :  selectedDataForCart[id].spotUvVarnish ? selectedDataForCart[id].spotUvVarnish : ''
                selectedDataForCart[id].surfaceFinishing = selectedValues.surfaceFinishing ? selectedValues.surfaceFinishing :  selectedDataForCart[id].surfaceFinishing ? selectedDataForCart[id].surfaceFinishing : ''
                selectedDataForCart[id].surfaceFinishingNew = selectedValues.surfaceFinishingNew ? selectedValues.surfaceFinishingNew :  selectedDataForCart[id].surfaceFinishingNew ? selectedDataForCart[id].surfaceFinishingNew : ''
                selectedDataForCart[id].textAreaValue = selectedValues.textAreaValue ? selectedValues.textAreaValue :  selectedDataForCart[id].textAreaValue ? selectedDataForCart[id].textAreaValue : ''
                selectedDataForCart[id].width = selectedValues.width ? selectedValues.width :  selectedDataForCart[id].width ? selectedDataForCart[id].width : ''
                selectedDataForCart[id].fileQuantityArtwork = selectedValues.fileQuantityArtwork ? this.state.fileQuantityArtwork :  selectedDataForCart[id].fileQuantityArtwork ? selectedDataForCart[id].fileQuantityArtwork : '' 
            }
        ResetInitialState()
        SelectedValues({})
        FetchProducts()
        ls.remove('selectedData')
        CartProduct(selectedDataForCart)
        window.scrollTo(0, 0);
        }else{
            window.scrollTo(0, 0);
            toast.error('Please choose pproduct')
        }
        ls.set('selectedStandardProduct',selectedDataForCart)
        console.log(selectedValues)
    }
    render(){
        return(
            <div className='priceDetailsContainer'>
                <ToastContainer />
                <div className='priceDataDetails'>
                    <DivWithSpans 
                        firstData='Price'
                        secondData= '$0'
                    />
                     <DivWithSpans 
                        firstData='Price per label:'
                        secondData= '$0'
                    />
                     <DivWithSpans 
                        firstData='Layout fee:'
                        secondData= '$0'
                    />
                     <DivWithSpans 
                        firstData='Production Proof:'
                        secondData= '$0'
                    />
                     <DivWithSpans 
                        firstData='Shipping Cost:'
                        secondData= '$0'
                    />
                     <DivWithSpans 
                        firstData='Sales Tax:'
                        secondData= '$0'
                    />
                     <DivWithSpans 
                        firstData='Total:'
                        secondData= '$0'
                    />    
                </div>
                    <div className='buttonContainer'>
                        <Button 
                            callback={this.addToCart}
                            className='customizedButton'
                            name="Add to Cart"
                        /> 
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    selectedValues:state.selectedValues  
    };
};
function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            CartProduct,
            ResetInitialState,
            SelectedValues,
            SelectedValuesFailure,
            FetchProducts
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(ProcustPriceDetails);
 