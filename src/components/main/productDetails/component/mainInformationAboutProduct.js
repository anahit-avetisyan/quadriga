import React,{Component} from 'react';
import ls from 'local-storage';
import MultipleSelectInput from '../../../helper/multipleSelectInput';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedProductId} from '../../../../reducers/action';
 

class   MainInformationAboutProduct extends Component { 
    state = {
        productValue:"",
        productDataAfterSelect:{},
        allData:{},
        defaultValue:''
    } 
    handleClick= (value,allData) => {
        console.log(value,'kkk')
        this.setState({defaultValue:'select option'})
        allData=this.state.allData
        Object.values(allData).forEach((data,index)=>{
            if(value===data.name){
                let selectedData={
                    id:data.id,
                    name:data.name
                }
                ls.set('selectedData',selectedData)
                this.props.SelectedProductId(ls.get('selectedData').id)
                this.setState({productDataAfterSelect:data,defaultValue:'Select option'})
                
            }   
        })
        this.setState({productValue:value})
    }
   
    componentDidMount=()=>{
        // getRequest('http://books.test/api/books?page=1')
        // .then(response => {
        //     this.setState({allData:response.payload})
        //     Object.values(response.payload).forEach((data,index)=>{
        //         let id=ls.get('selectedData') ? ls.get('selectedData').id : {}
        //          if(data.id===id){
        //              this.setState({defaultValue:data.name})
        //              return this.setState({ productDataAfterSelect : data });
        //          }
        //     })
        //     // return  this.setState({ productData : response.payload });
        // })
        // .catch(error => (error));

    };
    render(){
      const {allData} = this.state 
      const {productDataAfterSelect} =this.state
      console.log(productDataAfterSelect)
        return(
            <div className='productMainData'>
                <MultipleSelectInput
                     defaultValue ={productDataAfterSelect.name}
                     multiple = {false}
                     productData = {allData} 
                     handleClick = {this.handleClick} 
                     formClassName = "typeFormStyle"
                />
                <span>{productDataAfterSelect.name}</span>
                <img src = {`http://${productDataAfterSelect.image}`} alt = "img"  />
                <p>{productDataAfterSelect.price}</p>
                 
            </div>
        )
    }
}
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedProductId
        },
        dispatch
    );
};

export default connect(null,mapDispatchToProps)(MainInformationAboutProduct);
 