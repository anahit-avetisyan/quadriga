import React,{Component} from 'react';
import ProductLabelSpecifications from './detailsOfSpecificationsProduct/productLabelSpecifications';
import ProductOptions from './detailsOfSpecificationsProduct/productOptions';
import ProductRollParametrs from './detailsOfSpecificationsProduct/productRollParametrs';
import ProductShipping from './detailsOfSpecificationsProduct/productShipping';
import ProductOtherDetails from './detailsOfSpecificationsProduct/productOtherDetails';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedValues} from '../../../../reducers/action';

class SpecificationsOfProduct extends Component{
    state = {
        productData:{}  
    };
    handleSelect= (value,name) => {
        this.props.SelectedValues({[name]:value}) 
        this.setState({ [name]: value }) 

        this.props.SelectedValues({[name]: value})  
    };
    handleChange =(value,name)=>{
        let valuesOfInput = Object.assign({},this.props.selectedValues)
        valuesOfInput[name]=value
        this.setState({productData:valuesOfInput})
        this.props.SelectedValues(valuesOfInput)
    };
    selectValueFromInput = (value,name,e,closetoggle) =>{
        if(e.key === 'Enter') {
            let valuesOfInput = Object.assign({},this.props.selectedValues)
            valuesOfInput[name]=value
            this.props.SelectedValues(valuesOfInput)
            this.setState({[name]:value})
            closetoggle()
        }
    };
    changeToNotImportentForPerRoll =(closetoggle)=>{
        this.setState({labelsPerRoll:'Not importent'})
        let productData = {...this.props.selectedValues}
        productData.labelsPerRoll = 'Not importent'
        this.props.SelectedValues(productData)
        
        // let productData = {...this.state.productData}
        // productData.labelsPerRoll = 'Not importent'
        // this.setState({productData})
         
        closetoggle()
    };
    changeToNotImportentForMaxRoll =(closetoggle)=>{
        this.setState({maxRollDiameter:'Not importent'})
        let productData = {...this.props.selectedValues}
        productData.maxRollDiameter = 'Not importent'
        this.props.SelectedValues(productData)
        closetoggle()
    }
    render(){
        const {productData} = this.state
        const {selectedValues} = this.props
        return(
            <div className='productSpecificationsContainer'>
                <ProductLabelSpecifications
                    shape={productData.shape ? productData.shape : '' }
                    materialType={productData.materialType ? productData.materialType : ""}
                    size={productData.size ? productData.size : ""}
                    color={productData.color ? productData.color : ""}
                    style={this.props.style}
                    quantity= {productData.quantity}
                    handleChange={this.handleChange}
                    handleSelect={this.handleChange}
                    jobName={selectedValues  && Object.values(selectedValues).length !==0 ? selectedValues.jobName : ' ' }
                    jobNameValue = {selectedValues  && Object.values(selectedValues).length !==0 ? selectedValues.jobName : ' ' }
                    materialTypeData={this.props.materialTypeData}
                    dataOfQuantity = {this.props.dataOfQuantity} 
                    shapeData = {this.props.shapeData}
                    colorData={this.props.colorData}
                />
                <ProductOptions
                    style={this.props.style}
                    handleSelect={this.handleChange}
                    surFaceClassName='selectOptionStyle'
                    foilClassName='selectOptionStyle'
                    numberingClassName='selectOptionStyle'
                    spotClassName='selectOptionStyle'
                    header='Options'
                    headerClassName='optionHeader'
                    surfaceFinishingData={this.props.surfaceFinishingData}
                    surfaceValue={productData.surfaceFinishing ? productData.surfaceFinishing : ""}
                    foilStamping={productData.foilStamping ? productData.foilStamping : ""}
                    numbering={productData.numbering ? productData.numbering : ""}
                    spotUvVarnish={productData.spotUvVarnish ? productData.spotUvVarnish : ""}
                    stampingOptionsData={this.props.stampingOptionsData}
                    varnishOptionsData={this.props.varnishOptionsData}
                    variableDataOptionsData={this.props.variableDataOptionsData}
                />
                <ProductRollParametrs
                    inputInsideSelect='inputInsideSelectStandard'
                    inputInsideSelectFormContainer="inputInsideSelectFormContainerStandard"  
                    style={this.props.style}
                    mainInputInsideSelect='mainInputInsideSelect'
                    header='Roll Parameters'
                    headerClassName='optionHeader'
                    handleChange={this.handleChange}   
                    handleSelect={this.handleChange}
                    selectValue ={this.selectValueFromInput}
                    selectedDataForPerRoll={this.state.labelsPerRoll}
                    selectedDataForMaxRoll={this.state.maxRollDiameter}
                    changeToNotImportentForPerRoll = { this.changeToNotImportentForPerRoll}
                    changeToNotImportentForMaxRoll = { this.changeToNotImportentForMaxRoll}
                    labelOrientation={productData.labelOrientation ? productData.labelOrientation : ""}
                    gapBetweenLabels={productData.gapBetweenLabels ? productData.gapBetweenLabels : ""}
                    coreDiameter={productData.coreDiameter ? productData.coreDiameter : ""}
                    perforation={productData.perforation ? productData.perforation : ""}
                 />
                <ProductShipping
                    handleSelect={this.handleChange}
                    style={this.props.style}
                    shipping={productData.shipping ? productData.shipping : ""}
                />
                <ProductOtherDetails
                    style={this.props.style}
                    handleSelect={this.handleChange}
                    handleChange={this.handleChange}
                    digitalProof={productData.digitalProof ? productData.digitalProof : ""}
                />
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    selectedValues:state.selectedValues  
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedValues
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(SpecificationsOfProduct);
 