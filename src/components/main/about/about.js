import React,{Component} from 'react'
import './about.scss'
import Carousel from '../homePage/component/carousel'
// import {getRequest} from '../../helper/utils'


class About extends Component {
    state = {
        companyInformation:{}
    }
    componentDidMount() {     
        window.scrollTo(0, 0);
        // getRequest('url')
         // .then(response => {
    //  this.setState({companyInformation:response})
    // })
    // .catch(error => (error));

    }
    render(){
        return(
            <div className='aboutUsContainer'>
                <p className='header'>About us</p>
                <div>
                    {/* {!companyInformation ? null : Object.values(companyInformation).map ((information,index) => {
                        return(
                            <p className='information'>{information.data}</p>
                        )
                    }) } */}
                    <p className='information'>
                    Printed in full color with or without white support on the front side of adhesive-backed eggshell felt texture paper stock and mounted on a roll 
                    of carrier material with easy release. They are available in circle, oval, square and rectangle with rounded corners, and can be printed on eggshell
                    felt texture paper stocks.
                    </p>
                    <Carousel/>
                    <p className='information'>
                    Printed in full color with or without white support on the front side of adhesive-backed eggshell felt texture paper stock and mounted on a roll 
                    of carrier material with easy release. They are available in circle, oval, square and rectangle with rounded corners, and can be printed on eggshell
                    felt texture paper stocks.
                    </p>
                </div>
            </div>
        )
    }
}
export default About;