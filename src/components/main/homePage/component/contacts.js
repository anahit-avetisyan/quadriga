import React, {Component} from 'react'
import Facebook from '../../../../assets/icons/facebook.png'
import Google from '../../../../assets/icons/google+.png'
import Instagram from '../../../../assets/icons/instagram.png'
// import {Link} from 'react-router-dom'

class Contacts extends Component {
    render(){
        return(
            <div className='contactsShortInformationContainer'>
                <div className='contactsHeaderField'>
                    <span>Contacts</span>
                </div>
                <div className='contactsDataContainer'>
                    {/* {!this.props.contactsData ? null : Object.values(this.props.data).map((data,index)=>{
                        return(
                            <span key = {index}>{data}</span>
                        )
                        
                    }) 
                    } */}
                    <div className='phoneNumberAdressContainer'>
                        <span className='addressCompany'>
                        Address: 28410 Witherspoon Parkway
                        Valencia, CA 91355  
                        </span>
                        <span>
                        P: +1 (888) 669 9994
                        </span>
                    </div>
                    <div className='supportMarketingContactsContainer'>
                        <span className='supportContact'>Support: operator@quadrigausa.com </span>
                        <span>Marketing: accounting@quadrigausa.com</span>
                    </div>
                    <span className='socialInformationHeader'>Find us also </span>
                    <div className='socialInformationContainer'>
                         {/* {!this.props.socialData ? null : Object.values(this.props.data).map((data,index)=>{
                             return(
                                 <a key = {index} target='blank' href={data.link}><img src={`http://${data.image}`} alt='img'/></a>
                             )
                            
                            }) 
                        } */}
                        <a target='blank' href='https://www.facebook.com/'><img src={Facebook} alt='facebook'/></a>
                        <a target='blank' href='https://www.facebook.com/'><img src={Google} alt='Google+'/></a> 
                        <a target='blank' href='https://www.facebook.com/'><img src={Instagram} alt='Instagram'/></a> 
                    </div>
                </div>
            </div>
        )
    }
}
export default Contacts;