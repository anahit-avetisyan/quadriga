import React,{Component} from 'react';
// import Polygon from '../../../../assets/icons/Polygon_3.png'
import Product from '../../product/product';
import history from '../../../../routers/history'


class Products extends Component {
    productPage = () => {
        history.push('/products')
    }
    render(){
        const {productData}=this.props
        return(
            <div >
                <div className='productHeaderField'>
                  <span >All Roll Label Products</span>
                </div>
                <div className='productContainerHome'>
                    {productData && Object.keys(productData)!==0 ? Object.values(productData).map((data,index)=>{
                    return  <Product key = {index} dataAboutProduct={data}/> 
                }):<Product dataAboutProduct={{}} />}
  
                </div>
                <div className='moreProductButton'>
                    <span onClick={this.productPage}>See more</span>
                </div>
            </div>
        )
    }
}
export default Products