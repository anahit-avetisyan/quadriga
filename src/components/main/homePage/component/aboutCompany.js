import React, {Component} from 'react';
// import Polygon from '../../../../assets/icons/Polygon_3.png'
import  Carousel from './carousel'
import history from '../../../../routers/history'


class AboutCompany extends Component {
    aboutCompany = () => {
        history.push('/about')
    }
    render(){
        return(
            <div className='aboutCompanyContainer' >
            <div className='aboutCompanyHeaderField'>
        
              <span>About us</span>
            </div>
            <div className='aboutCompanyInformation'>
                <p className='textAboutCompany'>Printed in full color with or without white support on the front side of adhesive-backed eggshell felt texture paper stock and mounted on a roll of carrier material with easy release. They are available in circle, oval, square and rectangle with rounded corners, and can be printed on eggshell felt texture paper stocks.</p>
                <div className='carouselContainer'>
                    <Carousel/>
                </div>   
            </div>
            <div className='moreInformationButton'>
                <span onClick={this.aboutCompany}>See more</span>
            </div>
        </div>
        )
    }
}
export default AboutCompany;