import React,{Component} from "react";
import { MDBCarousel, MDBCarouselInner, MDBCarouselItem, MDBView, MDBMask, MDBContainer } from
"mdbreact";
import "mdbreact/dist/css/mdb.css";

class Carousel extends Component {
    
    render(){
        return (
            
                <div className='carouselContent'>
                    <MDBContainer>
                    <MDBCarousel
                        activeItem={1}
                        // length = {Object.keys(this.props.carouselData).length}
                        length={4}
                        showControls={true}
                        showIndicators={true}
                        className="z-depth-1"
                    >
                    <MDBCarouselInner>
                    {/* {!this.props.carouselData ? {} : Object.values(this.props.carouselData).map((data,index)=>{
                        return(
                            <MDBCarouselItem itemId={DataCue.id}>
                        <MDBView>
                            <img
                            className="d-block w-100"
                             src = {`http://${data.image}`}  
                            alt=""
                            />
                        <MDBMask overlay="black-light" />
                        </MDBView>
                        {/*  MDBCarouselCaption add in import line if need
                         <MDBCarouselCaption>
                            <h3 className="h3-responsive">Light mask</h3>
                            <p>First text</p>
                        </MDBCarouselCaption> 
                        </MDBCarouselItem>
                        )
                    })} */}
                        <MDBCarouselItem itemId="1">
                        <MDBView>
                            <img
                            className="d-block w-100"
                            src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
                            alt="First slide"
                            />
                        <MDBMask overlay="black-light" />
                        </MDBView>
                        {/*  MDBCarouselCaption add in import line if need
                         <MDBCarouselCaption>
                            <h3 className="h3-responsive">Light mask</h3>
                            <p>First text</p>
                        </MDBCarouselCaption> */}
                        </MDBCarouselItem>
                        <MDBCarouselItem itemId="2">
                        <MDBView>
                            <img
                            className="d-block w-100"
                            src="https://mdbootstrap.com/img/Photos/Slides/img%20(6).jpg"
                            alt="Second slide"
                            />
                        <MDBMask overlay="black-strong" />
                        </MDBView>
                        {/* <MDBCarouselCaption>
                            <h3 className="h3-responsive">Strong mask</h3>
                            <p>Second text</p>
                        </MDBCarouselCaption> */}
                        </MDBCarouselItem>
                        <MDBCarouselItem itemId="3">
                        <MDBView>
                            <img
                                className="d-block w-100"
                                src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg"
                                alt="Third slide"
                            />
                        <MDBMask overlay="black-slight" />
                        </MDBView>
                        {/* <MDBCarouselCaption>
                            <h3 className="h3-responsive">Slight Mast</h3>
                            <p>Third text</p>
                        </MDBCarouselCaption> */}
                        </MDBCarouselItem>
                        <MDBCarouselItem itemId="4">
                        <MDBView>
                            <img
                            className="d-block w-100"
                            src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg"
                            alt="Third slide"
                            />
                        <MDBMask overlay="black-slight" />
                        </MDBView>
                        {/* <MDBCarouselCaption>
                            <h3 className="h3-responsive">Slight Mast</h3>
                            <p>Fourth text</p>
                        </MDBCarouselCaption> */}
                        </MDBCarouselItem>
                    </MDBCarouselInner>
                    </MDBCarousel>
                    </MDBContainer>
                </div>
                
          ); 
    }
}
  


export default Carousel;