import React, {Component} from 'react'
import Carousel from './component/carousel'
import Products from './component/products';
import './homeContainerStyle.scss'
import AboutCompany from './component/aboutCompany';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {FetchProducts} from "../../../reducers/moduls/fetchProduct";

class HomePageContainer extends Component {
   
    componentDidMount = () => {
        const {FetchProducts} = this.props
        FetchProducts()
        window.scrollTo(0, 0);
    }
    render(){
        const {products} = this.props
        return(
            <div className='homeContainer'>
                <div className='carouselContainer'>
                    <Carousel
                    
                    />  
                </div>
                
                <Products productData={products} />
                <AboutCompany/>
            </div>

        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    products : state.productsReducer ? state.productsReducer.products :{}
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            FetchProducts
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(HomePageContainer);