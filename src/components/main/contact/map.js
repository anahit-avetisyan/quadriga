import React,{Component} from 'react';
import { Map, GoogleApiWrapper,Marker} from 'google-maps-react';

class MapComponent extends Component {
    mapStyles = {
        maxWidth: '1200px',
        width:'65%',
        height: '100%',
        maxHeight:'760px'
      };
    render() {
        return(
            <Map      
                google={this.props.google}
                zoom={15}
                style={this.mapStyles}
                initialCenter={{ lat: 34.4464595,  lng:  -118.6332275}}
            >
                <Marker 
                    google={this.props.google}
                    position={{ lat: 34.4464595,  lng:  -118.6332275}}
                />
                
            </Map>
        )
    }
}
export default GoogleApiWrapper({
    apiKey: 'AIzaSyBHJicFBWE1LZpw75y9Qzp_AtXYiewiGms'
  }) (MapComponent);