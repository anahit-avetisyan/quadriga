import React,{Component} from 'react'
import './contact.scss';
// import {getRequest} from '../../helper/utils';
import Facebook from '../../../assets/icons/facebook.png';
import Google from '../../../assets/icons/google+.png';
import Instagram from '../../../assets/icons/instagram.png';
import { Map, GoogleApiWrapper,Marker} from 'google-maps-react';
// import Button from '../../helper/button';
import history from '../../../routers/history'


class Contact extends Component {
    state = {
        contactData:{}
    }
    componentDidMount() {
        // getRequest(url)
        // .then(response => {
        //     this.setState({contactData:response.payload})
        // })
        // .catch(error => (error));
        window.scrollTo(0, 0);
    }
    mapStyles = {
        maxWidth:'1200px',
        width:'65%',
        height: '100%',
        marginBottom:'48px'
      };
      openMap = () => {
          history.push('/contact/map')
      }
    render(){
        return(
            <div className="contactsContainer">
                <p className='contactHeader'>Contacts</p>
                <div className='contactsDataContainer'>
                    {/* {!this.props.contactsData ? null : Object.values(this.props.data).map((data,index)=>{
                        return(
                            <p key = {index}>{data}</p>
                        )
                        
                    }) 
                    } */}
                    <div className='contactInformationContainer'>
                        <p>
                            P: +1 (888) 669 9994
                        </p>
                        <p>
                            Support: operator@quadrigausa.com 
                        </p>
                        <p>
                            Marketing: accounting@quadrigausa.com
                        </p>
                    </div>
                    <div className='socialInformationContainer'>
                        <span className='socialInformationHeader'>Find us also </span>
                         {/* {!this.props.socialData ? null : Object.values(this.props.data).map((data,index)=>{
                             return(
                                 <a key = {index} target='blank' href={data.link}><img src={`http://${data.image}`} alt='img'/></a>
                             )
                            
                            }) 
                        } */}
                        <a target='blank' href='https://www.facebook.com/'><img src={Facebook} alt='facebook'/></a>
                        <a target='blank' href='https://www.facebook.com/'><img src={Google} alt='Google+'/></a> 
                        <a target='blank' href='https://www.facebook.com/'><img src={Instagram} alt='Instagram'/></a> 
                    </div>
                    
                </div> 
                <div  >
                    <Map
                         className="mapContainer"
                        google={this.props.google}
                        zoom={15}
                        style={this.mapStyles}
                        initialCenter={{ lat: 34.4464595,  lng:  -118.6332275}}
                    >
                        <Marker 
                            google={this.props.google}
                            position={{ lat: 34.4464595,  lng:  -118.6332275}}
                        />

                    </Map>
                </div>
               
            </div>
        )
    }
}
export default GoogleApiWrapper({
    apiKey: 'AIzaSyBHJicFBWE1LZpw75y9Qzp_AtXYiewiGms'
  })(Contact);