import React,{Component} from 'react'
import './shippingAddressesStyle.scss'
import ModalForAddingAddress from './component/modalForAddingAddress'
import ShippingAddress from './component/shippingAddress';
import ls from 'local-storage'
// import {request} from '../../helper/utils'
 

class ShippingAddressesContainer extends Component { 
    state={
        checked : false,
        checkedValue:'',
        createAddress:{},
        shippingAddresses:{}
    }
    //show data in browser after first rendering
    componentDidMount = () =>{
        let shippingAddressesFromLs = ls.get('addresses') ? ls.get('addresses') : {}
        this.setState({shippingAddresses:shippingAddressesFromLs})
    }
    // update data after change checkbox result
    componentDidUpdate=(prevProps,prevState)=>{
        if(this.state.checked!==prevState.checked){
            if(this.state.checked===true){
              this.setState({checkedValue:'Request blind shipping'})
            }else{
                this.setState({checkedValue:''})  
            }
        }
    }
    //add inputs values in state 
    inputValue = (value,name) => {
        let valuesOfInput= Object.assign({}, this.state.createAddress); 
        valuesOfInput[name] = value
        this.setState({
            createAddress: valuesOfInput
        }); 
    }  
    changeCheckBox = () => {
        this.setState(prevState => ({
            checked: !prevState.checked
          }));
          
        }
        //add address in page 
    createAddress = (closeModal) => { 
        const {createAddress} = this.state
        const {checkedValue} = this.state
        let id = Math.floor(Math.random() * Math.floor(100))

        let createAddresses = ls.get('addresses') ? ls.get('addresses') : {}
        createAddresses[id]={
            id:id,
            companyName : createAddress.companyName,
            fullName : createAddress.fullName,
            streetAddress : createAddress.streetAddress,
            building : createAddress.building,
            city : createAddress.city,
            country : createAddress.country,
            zipCode : createAddress.zipCode,
            state : createAddress.state,
            phoneNumber : createAddress.phoneNumber,
            request : checkedValue

        }
        ls.set('addresses',createAddresses)
        this.setState({shippingAddresses:createAddresses})
        closeModal()
        //make request send data for adding
        // request(url,"POST",data,header)
        // .then(response =>{
        //     console.log(response)
        // })
        }
     //delete address from page    
    deleteAddress = (id) => {
        let addressesFromLs =  ls.get('addresses') ? ls.get('addresses') : {}
        Object.values(addressesFromLs).forEach((data)=>{
        if(data.id===id){
            delete addressesFromLs[data.id]
        }
        this.setState({shippingAddresses:addressesFromLs})
        ls.set('addresses',addressesFromLs)
        })
          //make request send data for adding
        // request(url,"DELETE",data,header)
        // .then(response =>{
        //     console.log(response)
        // })
    }
    //edit address data
    editAddress = (closeModal,id) => {
        const {createAddress} = this.state
        const {checkedValue} = this.state
        let addressesFromLs =  ls.get('addresses') ? ls.get('addresses') : {}
        if(addressesFromLs[id]){
            addressesFromLs[id]={
                id:id,
                companyName : createAddress.companyName ? createAddress.companyName : addressesFromLs[id].companyName,
                fullName : createAddress.fullName ? createAddress.fullName : addressesFromLs[id].fullName,
                streetAddress : createAddress.streetAddress ? createAddress.streetAddress : addressesFromLs[id].streetAddress ,
                building : createAddress.building ? createAddress.building : addressesFromLs[id].building ,
                city : createAddress.city ? createAddress.city : addressesFromLs[id].city,
                country : createAddress.country ? createAddress.country : addressesFromLs[id].country,
                zipCode : createAddress.zipCode ? createAddress.zipCode : addressesFromLs[id].zipCode ,
                state : createAddress.state ? createAddress.state : addressesFromLs[id].state,
                phoneNumber : createAddress.phoneNumber ? createAddress.phoneNumber : addressesFromLs[id].phoneNumber,
                request : checkedValue ? checkedValue : addressesFromLs[id].checkedValue
    
            }   
        }
        this.setState({shippingAddresses:addressesFromLs})
        ls.set('addresses',addressesFromLs)
        closeModal()
          //make request send data for adding
        // request(url,"PUT",data,header)
        // .then(response =>{
        //     console.log(response)
        // })
    }
   
    render(){
        const {shippingAddresses} = this.state
        return(
            <div className='shippingAdressesContainer'>
               
                <h1>Your Account: Pick up / Shipping Address </h1>
                <ModalForAddingAddress
                    addAdress={this.createAddress}
                    addData = {this.inputValue}
                    checked={this.state.checked}
                    changeCheckBox={this.changeCheckBox}
                />
                <div className='shippingAdressesContent'>
                    {Object.values(shippingAddresses).map((data,index)=>{
                        return(
                            <ShippingAddress
                                key={index}
                                data={data}
                                index={index}
                                deleteAddress={this.deleteAddress}
                                editAddress={(toogle)=>this.editAddress(toogle,data.id)}
                                addData={this.inputValue}
                                checked={this.state.checked}
                                changeCheckBox={this.changeCheckBox}
                            />
                        )
                    })}
                  
                </div>
                <ModalForAddingAddress/>
            </div>
        )
    }
}
export default ShippingAddressesContainer;