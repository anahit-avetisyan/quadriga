import React,{Component} from 'react'
import MainModal from '../../../helper/mainModal.js';
import Input from '../../../helper/input'
import  MultipleSelectInput from '../../../helper/multipleSelectInput'
import Data from '../../../helper/productData'



class ModalForAddingAddress extends Component {
    render(){
        return(
            <MainModal
                mainDivClassName = 'modalForAddresses'
                toggleName = 'Add Address'
                modalTitle = 'ADD A NEW ADDRESS'
                editData ={(toogle)=>this.props.addAdress(toogle)}
                cancel = 'Cancel'
                save = 'Add Address'
            >
             <div className="modalContentContainer">
                 <div>
                     <Input
                        className = 'modalInputClassName'
                        name = 'Company Name / Code' 
                        inputName = 'companyName'
                        placeholder ='Your customer company name'
                        type='text'
                        changeCallback = {(value,name)=>this.props.addData(value,name)}

                     />
                       <Input
                        name = 'Full Name / Nick Name' 
                        inputName = 'fullName'
                        placeholder ='Full Name'
                        type='text'
                        changeCallback = {(value,name)=>this.props.addData(value,name)}
                        className = 'modalInputClassName'

                     />
                      <Input
                        className = 'modalInputClassName'
                        name = 'Street address' 
                        inputName = 'streetAddress'
                        placeholder ='Street address'
                        type='text'
                        changeCallback = {(value,name)=>this.props.addData(value,name)}

                     />
                      <Input
                       className = 'modalInputClassName'
                        inputName = 'building'
                        placeholder ='Apartment,Suite,Unit,Building,Floor,etc'
                        type='text'
                        changeCallback = {(value,name)=>this.props.addData(value,name)}

                     />
                    <Input
                        className = 'modalInputClassName'
                        name = 'City' 
                        inputName = 'city'
                        type='text'
                        changeCallback = {(value,name)=>this.props.addData(value,name)}

                     />
                 </div>
                <div className='containerOfSelectInput'>
                    <div>
                        <MultipleSelectInput
                            formClassName = 'modalInputClassName'
                            name = 'Country'
                            selectName = "country"
                            multiple = {false}
                            productData = {Data} 
                            handleClick = {(value,name)=>this.props.addData(value,name)}
                         />
                         <Input
                            name='ZIP' 
                            inputName='zipCode'
                            placeholder='Your customer company name'
                            type='text'
                            changeCallback={(value,name)=>this.props.addData(value,name)}

                     />
                     </div>
                     <div>
                        <MultipleSelectInput
                            formClassName = 'modalInputClassName'
                            name ='State / Province / Region'
                            selectName = "state"
                            multiple = {false}
                            productData = {Data} 
                            handleClick = {(value,name)=>this.props.addData(value,name)}
                        />
                        <Input
                            name = 'Phone Number' 
                            inputName = 'phoneNumber'
                            placeholder ='Phone Number'
                            type='text'
                            changeCallback = {(value,name)=>this.props.addData(value,name)}

                     />
                     </div>
                 </div>
                 <div>
                     <label>
                        <input 
                            name='checkbox'
                            type='checkbox'
                            checked={this.props.checked}
                            onChange={()=>this.props.changeCheckBox() }
                        />
                        Request blind shipping
                     </label> 
                 </div>
            </div>  
            </MainModal>
        )
    }
}
export default ModalForAddingAddress;