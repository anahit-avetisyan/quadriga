import React,{Component} from 'react';
import EditModal from './editModal';
import Button from '../../../helper/button';

class ShippingAddress extends Component {
    render(){
        const {data , index} = this.props
        return(
            <div key = {index} className='shippingAddresses'>
                <div className='firstDiv'>
                    <span>Company Name:{data.companyName}</span>
                    <span>{data.request}</span>
                </div>
                <div className='secondDiv'>
                    <span>Company Name:{data.companyName}</span>
                    <span>Name:{data.fullName}</span>
                    <span>Address:{data.city},{data.country},{data.state},{data.streetAddress}</span>
                    <span>ZIP:{data.zipCode}</span>
                    <span>Phone Number:{data.phoneNumber}</span>
                </div>
                <div className='thirdDiv'>
                    <EditModal
                        data={data}
                        editAddress = {(toogle)=>this.props.editAddress(toogle)}
                        addData={(value,name)=>this.props.addData(value,name)}
                        checked={this.props.checked}
                        changeCheckBox={()=>this.props.changeCheckBox() }
                    />
                    <Button
                        name='Delete'
                        callback={()=>this.props.deleteAddress(data.id)}
                    />
                </div>
            </div>
        )
    }
}
export default ShippingAddress;