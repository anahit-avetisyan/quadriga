import React,{Component} from 'react'
import MainModal from '../../../helper/mainModal.js';
import Input from '../../../helper/input'
import  MultipleSelectInput from '../../../helper/multipleSelectInput'
import Data from '../../../helper/productData'


class EditModal extends Component{
    state={
        checked:false,
        checkedValue:'',
        createAddress:{}
    }
    addData = (value,name) => {
        let valuesOfInput= Object.assign({}, this.state.createAddress); 
        valuesOfInput[name] = value
        this.setState({
            createAddress: valuesOfInput
        });
        //values in one object
        // let addingData = {}
        // addingData[name]=value
        // this.setState({[name]:value})
    }
    changeCheckBox = () => {
        this.setState(prevState => ({
            checked: !prevState.checked
          }));
          
        }
    
    componentDidUpdate=(prevProps,prevState)=>{
        if(this.state.checked!==prevState.checked){
            if(this.state.checked===true){
              this.setState({checkedValue:'Request blind shipping'})
            }else{
                this.setState({checkedValue:''})  
            }
        }
    }
    editAdress = () => {
        let data = this.state.createAddress
        console.log(data,'states a')
        //make request send data for adding
        // request(url,"POST",data,header)
        // .then(response =>{
        //     console.log(response)
        // })
    }
    render(){
        const {data} = this.props
        return(
            <MainModal
                mainDivClassName = 'modalForEditingAddress'
                toggleName = 'Edit'
                modalTitle = 'EDIT ADDRESS'
                editData ={(toogle)=>this.props.editAddress(toogle)}
                cancel = 'Cancel'
                save = 'Save'
            >
                <div className="modalContentContainer">
                    <div>
                        <Input
                            defaultState = {data.companyName}
                            className = 'modalInputClassName'
                            name = 'Company Name / Code' 
                            inputName = 'companyName'
                            placeholder ='Your customer company name'
                            type='text'
                            changeCallback={(value,name)=>this.props.addData(value,name)}

                        />
                        <Input
                            defaultState = {data.fullName}
                            name = 'Full Name / Nick Name' 
                            inputName = 'fullName'
                            placeholder ='Full Name'
                            type='text'
                            changeCallback={(value,name)=>this.props.addData(value,name)}
                            className = 'modalInputClassName'

                        />
                        <Input
                            defaultState = {data.streetAddress}
                            className = 'modalInputClassName'
                            name = 'Street address' 
                            inputName = 'streetAddress'
                            placeholder ='Street address'
                            type='text'
                            changeCallback={(value,name)=>this.props.addData(value,name)}

                        />
                        <Input
                            defaultState = {data.streetAddress}
                            className = 'modalInputClassName'
                            inputName = 'companyName'
                            placeholder ='Apartment,Suite,Unit,Building,Floor,etc'
                            type='text'
                            changeCallback={(value,name)=>this.props.addData(value,name)}

                        />
                        <Input
                            defaultState = {data.city}
                            className = 'modalInputClassName'
                            name = 'City' 
                            inputName = 'city'
                            type='text'
                            changeCallback={(value,name)=>this.props.addData(value,name)}

                        />
                    </div>
                    <div className='containerOfSelectInput'>
                        <div>
                            <MultipleSelectInput
                                defaultValue={data.country}
                                formClassName = 'modalInputClassName'
                                name = 'Country'
                                selectName = "country"
                                multiple = {false}
                                productData = {Data} 
                                handleClick={(value,name)=>this.props.addData(value,name)}
                            />
                            <Input
                                defaultState = {data.zipCode}
                                name = 'ZIP' 
                                inputName = 'zipCode'
                                placeholder ='Your customer company name'
                                type='text'
                                changeCallback={(value,name)=>this.props.addData(value,name)}

                        />
                        </div>
                        <div>
                            <MultipleSelectInput
                                defaultValue={data.state}
                                formClassName = 'modalInputClassName'
                                name ='State / Province / Region'
                                selectName = "state"
                                multiple = {false}
                                productData = {Data} 
                                handleClick={(value,name)=>this.props.addData(value,name)}
                            />
                            <Input
                                defaultState = {data.phoneNumber}
                                name = 'Phone Number' 
                                inputName = 'phoneNumber'
                                placeholder ='Phone Number'
                                type='text'
                                changeCallback={(value,name)=>this.props.addData(value,name)}

                        />
                        </div>
                    </div>
                    <div>
                        <label>
                            <input 
                                name='checkbox'
                                type='checkbox'
                                checked={this.props.checked}
                                onChange={()=>this.props.changeCheckBox() }
                               
                            />
                            Request blind shipping
                        </label> 
                    </div>
                </div>  
        </MainModal>
        )
    }
}
export default EditModal;