import React, {Component} from 'react'
import CustomizedProduct from './component/informationAboutProduct'
import './customizedProductStyle.scss'
import CustomizedProductDetails from './component/customizedProductDetails';
import ls from 'local-storage'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedProductId,CartProduct,ResetInitialState} from "../../../reducers/action";
import {FetchProducts,FetchMaterialTypes} from '../../../reducers/moduls/fetchProduct'
import PriceTable from './component/priceTable';
import {SelectedValues} from '../../../reducers/action';
 



class CustomizedProductContainer extends Component {
  
        
    state ={
        materialTypeData: {},
        selectedValueWithInput:"" ,
        customizedProductData:{},
        dataChoosen:{},
        selectedOption:null,
        labelSpecification : true,
        option : false,
        roll : false,
        others : false,
        quantityShow:false,
        shippingAddress:'Quadriga USA Enterprises, Inc. Valencia CA 91355',
        filesUpload:{},
        fileQuantityArtwork:0,
        files:{},
        filesQuantity:0,
        shippingAddresNew:'Quadriga USA Enterprises, Inc. Valencia CA 91355',
        choosenProduct:{},
        showPricTable:false,
        selectedstyleName:-1,
        sizeData:{},
        heightData:[],
        widthData:[],
        materialtypedef:'sasas'
        
    }

    displayData = [];
    componentDidMount=()=> {
        let newAddress = Object.assign({},this.state.customizedProductData)
        newAddress={
            shippingAddress:this.state.shippingAddress
        }
        this.setState({customizedProductData:newAddress})
        const {products,SelectedValues,selectedValues} = this.props
        let selectedDataFromLs = ls.get("selectedData") ?  ls.get("selectedData") : {}
        let data = ls.get("selectedProductData") ?  ls.get("selectedProductData") : {}
        let id = selectedDataFromLs.id
        if(selectedDataFromLs.specificId){
              return  Object.values(data).map(selectedData=>{
                    if(selectedDataFromLs.specificId===selectedData.specificId){
                        console.log(selectedData,'kkkk')
                        let dataForChanging=Object.assign({},this.state.customizedProductData)
                        dataForChanging={
                            specificId:selectedData.specificId.specificId,
                            id:selectedData.id,
                            fileQuantityArtwork:selectedData.fileQuantityArtwork,
                            product:selectedData.name,
                            jobName:selectedData.jobName,
                            quantity:selectedData.quantity,
                            shape:selectedData.shape,
                            color: selectedData.color, 
                            coreDiameter: selectedData.coreDiameter,
                            productionSample: selectedData.productionSample,
                            foilStamping: selectedData.foilStamping,
                            labelDirection: selectedData.labelDirection,
                            height: selectedData.height,
                            rollFinishing: selectedData.rollFinishing,
                            labelsPerRoll: selectedData.labelsPerRoll,
                            materialType: selectedData.materialType,
                            maxRollDiameter: selectedData.maxRollDiameter,
                            numbering: selectedData.numbering,
                            perforation : selectedData.perforation,
                            spotUvVarnish: selectedData.spotUvVarnish,
                            surfaceFinishing: selectedData.surfaceFinishing,
                            shippingMethod: selectedData.shippingMethod,
                            textAreaValue: selectedData.textAreaValue,
                            width: selectedData.width,
                            shippingAddress : selectedData.shippingAddress ? selectedData.shippingAddress : "Quadriga USA Enterprises, Inc. Valencia CA 91355"
                        }
                        return this.setState({customizedProductData:dataForChanging})
                    }
                })
        }
        if(products){
            Object.values(products).forEach(data => {
                let customizedDefaultValue = Object.assign({},this.state.customizedProductData)
                customizedDefaultValue={
                    product:data.long_name
                }
                if(data.id===id){
                    this.setState({choosenProduct:data,productDefaultValue:data.long_name,customizedProductData:customizedDefaultValue})
                }
                 
            })
        }else {
            this.setState({choosenProduct:{},productDefaultValue:''})
        }
        let selectedData = Object.assign({},selectedValues)
        selectedData = {
            product: selectedDataFromLs.product,
            id: id,
            date:this.state.date
        }
        ls.set('selectedData',selectedData)
        SelectedValues(selectedData)
        
        window.scrollTo(0, 0);
    }
    componentDidUpdate =(prevProps,prevState)=>{
        if(this.state.customizedProductData.product!==prevState.customizedProductData.product){
           
            const {customizedProductData}=this.state
            
            const {products} = this.props
            if(products){
                Object.values(products).forEach((data)=>{
                    if(customizedProductData.product===data.long_name){
                        this.setState({choosenProduct:data,id:data.id,})
                        let selectedData = {
                            product: data.long_name,
                            id: data.id,
                            date:this.state.date
                        }
                        ls.set('selectedData',selectedData)
                    }
                })
            }
        }
        if(this.props.products !== prevProps.products){
            
            let selectedData = ls.get("selectedData") ?  ls.get("selectedData") : {}
            let id = selectedData.id
            const {products } = this.props
            if(products){
                Object.values(products).forEach(data => {
                    let customizedDefaultValue = Object.assign({},this.state.customizedProductData)
                    customizedDefaultValue={
                        product:data.long_name
                    }
                    if(data.id===id){
                        
                        this.setState({choosenProduct:data,productDefaultValue:data.long_name,customizedProductData:customizedDefaultValue})
                        }
                        
                })
            }else {
                this.setState({choosenProduct:{},productDefaultValue:''})
            }
         
        }
        if(this.state.customizedProductData.shape!==prevState.customizedProductData.shape){
            const {shape} = this.state.customizedProductData
            const {size}=this.props
            let selectedSize=Object.values(size).filter(data=>{
                return data.shape===shape
            })
            var dataHeight=[]
            var dataWidth=[]
            Object.values(selectedSize).map(data=>{
                let height=data.height
                let width=data.width
                dataHeight.push(height)
                return dataWidth.push(width)

            })
            this.setState({
                sizeData:selectedSize, 
                heightData:dataHeight,
                widthData:dataWidth,

            })
        }
        //add anem zaprosi pahy 
        if(this.state.customizedProductData!==prevState.customizedProductData){
        }
    }
    sendCustomOrder=()=>{
        const {customizedProductData,id} = this.state
        let data = (ls.get('selectedProductData') && Object.keys(ls.get('selectedProductData')).length!==0) ? ls.get('selectedProductData') : {}
        var randomNumber = Math.round(Math.random() * 10);
        if(customizedProductData && customizedProductData.product){
            if(data[randomNumber]!== undefined){
                data[randomNumber].name = customizedProductData.name ? customizedProductData.product :  data[customizedProductData.product].name ? data[customizedProductData.product].name : ''
                data[randomNumber].jobName = customizedProductData.jobName ? customizedProductData.jobName :  data[customizedProductData.product].jobName ? data[customizedProductData.product].jobName : ''
                data[randomNumber].quantity = customizedProductData.quantity ? customizedProductData.quantity :  data[customizedProductData.product].quantity ? data[customizedProductData.product].quantity : ''
                data[randomNumber].shape = customizedProductData.shape ? customizedProductData.shape :  data[customizedProductData.product].shape ? data[customizedProductData.product].shape : ''
                data[randomNumber].color = customizedProductData.color ? customizedProductData.color :  data[customizedProductData.product].color ? data[customizedProductData.product].color : ''
                data[randomNumber].coreDiameter = customizedProductData.coreDiameter ? customizedProductData.coreDiameter :  data[customizedProductData.product].coreDiameter ? data[customizedProductData.product].coreDiameter : ''
                data[randomNumber].productionSample = customizedProductData.productionSample ? customizedProductData.productionSample :  data[customizedProductData.product].productionSample ? data[customizedProductData.product].productionSample : ''
                data[randomNumber].foilStamping = customizedProductData.foilStamping ? customizedProductData.foilStamping :  data[customizedProductData.product].foilStamping ? data[customizedProductData.product].foilStamping : ''
                data[randomNumber].labelDirection = customizedProductData.labelDirection ? customizedProductData.labelDirection :  data[customizedProductData.product].labelDirection ? data[customizedProductData.product].labelDirection : ''
                data[randomNumber].height = customizedProductData.height ? customizedProductData.height :  data[customizedProductData.product].height ? data[customizedProductData.product].height : ''
                data[randomNumber].rollFinishing = customizedProductData.rollFinishing ? customizedProductData.rollFinishing :  data[customizedProductData.product].rollFinishing ? data[customizedProductData.product].rollFinishing : ''
                data[randomNumber].labelsPerRoll = customizedProductData.labelsPerRoll ? customizedProductData.labelsPerRoll :  data[customizedProductData.product].labelsPerRoll ? data[customizedProductData.product].labelsPerRoll : ''
                data[randomNumber].materialType = customizedProductData.materialType ? customizedProductData.materialType :  data[customizedProductData.product].materialType ? data[customizedProductData.product].materialType : ''
                data[randomNumber].maxRollDiameter = customizedProductData.maxRollDiameter ? customizedProductData.maxRollDiameter :  data[customizedProductData.product].maxRollDiameter ? data[customizedProductData.product].maxRollDiameter : ''
                data[randomNumber].numbering = customizedProductData.numbering ? customizedProductData.numbering :  data[customizedProductData.product].numbering ? data[customizedProductData.product].numbering : ''
                data[randomNumber].perforation = customizedProductData.perforation ? customizedProductData.perforation :  data[customizedProductData.product].perforation ? data[customizedProductData.product].perforation : ''
                data[randomNumber].spotUvVarnish = customizedProductData.spotUvVarnish ? customizedProductData.spotUvVarnish :  data[customizedProductData.product].spotUvVarnish ? data[customizedProductData.product].spotUvVarnish : ''
                data[randomNumber].surfaceFinishing = customizedProductData.surfaceFinishing ? customizedProductData.surfaceFinishing :  data[customizedProductData.product].surfaceFinishing ? data[customizedProductData.product].surfaceFinishing : ''
                data[randomNumber].shippingMethod = customizedProductData.shippingMethod ? customizedProductData.shippingMethod :  data[customizedProductData.product].shippingMethod ? data[customizedProductData.product].shippingMethod : ''
                data[randomNumber].textAreaValue = customizedProductData.textAreaValue ? customizedProductData.textAreaValue :  data[customizedProductData.product].textAreaValue ? data[customizedProductData.product].textAreaValue : ''
                data[randomNumber].width = customizedProductData.width ? customizedProductData.width :  data[customizedProductData.product].width ? data[customizedProductData.product].width : ''
                data[randomNumber].fileQuantityArtwork = customizedProductData.fileQuantityArtwork ? this.state.fileQuantityArtwork :  data[customizedProductData.product].fileQuantityArtwork ? data[customizedProductData.product].fileQuantityArtwork : '' 
                data[randomNumber].shippingAddress = customizedProductData.shippingAddress ? this.state.shippingAddress :  data[customizedProductData.product].shippingAddress ? data[customizedProductData.product].shippingAddress : ''
            }else{
                data[randomNumber]={
                    specificId:randomNumber,
                    id:id,
                    fileQuantityArtwork:this.state.fileQuantityArtwork,
                    name:customizedProductData.product,
                    jobName:customizedProductData.jobName,
                    quantity:customizedProductData.quantity,
                    shape:customizedProductData.shape,
                    color: customizedProductData.color, 
                    coreDiameter: customizedProductData.coreDiameter,
                    productionSample: customizedProductData.productionSample,
                    foilStamping: customizedProductData.foilStamping,
                    labelDirection: customizedProductData.labelDirection,
                    height: customizedProductData.height,
                    rollFinishing: customizedProductData.rollFinishing,
                    labelsPerRoll: customizedProductData.labelsPerRoll,
                    materialType: customizedProductData.materialType,
                    maxRollDiameter: customizedProductData.maxRollDiameter,
                    numbering: customizedProductData.numbering,
                    perforation : customizedProductData.perforation,
                    spotUvVarnish: customizedProductData.spotUvVarnish,
                    surfaceFinishing: customizedProductData.surfaceFinishing,
                    shippingMethod: customizedProductData.shippingMethod,
                    textAreaValue: customizedProductData.textAreaValue,
                    width: customizedProductData.width,
                    shippingAddress : customizedProductData.shippingAddress ? customizedProductData.shippingAddress : "Quadriga USA Enterprises, Inc. Valencia CA 91355"
                }    
            }
            

            this.displayData.push(1);
            this.setState({
                dataChoosen:data,
                customizedProductData:{},
                labelSpecification:true,
                others:false,
                productDefaultValue:' ',
                choosenProduct:{},
                materialType:'',
                maxRollDiameter:'',
                labelsPerRoll:'',
            })


            ls.set('selectedProductData',data)
            const {FetchProducts,ResetInitialState} = this.props
            ResetInitialState()
            // CartProduct(ls.get("selectedCustomizedProduct") ? ls.get("selectedCustomizedProduct") : {})
            // CartProduct(data)
            ls.remove('selectedData')
            FetchProducts()
           
        }else{
            toast.error('Please choose pproduct')
        }
        window.scrollTo(0, 0);
    }
    changeInputValue = (value,name) => {
        let valuesOfInput = Object.assign({},this.state.customizedProductData)
        valuesOfInput[name]=value
        this.setState({customizedProductData:valuesOfInput})
        
    }
    selectValueFromInput = (value,name,e,closetoggle) =>{
        if(e.key === 'Enter') {
        this.setState({[name]:value})
         
        closetoggle()
         
        }
    }
    changeToNotImportentForPerRoll =(closetoggle)=>{
        this.setState({labelsPerRoll:'Not important'})
        let customizedProductData = {...this.state.customizedProductData}
        customizedProductData.labelsPerRoll = 'Not important'
        this.setState({customizedProductData})
        closetoggle()
    }
    changeToNotImportentForMaxRoll =(closetoggle)=>{
        this.setState({maxRollDiameter:'Not important'})
        let customizedProductData = {...this.state.customizedProductData}
        customizedProductData.maxRollDiameter = 'Not important'
        this.setState({customizedProductData})
        closetoggle()
    }
    changeToNotImportentlabelsPerStack = (closeToggle) => {
        this.setState({labelsPerStack:'Not important'})
        let customizedProductData = {...this.state.customizedProductData}
        customizedProductData.labelsPerStack = 'Not important'
        this.setState({customizedProductData})
        closeToggle()
    }
    selectValue = (selectedOption,name) => {
        this.setState({[name.name]:selectedOption.value })
        let customizedProductData = {...this.state.customizedProductData}
        customizedProductData.product = selectedOption.value
        this.setState({customizedProductData})
    }
    prevComponent = () => {
        const {option,roll,others,quantityShow} =this.state
         if(option){
             this.setState({labelSpecification : true,
                 option : false,})
         }
         if(roll){
             this.setState({roll : false,
                 option : true,})
         }
         if(quantityShow){
            this.setState({quantityShow : false,
                roll : true,})
         }
         if(others){
             this.setState({others : false,
                quantityShow : true,})
         }
 
     }
     nextComponent = () => {
        const {labelSpecification,option,roll,quantityShow} =this.state
        if(labelSpecification){
             this.setState({labelSpecification : false,
                 option : true,})
        }
        if(roll){
             this.setState({roll : false,
                quantityShow : true,})
        }
        if(option){
            this.setState({option : false,
                roll : true,})
       }
        if(quantityShow){
            this.setState({quantityShow : false,
                others : true,})
         }
        
        
    }
    onFilesChangeUpload = (files) => {
        this.setState({
            filesUpload:files
          })
    }
    onFilesError = (error, file) => {
        console.log('error code ' + error.code + ': ' + error.message)
    }
    
    filesRemoveOneFromUpload = (toggle,choosenfile) => {
        let {filesUpload} = this.state
        filesUpload.forEach((files,index)=>{
            if(choosenfile.id===files.id){  
                 filesUpload.splice([index],1);
                this.setState({filesUpload})
                toggle()
            }
        })
   
    }
    onClickHandlerUpload = (closeToggle) => {
        const formData = new FormData()
        Object.keys(this.state.filesUpload).forEach((key) => {
          const file = this.state.filesUpload[key]
          formData.append(key, new Blob([file], { type: file.type }), file.name || 'file')
           
        })
        
        // axios.post(`/files`, formData)
        // .then(response => {
        //     console.log(response,"sssssss")
        //      window.alert(`${this.state.files.length} files uploaded succesfully!`)}
        // )
        // .catch(err => window.alert('Error uploading files :('))
        let quantity=Object.keys(this.state.filesUpload).length
        this.setState({fileQuantity:quantity})
        closeToggle()
    
    }
    onFilesChange = (files) => {
        this.setState({
          files
        })
      
    }
    filesRemoveOne = (toggle,file) => {
        let {files } = this.state
        files.forEach((data,index)=>{
            if(file.id===data.id){  
                 files.splice([index],1);
                this.setState({files })
                toggle()
            }
        })
    }
    onClickHandler = (closeToggle) => {
        const formData = new FormData()
        Object.keys(this.state.files).forEach((key) => {
          const file = this.state.files[key]
          formData.append(key, new Blob([file], { type: file.type }), file.name || 'file')
        })
       
        // axios.post(`/files`, formData)
        // .then(response => {
        //     console.log(response,"sssssss")
        //      window.alert(`${this.state.files.length} files uploaded succesfully!`)}
        // )
        // .catch(err => window.alert('Error uploading files :('))
        let quantity=Object.keys(this.state.files).length
        this.setState({filesQuantity:quantity})
        closeToggle()
    
    }
    changeAddres = (toggle) =>{
        const {shippingAddress,customizedProductData} = this.state
        let newAddress = Object.assign({},this.state.customizedProductData)
        newAddress={
            shippingAddress: shippingAddress ?  shippingAddress : customizedProductData.shippingAddress
        }
        this.setState({customizedProductData:newAddress})
        toggle()
    }
    changeAddresForShipping = (value,name) => {
        this.setState({[name]:value})
    }
    cancelChangeAddres = (toggle) => {
        this.setState({shippingAddress:""})  
        toggle()
    }
    style = {
        control: (base, state) => ({
          ...base,
          background: "#e4e4e4",
          margin:"0",
          border: state.isFocused ? 0 : 0,
          // This line disable the blue border
          boxShadow: state.isFocused ? 0 : 0,
          "&:hover": {
            border: state.isFocused ? 0 : 0
          }
        }),
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
              ...styles,
              backgroundColor:isSelected ? '#71A9B0':  "#e4e4e4",
              margin:"0",
              color: 'black',
              cursor:'pointer',
              "&:hover": {
                backgroundColor: '#71A9B0',
                color:"white",
              } 
            };
          },
          menu: (styles) => {
            return {
              ...styles,
              backgroundColor:"#e4e4e4",
              marginTop:"3px",
              paddingTop:"0px",
              width:"calc(100% + 3px)",
              color: 'black',
              cursor:'pointer',
              border:"2px solid #B1B1B1",
               borderTop:'none',
               borderRadius:"0",
               transform: "translate3d(-1.5px, 0px, 0px)"


                
            };
          },
      };
      estimatePrice=()=>{
          const {roll}=this.state
        if(roll){
            this.setState({roll : false,
                others : false,showPricTable:true,quantityShow:true})
         }
      }
      chooseQuantity=(quantity,index)=> {
        let valuesOfquantity = Object.assign({},this.state.customizedProductData)
        valuesOfquantity['quantity']=quantity
        this.setState({customizedProductData:valuesOfquantity,selectedstyleName:index})
    }
    handleChangeQuantity=(value,name)=> {
        let valuesOfquantity = Object.assign({},this.state.customizedProductData)
        valuesOfquantity[name]=value
        this.setState({customizedProductData:valuesOfquantity,selectedstyleName:-1})

    }
    generateCode = () => {
    }
    render(){
         const {others,customizedProductData,productDefaultValue,quantityShow,choosenProduct,showPricTable,heightData,widthData,materialtypedef} = this.state
         const {products,materialTypes,shape,surfaceFinishing,color,stampingOptions,varnishOptions,variableDataOptions,quantity,selectedValues} = this.props
         console.log(this.state.customizedProductData,'l')
        return(
            <div className='customizedProductPage'>
                <div className='customizedHeaderField'>
                  <span>Label</span>
                  <ToastContainer />
                </div>
                <div>
                 {quantityShow ? null :<CustomizedProduct
                        style={this.style}
                        allData={products}
                        handleSelect={this.changeInputValue}
                        product={choosenProduct}
                        productValue={customizedProductData && customizedProductData.product ? customizedProductData.product : 'Customized labels'}
                        others={others}
                    />
                 }
                </div>
                <div className='customizedProductDetailsContainer'>
                        <CustomizedProductDetails
                            generateCode={this.generateCode}
                            heightData={heightData}
                            widthData={widthData}
                            handleChangeQuantity={this.handleChangeQuantity}
                            chooseQuantity={this.chooseQuantity}
                            selectedstyleName={this.state.selectedstyleName}
                            quantityData = {quantity}
                            quantityShow={this.state.quantityShow}
                            style={this.style} 
                            changeSelectInputValue={this.changeInputValue}
                            handleChange={this.changeInputValue}
                            handleSelect={this.changeInputValue}
                            selectValue = {this.selectValueFromInput}
                            // selectedData ={this.state.materialType}
                            materialType={customizedProductData&&customizedProductData.materialType ? customizedProductData.materialType : 'Semi Gloss Paper'}
                            materialTypeData={materialTypes}
                            shapeData = {shape}
                            selectedDataForPerRoll={this.state.labelsPerRoll}
                            selectedDataForMaxRoll={this.state.maxRollDiameter}
                            changeToNotImportentForPerRoll = { this.changeToNotImportentForPerRoll}
                            changeToNotImportentForMaxRoll = { this.changeToNotImportentForMaxRoll}
                            selectedDatalabelsPerStack={this.state.labelsPerStack}
                            changeToNotImportentlabelsPerStack={this.changeToNotImportentlabelsPerStack}
                            labelSpecification={this.state.labelSpecification}
                            option={this.state.option}
                            roll={this.state.roll}
                            others={this.state.others}
                            prevComponent={this.prevComponent}
                            nextComponent={this.nextComponent}
                            shippingAddress={customizedProductData.shippingAddress}
                            changeAddres={this.changeAddres}
                            onFilesChangeUpload={this.onFilesChangeUpload}
                            onError={this.onError}
                            filesUpload={this.state.filesUpload}
                            filesRemoveOneFromUpload={this.filesRemoveOneFromUpload}
                            fileQuantity={this.state.fileQuantity}
                            onClickHandlerUpload={this.onClickHandlerUpload}
                            onFilesChange={this.onFilesChange}
                            files={this.state.files}
                            filesRemoveOne={this.filesRemoveOne}
                            filesQuantity={this.state.filesQuantity}
                            onClickHandler={this.onClickHandler}
                            changeAddresForShipping={this.changeAddresForShipping}
                            surfaceValue={customizedProductData ?customizedProductData.surfaceFinishing : ''}
                            foilStamping={customizedProductData&&customizedProductData.foilStamping ? customizedProductData.foilStamping : 'No Foil Stamping'}
                            numbering={customizedProductData&&customizedProductData.numbering ? customizedProductData.numbering : 'No Variable Data'}
                            spotUvVarnish={customizedProductData&&customizedProductData.spotUvVarnish ? customizedProductData.spotUvVarnish : 'No Spot UV Varnish'}
                            jobName={customizedProductData ? customizedProductData.jobName : ''}
                            shape={customizedProductData&&customizedProductData.shape ? customizedProductData.shape : 'Rectangle'}
                            quantity={customizedProductData ? customizedProductData.quantity : ''}
                            height={customizedProductData ? customizedProductData.height : ''}
                            width={customizedProductData && customizedProductData.width ? customizedProductData.width : ''}
                            color = {customizedProductData&&customizedProductData.color ? customizedProductData.color : 'Full Color'}
                            rollFinishing = {customizedProductData&&customizedProductData.rollFinishing ? customizedProductData.rollFinishing : 'Labels on Roll'}
                            labelDirection={customizedProductData&&customizedProductData.labelDirection ? customizedProductData.labelDirection : 'Not important'}
                            coreDiameter={customizedProductData&&customizedProductData.coreDiameter ?customizedProductData.coreDiameter : '3 Inch'}
                            perforation={customizedProductData ?customizedProductData.perforation : ''}
                            shippingMethod = {customizedProductData ?customizedProductData.shippingMethod : ''} 
                            surfaceFinishing = {customizedProductData&&customizedProductData.surfaceFinishing ? customizedProductData.surfaceFinishing : 'No Surface Finishing'} 
                            productionSample = {customizedProductData ? customizedProductData.productionSample : ''} 
                            textAreaValue = {customizedProductData ? customizedProductData.textAreaValue : ''} 
                            cancelChangeAddres={this.cancelChangeAddres}
                            surfaceFinishingData = {surfaceFinishing}
                            colorData={color}
                            stampingOptionsData={stampingOptions}
                            varnishOptionsData={varnishOptions}
                            variableDataOptionsData={variableDataOptions}
                            sendCustomOrder={this.sendCustomOrder}
                            estimatePrice={this.estimatePrice}
                            showPricTable={showPricTable}                      
                            />
                    <div className='buttonContainer'>
                    </div>
                    {showPricTable ? <PriceTable
                        quantity={customizedProductData && customizedProductData.quantity ? customizedProductData.quantity : ''}
                    /> : null}
                    
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    products : state.productsReducer ? state.productsReducer.products :{},
    materialTypes : state.materialTypeReducer ? state.materialTypeReducer.materialTypes : {},
    shape: state.shapeReducer ? state.shapeReducer.shape : {},
    surfaceFinishing: state.surfaceFinishingReducer ? state.surfaceFinishingReducer.surfaceFinishing : {},
    color: state.colorReducer ? state.colorReducer.color : {},
    stampingOptions: state.stampingOptionsReducer ? state.stampingOptionsReducer.stampingOptions : {},
    varnishOptions: state.varnishOptionsReducer ? state.varnishOptionsReducer.varnishOptions: {},
    variableDataOptions: state.variableDataOptionsReducer ? state.variableDataOptionsReducer.variableDataOptions : {},
    quantity: state.orderQuantityReducer ? state.orderQuantityReducer.quantity : {},
    size: state.sizeReducer && state.sizeReducer.size ? state.sizeReducer.size.result : {}
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedProductId,
            CartProduct,
            FetchProducts,
            ResetInitialState,
            FetchMaterialTypes,
            SelectedValues
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(CustomizedProductContainer);