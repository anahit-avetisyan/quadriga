import React,{Component} from 'react'
// import ls from 'local-storage'
import MultipleSelectInput from '../../../helper/multipleSelectInput'
// import {getRequest} from '../../../helper/utils'
import Photo from '../../../../assets/images/custom.png'



class  MainInformationAboutProduct extends Component {
      
    render(){
        return(
            <div className='customizedProductMainData'>
                <div className='selectOptionContainer'>
                     <span>Product </span>
                    <MultipleSelectInput
                        styles={this.props.style} 
                        selectClassName="select"
                        selectName='product'
                        defaultValue={this.props.productValue}
                        multiple = {false}
                        productData = {this.props.allData} 
                        handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                />
                
                </div>
               <div className='imageAndInformationContainer'>
                    {/* <img src = {`http://${productDataAfterSelect.image}`} alt = "img"  /> */}
                    <div className='imageContainer'>
                        <img src={Photo} alt='img'/>
                    </div>
                    <div className='informationContainer'>
                        <div className='informationAndLinesContainer'>
                            <p>{this.props.product ? this.props.product.description : " "  } 
                            </p>
                            
                        </div>
                    </div>
                </div> 
            </div>
        )
    }
}
export default MainInformationAboutProduct;