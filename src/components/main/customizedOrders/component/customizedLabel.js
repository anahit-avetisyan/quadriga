import React ,{Component} from 'react';
import MultipleSelectInput from '../../../helper/multipleSelectInput';
import Data from '../../../helper/productData';
import Button from '../../../helper/button';
import "mdbreact/dist/css/mdb.css"
// import SelectWithInput from '../../../helper/selectwithInput';
 
 

class CustomizedLabel extends Component {
    downloadFile =  () => {
        //fetch get data
        
    }
    render(){
        return(
            <div className='labelSpecification'>
            <p className='labelHeader'>Label Specifications</p>
            <div className='labelSpecificationContent'>
                <div className='labelDiv'>
                    {/* <Input
                        name="Job Name"
                        inputName='jobName'
                        placeholder ='Job Name ' 
                        changeCallback = {(value,name)=>this.props.handleChange(value,name)}  
                        type = 'text'
                        defaultState = {this.props.jobName}
                        className='inputSyle'
                    />         */}
                     < MultipleSelectInput
                        name='Material type'
                        selectName = "materialType"
                        defaultValue={this.props.materialType}
                        multiple = {false}
                        // productData = {Data}
                        productData = {this.props.materialTypeData} 
                        handleClick = {(value,name)=>this.props.handleSelect(value,name)}    
                        formClassName = "selectStyle"
                        styles={this.props.style} 
                        selectClassName="selectInput"
                    /> 
                    < MultipleSelectInput
                        styles={this.props.style} 
                        name='Shape'
                        selectName = "shape"
                        defaultValue={this.props.shape}
                        multiple = {false}
                        productData = {this.props.shapeData} 
                        handleClick = {(value,name)=>this.props.handleSelect(value,name)} 
                        formClassName = "customizedFormStyle" 
                        selectClassName="selectInput" 
                    />       
                    {/* <Input
                        name='Quantity'
                        inputName='quantity'
                        className='inputSyle'
                        changeCallback = {(value,name)=>this.props.handleChange(value,name)}  
                        type = 'text'
                        defaultState = {this.props.quantity}
                    />  */}
                </div>
                        <div className='labelDiv'>   
                            {/* <SelectWithInput
                                inputInsideSelect='inputInsideSelect'
                                mainInputInsideSelect='selectInputInLabelSpecification'
                                name= 'Material type'
                                onKeyPress={(value,name,e,close)=>this.props.selectValue(value,name,e,close)}
                                changeCallback={(value,name) => this.props.changeSelectInputValue(value,name)}
                                inputName='materialType'
                                type='text'
                                selectedData={this.props.selectedData}
                            />    */}
                           
                                
                            <span className='size'>Size</span>
                            <div className='sizeDetails'>
                                
                                <MultipleSelectInput
                                    multiple = {false}
                                    productData = {this.props.heightData} 
                                    handleClick = {(value,name)=>this.props.handleSelect(value,name)}    
                                    formClassName = "colorStyle" 
                                    styles={this.props.style}
                                    selectClassName="selectInput"
                                    name='Height'
                                    selectName='height'
                                    type = 'text'
                                    // defaultValue = {this.props.height ? this.props.height : this.props.heightData[0]}
                                    
                                />
                                <MultipleSelectInput
                                    multiple = {false}
                                    productData = {this.props.widthData} 
                                    handleClick = {(value,name)=>this.props.handleSelect(value,name)}    
                                    formClassName = "colorStyle" 
                                    styles={this.props.style}
                                    selectClassName="selectInput"
                                    name='Width'
                                    inputName='width'  
                                    // defaultValue = {this.props.width ? this.props.width:this.props.widthData[0]}
                                />
                            </div> 
                   
                    
                    < MultipleSelectInput
                        name='Color'
                        selectName = "color"
                        defaultValue = {this.props.color}
                        multiple = {false}
                        productData = {this.props.colorData ? this.props.colorData :  Data} 
                        handleClick = {(value,name)=>this.props.handleSelect(value,name)}    
                        formClassName = "colorStyle" 
                        styles={this.props.style}
                        selectClassName="selectInput"
                    /> 
                  </div>
            </div>
            <div className="downloadFileContainer">
                <Button
                    name="Download Die Template File"
                    callback={this.downloadFile}
                    className="downloadFile"
                />
            </div>
            
        </div>
        )
    }
}
export default CustomizedLabel;