import React,{Component} from 'react';
import MainModal from '../../../helper/mainModal.js';
import Button from '../../../helper/button.js';
import DivWithSpans from '../../../helper/divWithSpans';
import { FaTrashAlt } from "react-icons/fa";
import ls from 'local-storage';
import history from '../../../../routers/history';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedProductId,CartProduct} from "../../../../reducers/action";
import ProductOptions from '../../productDetails/component/detailsOfSpecificationsProduct/productOptions';
import ProductRollParametrs from '../../productDetails/component/detailsOfSpecificationsProduct/productRollParametrs';
import ProductLabelSpecifications from '../../productDetails/component/detailsOfSpecificationsProduct/productLabelSpecifications.js';
import CustomizedLabel from './customizedLabel'

class CartOrderDetails extends Component {
    state= {
        productDetails:{},
        materialTypeData: {},
        labelProductData: {},
        maxRollDiameter:'',
        labelsPerRoll:'',
    }
    // componentDidMount=()=>{
    //     let id = this.props.match.params.id
    //     let dataFromLs = ls.get("selectedStandardProduct") ? ls.get("selectedStandardProduct") : {}
    //     //customized product
    //     //let dataFromLs = ls.get("selectedCustomizedProduct") ? ls.get("selectedCustomizedProduct") : {}
    //     Object.values(dataFromLs).forEach((data,index)=>{
    //         if(data.id ===  parseInt(id)){
    //            this.setState({productDetails:data}) 
    //         }
    //     })
    // }
    changeToNotImportentForPerRoll =(closetoggle)=>{
        this.setState({labelsPerRoll:'Not importent'})
        let labelProductData = {...this.state.labelProductData}
        labelProductData.labelsPerRoll = 'Not importent'
        this.setState({labelProductData})
         
        closetoggle()
    }
    changeToNotImportentForMaxRoll =(closetoggle)=>{
        this.setState({maxRollDiameter:'Not importent'})
        let labelProductData = {...this.state.labelProductData}
        labelProductData.maxRollDiameter = 'Not importent'
        this.setState({labelProductData})
        closetoggle()
    }
    selectValueFromInput = (value,name,e,closetoggle) =>{
        if(e.key === 'Enter') {
        this.setState({[name]:value})
     
        closetoggle()
         
        }
    }
    deleteFromCart = (toggle) => {
        let id = this.props.match.params.id
        let dataFromLs = ls.get("selectedStandardProduct") ? ls.get("selectedStandardProduct") : {}
        //let dataFromLs = ls.get("selectedProduct") ? ls.get("selectedProduct") : {}
        Object.values(dataFromLs).forEach((data,index)=>{
            if(data.id===parseInt(id)){
                delete dataFromLs[data.id]
            }
        })
        ls.set("selectedStandardProduct",dataFromLs)
        this.props.CartProduct(ls.get("selectedStandardProduct") ? ls.get("selectedStandardProduct") : {})
        this.props.CartProduct(dataFromLs)
        toggle()
        history.push('/cart')
    }
    handleSelect = (value,name) => {
        let valuesOfInput = Object.assign({},this.state.labelProductData)
        valuesOfInput[name]=value
        this.setState({labelProductData:valuesOfInput})
    }
    changeProductData = (toggle) => {
        const {labelProductData,productDetails }= this.state
        let newData={
            jobName: labelProductData.jobName ? labelProductData.jobName : productDetails.jobName,
            id: productDetails.id,
            product: labelProductData.product ? labelProductData.product : productDetails.product,
            size:labelProductData.size ? labelProductData.size : productDetails.size,
            height:labelProductData.height ? labelProductData.height : productDetails.height,
            width:labelProductData.width ? labelProductData.width : productDetails.width,
            materialType : labelProductData.materialType ? labelProductData.materialType : productDetails.materialType,
            shape : labelProductData.shape ? labelProductData.shape : productDetails.shape,
            color: labelProductData.color ? labelProductData.color : productDetails.color,
            quantity: labelProductData.quantity ? labelProductData.quantity : productDetails.quantity,
            surfaceFinishing:labelProductData.surfaceFinishing ? labelProductData.surfaceFinishing : productDetails.surfaceFinishing,
            foilStamping : labelProductData.foilStamping ? labelProductData.foilStamping : productDetails.foilStamping,
            numbering : labelProductData.numbering ? labelProductData.numbering : productDetails.numbering,
            spotUvVarnish : labelProductData.spotUvVarnish ? labelProductData.spotUvVarnish : productDetails.spotUvVarnish,
            labelOrientation : labelProductData.labelOrientation ? labelProductData.labelOrientation : productDetails.labelOrientation,
            gapBetweenLabels : labelProductData.gapBetweenLabels ? labelProductData.gapBetweenLabels : productDetails.gapBetweenLabels,
            coreDiameter : labelProductData.coreDiameter ? labelProductData.coreDiameter : productDetails.coreDiameter,
            perforation : labelProductData.perforation ? labelProductData.perforation : productDetails.perforation,
            maxRollDiameter: labelProductData.maxRollDiameter ? labelProductData.maxRollDiameter : productDetails.maxRollDiameter,
            labelsPerRoll: labelProductData.labelsPerRoll ? labelProductData.labelsPerRoll : productDetails.labelsPerRoll,
            textAreaValue: labelProductData.textAreaValue ? labelProductData.textAreaValue : productDetails.textAreaValue,
            shipping: labelProductData.shipping ? labelProductData.shipping : productDetails.shipping,
            date:productDetails.date

        }
        let dataFromLs = ls.get("selectedStandardProduct") ? ls.get("selectedStandardProduct") : {}
        Object.values(dataFromLs).forEach((data,index)=>{
            if(data.id===productDetails.id){
                dataFromLs[productDetails.id]=newData
            }
        })
        ls.set("selectedStandardProduct",dataFromLs)
        this.setState({productDetails:newData,labelProductData:{},labelsPerRoll:'',maxRollDiameter:''})
        toggle()
       
    }
    cancelChangeProductData = (toggle) => {
        this.setState({labelProductData:{}})
        toggle()
    }
    style = {
        control: (base, state) => ({
          ...base,
          background: "#e4e4e4",
          margin:"0",
          border: state.isFocused ? 0 : 0,
          // This line disable the blue border
          boxShadow: state.isFocused ? 0 : 0,
          "&:hover": {
            border: state.isFocused ? 0 : 0
          }
        }),
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
              ...styles,
              backgroundColor:isSelected ? '#71A9B0':  "#e4e4e4",
              margin:"0",
              color: 'black',
              cursor:'pointer',
              "&:hover": {
                backgroundColor: '#71A9B0',
                color:"white",
              } 
            };
          },
          menu: (styles) => {
            return {
              ...styles,
              backgroundColor:"#e4e4e4",
              marginTop:"3px",
              paddingTop:"0px",
              width:"calc(100% + 3px)",
              color: 'black',
              cursor:'pointer',
              border:"2px solid #B1B1B1",
               borderTop:'none',
               borderRadius:"0",
               transform: "translate3d(-1.5px, 0px, 0px)"


                
            };
          },
      };
      makeOrder=()=> {
          console.log("jjjj")
      }
    render(){
        const {productDetails,labelProductData }=this.state
        const {materialTypes,quantity,shape,surfaceFinishing,color,stampingOptions,varnishOptions,variableDataOptions} = this.props
        return(
            <div className="orderDetailsContainer detailsContent">
                {/* <p className="orderDetailsHeader">Cart</p> */}
                <div className="orderMainDetails">
                    <span className="headerOfMain"> {productDetails.product}</span>
                     
                </div>
                <div>
                    <p className="parametersHeader">Order detiles</p>
                    <div className="ordersDetailsFirstPartContainer">
                        <div className="orderLabelDetails">
                            <span className="componentsHeader">Label Specifications</span>
                            <DivWithSpans
                                divClassName='editDataContainer'
                                firstSpanClassName="headerForCartData"
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Product: "
                                secondData={productDetails.product}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Material type: "
                                secondData={productDetails.materialType}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData" 
                                firstData="Shape: "
                                secondData={productDetails.shape}
                                />
                            <DivWithSpans 
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Size: "
                                secondData={productDetails.size}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData" 
                                firstData="Color: "
                                secondData={productDetails.color}
                            /> 
                            <MainModal
                                mainDivClassName = "editButtonModal "
                                className=  'editLabelModal'
                                modalTitle="CONFIRMATION"
                                editData={(toggle)=>this.changeProductData(toggle)}
                                toggleName="Edit"
                                save="Save"
                                cancel="NO"
                                saveButtonClassName="deleteButtonConformation"
                                cancelData={(toggle)=>this.cancelChangeProductData(toggle)}
                                backdrop={"static"}
                            >
                                <CustomizedLabel
                                    style={this.style}
                                    handleSelect ={this.handleSelect}
                                    handleChange = {this.handleSelect}
                                    selectedData={this.props.selectedData}
                                    jobName = {labelProductData.jobName ? labelProductData.jobName: productDetails.jobName }
                                    shape={labelProductData.shape ? labelProductData.shape :productDetails.shape}
                                    quantity = {labelProductData.quantity ? labelProductData.quantity : productDetails.quantity}
                                    size = {labelProductData.size ? labelProductData.size:productDetails.size}
                                    color = {labelProductData.color ? labelProductData.color : productDetails.color}
                                    materialTypeData={materialTypes}
                                    materialType={labelProductData.materialType?labelProductData.materialType:productDetails.materialType}
                                    shapeData={shape}
                                    dataOfQuantity={quantity}
                                    colorData={color}
                                    
                                    
                                />
                                {/* <CustomizedLabel
                                    handleSelect ={this.handleSelect}
                                    handleChange = {this.handleSelect}
                                    // selectValue = {(value,name,e,close)=>this.props.selectValue(value,name,e,close)}
                                    // changeSelectInputValue={(value,name) => this.props.changeSelectInputValue(value,name)}
                                    selectedData={this.props.selectedData}
                                    jobName = {productDetails.jobName}
                                    shape={productDetails.shape}
                                    quantity = {productDetails.quantity}
                                    width = {productDetails.width}
                                    height = {productDetails.height}
                                    color = {productDetails.color}
                                    materialTypeData={materialTypeData}
                                    materialType={productDetails.materialType}
                                /> */}
                            </MainModal> 
                        </div>
                        <div className="orderOptionsDetails">
                            <span className="componentsHeader">Options</span>
                            <DivWithSpans 
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Surface Finishing: "
                                secondData={productDetails.surfaceFinishing}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Foil Stamping: "
                                secondData={productDetails.foilStamping}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData"
                                firstData="Numbering: "
                                secondData={productDetails.numbering}
                            />
                            <DivWithSpans
                                firstSpanClassName="headerForCartData"
                                divClassName='editDataContainer'
                                secondSpanClassName="resultheaderForCartData" 
                                firstData="Spot UV Varnish: "
                                secondData={productDetails.spotUvVarnish}
                                /> 
                            <MainModal
                                mainDivClassName = "editButtonModal"
                                className='editLabelModal'
                                modalTitle="CONFIRMATION"
                                editData={(toggle)=>this.changeProductData(toggle)}
                                toggleName="Edit"
                                save="YES"
                                cancel="NO"
                                saveButtonClassName="deleteButtonConformation"
                                cancelData={(toggle)=>this.cancelChangeProductData(toggle)}
                                backdrop={"static"}
                            >
                                <ProductOptions
                                    surfaceFinishingData={surfaceFinishing}
                                    style={this.style}
                                    header='Options'
                                    headerClassName='optionHeader'
                                    handleClick = {this.handleSelect}
                                    handleSelect = {this.handleSelect}
                                    surFaceClassName='selectOptionStyle'
                                    foilClassName='selectOptionStyle'
                                    numberingClassName='selectOptionStyle'
                                    spotClassName='selectOptionStyle'
                                    surfaceValue={labelProductData.surfaceFinishing ? labelProductData.surfaceFinishing : productDetails.surfaceFinishing}
                                    foilStamping = {labelProductData.foilStamping ? labelProductData.foilStamping :productDetails.foilStamping}
                                    numbering = {labelProductData.numbering ? labelProductData.numbering :productDetails.numbering}
                                    spotUvVarnish = {labelProductData.spotUvVarnish ? labelProductData.spotUvVarnish :productDetails.spotUvVarnish}
                                    stampingOptionsData={stampingOptions}
                                    varnishOptionsData={varnishOptions}
                                    variableDataOptionsData={variableDataOptions}
                                /> 
                                </MainModal> 
                        </div>
                    </div>
                    <div className="ordersDetailsSecondPartContainer">
                        <div className="orderRollDetails">
                                <span className="componentsHeader">Roll Parameters</span>
                                <DivWithSpans 
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData"
                                    firstData="Label Orientation: "
                                    secondData={productDetails.labelOrientation}
                                />
                                <DivWithSpans
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData"
                                    firstData="Gap Between Labels: "
                                    secondData={productDetails.gapBetweenLabels}
                                />
                                <DivWithSpans
                                    divClassName='editDataContainer'
                                    firstSpanClassName="headerForCartData"
                                    secondSpanClassName="resultheaderForCartData" 
                                    firstData="Core Diameter: "
                                    secondData={productDetails.coreDiameter}
                                    />
                                <DivWithSpans 
                                    firstSpanClassName="headerForCartData"
                                    divClassName='editDataContainer'
                                    secondSpanClassName="resultheaderForCartData"
                                    firstData="Max Roll Diameter: "
                                    secondData={productDetails.maxRollDiameter}
                                />
                                <DivWithSpans
                                    firstSpanClassName="headerForCartData"
                                    secondSpanClassName="resultheaderForCartData"
                                    divClassName='editDataContainer'
                                    firstData="Labels Per Roll: "
                                    secondData={productDetails.labelsPerRoll}
                                />
                                <DivWithSpans
                                    firstSpanClassName="headerForCartData"
                                    secondSpanClassName="resultheaderForCartData"
                                    divClassName='editDataContainer'
                                    firstData="Perforation: "
                                    secondData={productDetails.perforation}
                                    /> 
                                <MainModal
                                    mainDivClassName = "editButtonModal"
                                    className='editLabelModal'
                                    modalTitle="CONFIRMATION"
                                    toggleName="Edit"
                                    save="Save"
                                    cancel="NO"
                                    saveButtonClassName="deleteButtonConformation"
                                    editData={(toggle)=>this.changeProductData(toggle)}
                                    cancelData={(toggle)=>this.cancelChangeProductData(toggle)}
                                    backdrop={"static"}
                                >
                                    <ProductRollParametrs
                                        style={this.style}
                                        inputInsideSelect='inputInsideSelectModal'
                                        mainInputInsideSelect='mainInputInsideSelectModal'
                                        divLineClassName='rollLine'
                                        header='Roll Parameters'
                                        headerClassName='optionHeader'
                                        handleSelect = {this.handleSelect}
                                        handleChange = {this.handleSelect}
                                        selectValue = {this.selectValueFromInput}
                                        selectedDataForPerRoll={this.state.labelsPerRoll ? this.state.labelsPerRoll : productDetails.labelsPerRoll}
                                        selectedDataForMaxRoll={this.state.maxRollDiameter ? this.state.maxRollDiameter :productDetails.maxRollDiameter }
                                        changeToNotImportentForPerRoll = {(closeButton) => this.changeToNotImportentForPerRoll(closeButton)}
                                        changeToNotImportentForMaxRoll = {(closeButton) => this.changeToNotImportentForMaxRoll(closeButton)}
                                        labelOrientation = {labelProductData.labelOrientation ?labelProductData.labelOrientation: productDetails.labelOrientation}
                                        gapBetweenLabels={labelProductData.gapBetweenLabels ? labelProductData.gapBetweenLabels : productDetails.gapBetweenLabels}
                                        coreDiameter={labelProductData.coreDiameter ? labelProductData.coreDiameter : productDetails.coreDiameter}
                                        perforation={labelProductData.perforation ? labelProductData.perforation : productDetails.perforation}

                                    /> 
                                </MainModal> 
                            </div>
                    </div>
                    
                </div>
                
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
    state,
    products : state.productsReducer ? state.productsReducer.products :{},
    materialTypes : state.materialTypeReducer ? state.materialTypeReducer.materialTypes : {},
    quantity: state.orderQuantityReducer ? state.orderQuantityReducer.quantity : {},
    shape: state.shapeReducer ? state.shapeReducer.shape : {},
    surfaceFinishing: state.surfaceFinishingReducer ? state.surfaceFinishingReducer.surfaceFinishing : {},
    color: state.colorReducer ? state.colorReducer.color : {},
    stampingOptions: state.stampingOptionsReducer ? state.stampingOptionsReducer.stampingOptions : {},
    varnishOptions: state.varnishOptionsReducer ? state.varnishOptionsReducer.varnishOptions: {},
    variableDataOptions: state.variableDataOptionsReducer ? state.variableDataOptionsReducer.variableDataOptions : {}
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedProductId,
            CartProduct,
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(CartOrderDetails);