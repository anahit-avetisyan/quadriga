import React,{Component} from 'react';
import { FaChevronDown } from "react-icons/fa";
import { FaPlus } from "react-icons/fa";
import PriceRow from './priceRow'

class PriceTable extends Component {
    state = {
        showPrice:true,
        appendedCompsCount: 0,
        appendedComponents:[]
    }
    openPriceContainer=()=>{
        this.setState(prevState => ({
            showPrice: !prevState.showPrice
          }));
    }
    addRow=()=>{
        this.setState({
            appendedCompsCount: this.state.appendedCompsCount + 1
          }) 
         
    }
    componentDidUpdate = (prevProps,prevState) => {
        if(this.state.appendedCompsCount !== prevState.appendedCompsCount){
            this.getAppendedComponents()
        }
    }
    getAppendedComponents = () => {
        let appendedComponents = [];
        for (let i = 0; i < this.state.appendedCompsCount; i++) {
           appendedComponents.push(
             <PriceRow key={i}
             quantity={i}
             deleteRow={()=>this.deleteRow(i)}
             handleChangeQuantity={this.handleChangeQuantity}
             onBlur={this.onKeyDownEnter}
             onKeyDown={this.onKeyDownEnter}
             selectQuantity={this.choosePriceDependOnQuantity}
             />
           )
        }
        this.setState({appendedComponents:appendedComponents})
       
        ;
    }
    handleChangeQuantity = (value,name)=> {
        this.setState({[name]:value})
    }
    deleteRow = (index) => {
        let {appendedComponents} = this.state
        Object.values(appendedComponents).map((data,i)=>{
           if(parseInt(data.key)===index){
                appendedComponents.splice(index,1)
                this.setState({appendedComponents:appendedComponents, appendedCompsCount: this.state.appendedCompsCount -1})
             }
             
            
        })
            
    }
    choosePrice = (data) => {
       this.setState({quantity:data})
    } 
    onKeyDownEnter = (e)=> {
        if (e.key === 'Enter') {
            console.log(this.state,'enter') 
        } 
    }
    choosePriceDependOnQuantity = ()=> {
        
    }
    render(){
        let data=[1,2,3,4]
        const {showPrice}=this.state
        return(
        <div className='fixedPosision'>
            <div className='priceTableContainer'>
                <div className='closeIcon'>
                    <FaChevronDown onClick={this.openPriceContainer}/>
                </div>
                {showPrice ? 
                <div className="quantityAndPriceContainer">
                    <div className='quantityContent'>
                        <div className="quantityHeaderContainer">
                            <p>Quantity</p>
                            <p>Price (USD)</p>
                            <p>Price Per Labels (USD)</p>
                        </div>
                        { data.map((data,index)=>{
                            return(
                                <div key={index} onClick={()=>this.choosePrice(data)} className='quantityDataContainer'>
                            <div  className="quantity">
                                <p>{data}</p>
                            </div>
                            <div className="price">
                                <p>000</p>
                            </div>
                            <div className="pricePerlabel">
                                <p>0000</p>
                            </div>
                        </div>
                            )
                        })
                        
                        }
                        <div className="plusContainer">
                            <div onClick={this.addRow} className="plusIconContainer">
                                <FaPlus/>
                            </div>
                            {this.state.appendedComponents.map(data=>{
                                return data
                            })}
                        </div>
                    </div>
                    
                    <div className='priceContent'>
                        <div>
                            <p>Quantity:</p>
                            <div className='line'></div>
                            <p>$</p>
                        </div>
                        <div>
                            <p>Price:</p>
                            <div className='line'></div>
                            <p>$</p>
                        </div>
                        <div>
                            <p>Price Per Label:</p>
                            <div className='line'></div>
                            <p>$</p>
                        </div>
                        <div>
                            <p>Layout Fee:</p>
                            <div className='line'></div>
                            <p>$</p>
                        </div>
                        <div>
                            <p>Die Cut Charge:</p>
                            <div className='line'></div>
                            <p>$</p>
                        </div>
                        <div>
                            <p>Production Sample:</p>
                            <div className='line'></div>
                            <p>$</p>
                        </div>
                        <div>
                            <p>Shipping Cost:</p>
                            <div className='line'></div>
                            <p>$</p>
                        </div>
                        <div>
                            <p>Sales Tax:</p>
                            <div className='line'></div>
                            <p>$</p>
                        </div>
                        <div className='endLine'></div>
                    </div>
                </div>:null}
                <div className='totalPrice'>
                    <p>Total:</p>
                    <p>$</p>
                </div>
            </div>
         </div>
        )
    }
}
export default PriceTable;