import React,{Component} from 'react';
import ProductOptions from '../../productDetails/component/detailsOfSpecificationsProduct/productOptions';
import ProductRollParametrs from '../../productDetails/component/detailsOfSpecificationsProduct/productRollParametrs';
import CustomizedProductOtherDetails from './customizedProductOtherDetails';
import CustomizedLabel from './customizedLabel';
import Button from '../../../helper/button';
// import CustomizedQuantity from './customizedQuantity';
import OrderDetails from './orderDetails';

class CustomizedProductDetails extends Component {
    render(){
        const {labelSpecification,option,roll,others,quantityShow,heightData,widthData,showPricTable} =this.props
        return(
            <div className='customizedProductDetails'>
                <div className={showPricTable ? 'productDetailsContainer':null}>
                    {labelSpecification ? <CustomizedLabel
                        heightData={heightData}
                        widthData={widthData}
                        style={this.props.style} 
                        handleSelect ={(value,name)=>this.props.handleSelect(value,name)}
                        handleChange = {(value,name)=>this.props.handleSelect(value,name)}
                        selectValue = {(value,name,e,close)=>this.props.selectValue(value,name,e,close)}
                        changeSelectInputValue={(value,name) => this.props.changeSelectInputValue(value,name)}
                        selectedData={this.props.selectedData}
                        shape={this.props.shape}
                        width = {this.props.width}
                        height = {this.props.height}
                        color = {this.props.color}
                        materialTypeData={this.props.materialTypeData}
                        shapeData = {this.props.shapeData}
                        materialType={this.props.materialType}
                        colorData={this.props.colorData}
                    /> : null}              
                    {option ? <ProductOptions
                        style={this.props.style} 
                        header='Label Options'
                        headerClassName='optionHeader'
                        handleClick = {(value,name)=>this.props.handleSelect(value,name)} 
                        handleSelect = {(value,name)=>this.props.handleSelect(value,name)}
                        surFaceClassName='selectOptionStyle'
                        foilClassName='selectOptionStyle'
                        numberingClassName='selectOptionStyle'
                        spotClassName='selectOptionStyle'
                        surfaceValue={this.props.surfaceFinishing}
                        foilStamping = {this.props.foilStamping}
                        numbering = {this.props.numbering}
                        spotUvVarnish = {this.props.spotUvVarnish}
                        surfaceFinishingData={this.props.surfaceFinishingData}
                        stampingOptionsData={this.props.stampingOptionsData}
                        varnishOptionsData={this.props.varnishOptionsData}
                        variableDataOptionsData={this.props.variableDataOptionsData}
                    /> : null}               
                    {roll ? <ProductRollParametrs
                        inputInsideSelect='inputInsideSelect'
                        mainInputInsideSelect='mainInputInsideSelect'
                        divLineClassName='rollLine'
                        style={this.props.style} 
                        header='Roll Parameters'
                        headerClassName='optionHeader'
                        handleSelect ={(value,name)=>this.props.handleSelect(value,name)}
                        handleChange = {(value,name)=>this.props.handleSelect(value,name)}
                        selectValue={(value,name,e,closeButton)=>this.props.selectValue(value,name,e,closeButton)}
                        selectedDataForPerRoll={this.props.selectedDataForPerRoll}
                        selectedDataForMaxRoll={this.props.selectedDataForMaxRoll}
                        selectedDatalabelsPerStack={this.props.selectedDatalabelsPerStack}
                        changeToNotImportentlabelsPerStack={(closeButton) => this.props.changeToNotImportentlabelsPerStack(closeButton)}
                        changeToNotImportentForPerRoll = {(closeButton) => this.props.changeToNotImportentForPerRoll(closeButton)}
                        changeToNotImportentForMaxRoll = {(closeButton) => this.props.changeToNotImportentForMaxRoll(closeButton)}
                        rollFinishing = {this.props.rollFinishing}
                        labelDirection={this.props.labelDirection}
                        coreDiameter={this.props.coreDiameter}
                        perforation={this.props.perforation}
                        inputInsideSelectFormContainer="inputInsideSelectFormContainer"  
                                   
                    /> : null}
                    {quantityShow ?<OrderDetails
                        stayle={this.props.stayle}    
                    />:null}
                    {others ? <div className='otherSpecificationContainer'>
                        <CustomizedProductOtherDetails
                            header='Order data'
                            headerClassName='optionHeader'
                            handleSelect = {(value,name)=>this.props.handleSelect(value,name)}
                            handleChange={(value,name)=>this.props.handleChange(value,name)}
                            shippingAddress={this.props.shippingAddress}
                            changeAddres={(toggle)=>this.props.changeAddres(toggle)}
                            onError={()=>this.props.onError()}
                            onFilesChangeUpload={(files)=>this.props.onFilesChangeUpload(files)}
                            filesUpload={this.props.filesUpload}
                            filesRemoveOneFromUpload={(toggle,file)=>this.props.filesRemoveOneFromUpload(toggle,file)}
                            fileQuantity={this.props.fileQuantity}
                            onClickHandlerUpload={(toggle)=>this.props.onClickHandlerUpload(toggle) }
                            onFilesChange={(files)=>this.props.onFilesChange(files)}
                            files={this.props.files}
                            filesRemoveOne={(toggle,file)=>this.props.filesRemoveOne(toggle,file)}
                            filesQuantity={this.props.filesQuantity}
                            onClickHandler={(toggle)=>this.props.onClickHandler(toggle) }
                            changeAddresForShipping= {(value,name)=>this.props.changeAddresForShipping(value,name)}
                            shippingMethod = {this.props.shippingMethod} 
                            surfaceFinishing = {this.props.surfaceFinishing} 
                            productionSample = {this.props.productionSample} 
                            cancelChangeAddres={(toggle)=>this.props.cancelChangeAddres(toggle)}
                            style={this.props.style} 
                            jobName = {this.props.jobName}
                        />                           
                        <div>
                            <div className='textareaContainer'>
                                <p>Notes</p>
                                <textarea name='textAreaValue' placeholder='Please,write your notes here' defaultValue={this.props.textAreaValue} onChange={(event)=>this.props.handleChange(event.target.value,event.target.name)}/> 
                            </div> 
                        </div>            
                    </div> : null}               
                </div>
                 <div  className={labelSpecification ? 'buttonsContainerEnd' : others ? 'buttonsContainerSpace' : 'buttonsContainer'}>
                    
                        { labelSpecification ? null : <Button
                        callback={()=>this.props.prevComponent()}
                        name = '< Back'
                        className="backButton"
                    />}
                    {roll ? <Button
                        callback={()=>this.props.estimatePrice()}
                        name = 'Estimate'
                        className="estimateButton"
                    />:null}
                     {others ? 
                        <div className='othersButtonsContainer'>
                            <Button
                                callback={()=>this.props.sendCustomOrder()}
                                name="Add To Card"
                                className="nextButton"
                            /> 
                            <Button
                                callback={()=>this.props.generateCode()}
                                name="Generate Code"
                                className="nextButton"
                            />
                        </div>:null}
                    {roll||others ? null :<Button
                        name = 'Next >'
                        callback={()=>this.props.nextComponent()}
                        className="nextButton"
                    />}
                </div>
            </div>
            
        )
    }
}
export default CustomizedProductDetails;