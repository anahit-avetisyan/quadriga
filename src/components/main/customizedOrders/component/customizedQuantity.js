import React,{Component} from 'react';
import { FaPlus } from "react-icons/fa";
import Input from '../../../helper/input';


class CustomizedQuantity extends Component {
    state = {
        newRow:false,
    }
    addRow=()=>{
        this.setState({newRow:true})
    }
   
    render(){
        const {quantityData}=this.props
        return(
            <div className='quantityContainer'>
                <p className="quantityHeader">Quantity</p>
                <div className="quantityHeaderContainer">
                    <p>Quantity</p>
                    <p>Price (USD)</p>
                    <p>Price Per Labels (USD)</p>
                </div>
                {!quantityData ? []: Object.values(quantityData).map((data,index)=>{
                    return(
                        <div key={index} className={this.props.selectedstyleName===index ? "choosedquantityDataContainer": 'quantityDataContainer'}>
                    <div onClick={()=>this.props.chooseQuantity(data.quantity,index)} className='quantity' >
                        <p>{data.quantity}</p>
                    </div>
                    <div className="price">
                        <p>000</p>
                    </div>
                    <div className="pricePerlabel">
                        <p>0000</p>
                    </div>
                </div>
                    )
                })}
                
                {this.state.newRow ? <div className='quantityDataContainer'>
                    <div className="quantity">
                        <Input
                            inputName='quantity'
                            className='inputSyle handleQuantity'
                            changeCallback = {(value,name)=>this.props.handleChangeQuantity(value,name)}  
                            type = 'text'
                            defaultState = {this.props.quantity}
                        />
                    </div>
                    <div className="price">
                        <p>000</p>
                    </div>
                    <div className="pricePerlabel">
                        <p>0000</p>
                    </div>
                </div>:null}
                <div className="plusContainer">
                    <div onClick={this.addRow} className="plusIconContainer">
                        <FaPlus/>
                    </div>
                </div>
                
            </div>
        )
    }
}
export default CustomizedQuantity;