import React,{Component} from 'react';
import Input from '../../../helper/input'
import { FaTimes } from "react-icons/fa";

class PriceRow extends Component {
    handleSubmit(e) {
        e.preventDefault();
    }
    render(){
        return(
            <div className='quantityDataContainer'>
                <div className="quantity">
                <form  className='inputSyle handleQuantity'  onSubmit={this.handleSubmit}>
                    <label>
                        <input
                            name='quantity'
                            onChange = {event=>this.props.handleChangeQuantity(event.target.value,event.target.name)}  
                            type = 'text'
                            defaultValue = {this.props.quantity}
                            onBlur = {event => this.props.onBlur(event)}
                            onKeyDown = {(event)=>this.props.onKeyDown(event)}
                        />
                    </label>
                </form>  
                </div>
                <div className="price" onClick={this.props.selectQuantity}>
                    <p>{this.props.quantity}</p>
                </div>
                <div className="pricePerlabel" onClick={this.props.selectQuantity} >
                    <p>0000</p>
                    <FaTimes className='deleteButton' onClick={this.props.deleteRow}/>
                </div>
            </div>
        )
    }
}
export default PriceRow;