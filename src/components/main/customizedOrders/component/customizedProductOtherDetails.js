import React,{Component} from 'react'
import MultipleSelectInput from '../../../helper/multipleSelectInput'
import Data from '../../../helper/productData'
import MainModal from '../../../helper/mainModal.js'
import Input from '../../../helper/input';
import File from '../../../helper/file';
import { FaTrashAlt } from "react-icons/fa";
// import axios from 'axios'

class CustomizedProductOtherDetails extends Component {
   
    render(){
 
        return(
            <div>
                <p className ={this.props.headerClassName}>{this.props.header}</p>
                <div  className='otherSpecificationContent'>
                    
                        <div className='otherSpecificationDiv'>
                        <Input
                            name="Job Name"
                            inputName='jobName'
                            placeholder ='Job Name ' 
                            changeCallback = {(value,name)=>this.props.handleChange(value,name)}  
                            type = 'text'
                            defaultState = {this.props.jobName}
                            className='inputSyle'
                        />
                    < MultipleSelectInput
                        name='Production Sample'
                        selectName = "productionSample"
                        multiple = {false}
                        productData = {Data} 
                        handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                        formClassName = "selectStyle"    
                        defaultValue = {this.props.productionSample}
                        styles={this.props.style}
                        selectClassName="selectInput"
                    />                         
                    < MultipleSelectInput
                        name="Shipping Method"
                        selectName = "shippingMethod"
                        multiple = {false}
                        productData = {Data} 
                        handleClick = {(value,name)=>this.props.handleSelect(value,name)}
                        formClassName = "selectStyle"     
                        defaultValue = {this.props.shippingMethod}
                        styles={this.props.style}
                        selectClassName="selectInput"
                    /> 
                    </div>
                    <div className='otherSpecificationDiv'> 
                    <span className='filesHeader'>Artwork(s)</span>
                    <div className="artworksQuantityContainer">
                        <p>Number of Artwork(s)</p>
                        <div className="artworkQuantity">
                            <p className='uploadFile'>{this.props.fileQuantity}</p>
                        </div>
                    </div>
                            <div className='uploadFilesContainer'>
                                <p className="headeruploadArtwork">Upload Artwork(s)</p>
                                <MainModal
                                    className='fileModalContainer'
                                    contentClassName='fileContentClassName'
                                    toggleClassName='uploadFile'
                                    editData ={(toggle)=>this.props.onClickHandlerUpload(toggle) }
                                    toggleName="Upload file(s)"
                                    save='Done'
                                    saveButtonClassName='saveButtonClassName'
                                >    
                                    <File
                                        onChange={(files)=>this.props.onFilesChangeUpload(files)}
                                        onError={()=>this.props.onError()}
                                        multiple 
                                        maxFiles={10}
                                        maxFileSize={10000000}
                                        minFileSize={0}
                                        header='Attach file(s) to your order'
                                        buttonName='Choose File'
                                        files={this.props.filesUpload}
                                        filesRemoveOne={(toggle,file)=>this.props.filesRemoveOneFromUpload(toggle,file)}
                                        removeName={<FaTrashAlt className="trashIcon"/>}
                                        textAboutConformation="Do you want to delete attach file"
                                        clickable
                                    />                      
                                    
                                </MainModal>
                        
                        </div>
                        {/* <span  className='filesHeader'>Attached Files</span>                      
                        <div className='containerFileLoaderAndQuantity'>
                            <MainModal
                                className='fileModalContainer'
                                contentClassName='fileContentClassName'
                                mainDivClassName='filesModal'
                                toggleClassName='chooseFile'
                                editData ={(toggle)=>this.props.onClickHandler(toggle) }
                                toggleName="Choose"
                                save='Done'
                                saveButtonClassName='saveButtonClassName'
                            >   
                                <File
                                    onChange={(files)=>this.props.onFilesChange(files)}
                                    onError={()=>this.props.onFilesError()}
                                    multiple 
                                    maxFiles={10}
                                    maxFileSize={10000000}
                                    minFileSize={0}
                                    header='Attach file(s) to your order'
                                    buttonName='Choose File'
                                    files={this.props.files}
                                    filesRemoveOne={(toggle,file)=>this.props.filesRemoveOne(toggle,file)}
                                    clickable
                                    removeName={<FaTrashAlt className="trashIcon"/>}
                                    textAboutConformation="Do you want to delete attach file"
                                    />                      
                            </MainModal>
                            <span className='chooseFile'>({this.props.filesQuantity})</span>
                        </div>                           */}
    
                        <div className='changeAddresContainer'> 
                            <div className='addressChangefield'>
                                <span>Pickup / Shipping Address </span>
                                <MainModal
                                    className='fileModalContainer changeAddresModal'
                                    toggleClassName='changeAddres'
                                    editData ={(toggle)=>this.props.changeAddres(toggle)}
                                    toggleName="Change"
                                    save='Change'
                                    cancel="Cancel"
                                    saveButtonClassName='saveButtonClassName'
                                    backdrop={"static"}
                                    cancelData={(toggle)=>this.props.cancelChangeAddres(toggle)}
                                >
                                    <Input
                                        defaultState= {this.props.shippingAddress}
                                        inputName='shippingAddress'
                                        placeholder ='New address ' 
                                        name="Shipping address"
                                        changeCallback = {(value,name)=>this.props.changeAddresForShipping(value,name)}  
                                        type = 'text'
                                        className='inputSyleForShippingAddres'
                                    />
                                </MainModal>
                            </div>
                            <span className='changedAddress'>{this.props.shippingAddress}</span>
                            
                            
                        </div> 
                    </div>            
            </div>   
        </div>  
        )
    }
}
export default CustomizedProductOtherDetails;