import React ,{Component} from 'react'
import DivWithSpans from '../../../helper/divWithSpans'
import {Link } from 'react-router-dom';
import Button from '../../../helper/button'
import history from '../../../../routers/history'
 


class OrderHeader extends Component {
    state = {
        checked:false,
        disabledButton:'disabled',
        disabledLink:'none'
    }
    changeCheckBox =()=>{
        this.setState(prevState => ({
            checked: !prevState.checked
          }));
     
        
    }
    componentDidUpdate(prevProps,prevState){
        if(this.state.checked!==prevState.checked){
              if(this.state.checked===true){
              this.setState({disabledButton:"",disabledLink:''})
          }else{
            this.setState({disabledButton:'disabled',disabledLink:'disabled'})
          }
        }
    }
    openOrder = () => {
        history.push('/open-orders')
    }
    makeOrder = () => {
        window.open("https://www.paypal.com/webapps/", '_blank')
    }
    render(){
        return(
            <div className="orderHeaderContainer">
                <div>
                    <DivWithSpans
                        firstData='Job name:'
                        secondData="dsd"
                    />
                     <DivWithSpans
                        firstData='Order #'
                        secondData="dsd"
                    />
                     <DivWithSpans
                        firstData='Ordered on:'
                        secondData="dsd"
                    />
                     <DivWithSpans
                        firstData='Pickup / Shipping Address:'
                        secondData="dsd"
                    />
                     <DivWithSpans
                        firstData='Amount: '
                        secondData="dsd"
                    />
                </div>
                <div>

                </div>
                <div className="orderMaking">
                    <input
                        checked={this.state.checked}
                        name='checkBox'
                        type="checkbox"
                        onChange={this.changeCheckBox}
                    />
                    <span>I agree to Quadriga USA Enterprises'</span>
                    <Link to='/term-of-use' >Term of Use</Link>
                    <Link to='/privacy-policy' >Privacy Policy</Link>
               
                    <Button
                        callback={this.makeOrder}
                        disabled={this.state.disabledLink }
                        name="Make order"
                    />
                    <Button
                        name= 'NET 15'
                        disabled={this.state.disabledButton}
                        callback={this.openOrder}
                    />
                </div>
            </div>
        )
    }
}
export default OrderHeader;