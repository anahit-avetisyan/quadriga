import React,{Component} from 'react'


class OrderBody extends Component {
    render(){
        return(
            <div className='orderBodyContainer'>
                <div className='firstDivOfOrder'>
                    <div>
                        <h6>LABEL SPECIFICATIONS</h6>
                    </div>
                    <div>
                        <h6>OPTIONS</h6>
                    </div>
                    <div>
                        <h6>ROLL PARAMETERS</h6>
                    </div>
                </div>
                <div className='secondDivOfOrder'>
                <div>
                    <h6>OTHER</h6>
                </div>
                <div>
                    <h6>SHIPPING</h6>
                </div>
                <div>
                    <h6>PRICE</h6>
                </div>
                </div>
                <div className='thirdDivOfOrder'>
                  {this.props.children}
                </div>
            </div>
        )
    }
}
export default OrderBody