import React ,{Component} from 'react'
import OrderHeader from './component/orderHeader'
import './orderReviewStyle.scss'
import OrderBody from './component/orderBody';
import {getRequest} from '../../helper/utils'
import ls from 'local-storage'
import Button from '../../helper/button'
import history from '../../../routers/history'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {SelectedProductId} from '../../../reducers/action'

class OrderReviewContainer extends Component {
    state= {
        allOrderData:{},
        gettingDataWithId:{},
    
    }
    // i will take data with id 
    componentDidMount = () => {
        getRequest('https://sparik.ml/api/v1/product/label_type/')
        .then(response => {
            this.setState({allOrderData:response.data})
            Object.values(response.data).forEach((data,index)=>{
                //Take id from local storage
               
                let id=this.props.match.params.id
                 if(data.id===id){
                     return this.setState({ gettingDataWithId : data });
                 }
            })
        })
    }
    changeParameters = () => {
        let id=this.props.match.params.id
        history.push(`/cart/order-edit/${id}`)
        let dataFromLs = ls.get("selectedStandardProduct") ? ls.get("selectedStandardProduct") : {}
        Object.values(dataFromLs).forEach((data,index)=>{
            //Take id from local storage
            if(data.id===parseInt(id)){
                let selectedData={
                    id:data.id,
                    name:data.name
                }
                ls.set('selectedData',selectedData)
                this.props.SelectedProductId(ls.get('selectedData').id)
            }
        })
    }

    render(){
        return(
            <div className='orderReviewContainer'>
            <h1>Order Review</h1>
            <div className='orderReviewContent'>
                <OrderHeader 
                />
                <OrderBody>
                    <Button
                        name = 'Change Parameters'
                        callback = {this.changeParameters}

                    />
                </OrderBody>
            </div>

        </div>
        )
    }
}
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            SelectedProductId
        },
        dispatch
    );
};

export default connect(null,mapDispatchToProps)(OrderReviewContainer);
