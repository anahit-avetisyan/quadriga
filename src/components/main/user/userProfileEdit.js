import React,{Component} from 'react'
import MainModal from '../../helper/mainModal.js.js'
import Input from '../../helper/input'
import './userStyle.scss'
// import Button from '../../helper/button';
// import {request} from '../../helper/utils';
import {getRequest} from '../../helper/utils';
import Files from 'react-files';
import MultipleSelectInput from '../../helper/multipleSelectInput';
import Data from '../../helper/productData'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {UserInformation} from "../../../reducers/action"
import { FaTrashAlt } from "react-icons/fa"
// import history from '../../../routers/history';

class UserProfileEdit extends Component {
    state = {
        mainUserData:{},
        value : '',
        files:{},
        newData:[],
        changedData:{}
    }
    componentDidMount=()=>{
        const {userInformation} = this.props
        this.setState({mainUserData:userInformation})
        let url='sss'
        getRequest(url)
        .then(response => {
         this.setState({mainUserData:response})
         })
        .catch(error => (error));
        //for tasting
        
    }
    userData={
        companyName:'QuadrigaUSA',
        contactName:'Mher Mehrabyan',
        loginEmail:'mher@quadrigausa.com',
        additionalEmail:'mher.m@quadrigausa.com',
        phoneNumber:'8189677466',
        companyAddress:'28410 WITHERSPOON PKWY',
        state:'VALENCIA, CA',
        website:'',
        password:'******',
        sellersPermit:"",
        zip:"91355",

    }
    removeAdditionalMale = (toggle) =>{
        
        // let url = "aaaa"
        // let data = {
        //     additionalEmail:this.state.additionalEmail  ? this.state.additionalEmail  : this.userData.additionalEmail
        // }
        // request(url,'DELETE',data)
        // .then(response => {
        //     this.setState({mainUserData:response})
        // })
        // .catch(error => error);
          
        delete this.userData.additionalEmail
        toggle()
        this.setState({mainUserData:this.userData})
    }
    
    handleChange = (value,name) => {
        let changedData = Object.assign({},this.state.newData)
        changedData[name]=value
        this.setState({newData: changedData })

       
    }
    editData=(toggle)=>{
        const {mainUserData} = this.state
        let changedUserData={
            companyName:this.state.newData.companyName ? this.state.newData.companyName: mainUserData.companyName,
            contactName:this.state.newData.contactName ? this.state.newData.contactName : mainUserData.contactName,
            loginEmail:this.state.newData.loginEmail ? this.state.newData.loginEmail  : mainUserData.loginEmail,
            additionalEmail:this.state.newData.additionalEmail  ? this.state.newData.additionalEmail  : mainUserData.additionalEmail,
            phoneNumber:this.state.newData.phoneNumber ? this.state.newData.phoneNumber : mainUserData.phoneNumber,
            companyAddress:this.state.newData.companyAddress  ? this.state.newData.companyAddress : mainUserData.companyAddress,
            website:this.state.newData.website  ? this.state.newData.website  : mainUserData.website,
            password:this.state.newData.password  ? this.state.newData.password  : mainUserData.password,
            sellersPermit:this.state.newData.sellersPermit  ? this.state.newData.sellersPermit  : mainUserData.sellersPermit,
            notesAboutPermition: this.state.newData.notesAboutPermition  ? this.state.newData.notesAboutPermition  : mainUserData.notesAboutPermition,
        }
        this.props.UserInformation(changedUserData)
        this.setState({changedData:changedUserData,mainUserData:changedUserData})
        toggle()
    }
    onFilesChange = (file) => {
        this.setState({
          files:file
        })
      }
    
      onFilesError = (error, file) => {
        console.log('error code ' + error.code + ': ' + error.message)
      }
    
    filesRemoveOne = () => {
        this.refs.files.removeFiles()
    }
    onClickHandler = () => {
        const formData = new FormData()
        Object.keys(this.state.files).forEach((key) => {
          const file = this.state.files[key]
          formData.append(key, new Blob([file], { type: file.type }), file.name || 'file')
        })
    
        // axios.post(`/files`, formData)
        // .then(response => {
        //     console.log(response,"sssssss")
        //      window.alert(`${this.state.files.length} files uploaded succesfully!`)}
        // )
        // .catch(err => window.alert('Error uploading files :('))
        // fetch(url, {
        //     method: method,
        //     body: data,
        //     headers: {
        //         'Accept': 'application/json',
        //         ...authHeader(authToken)
        //     }
        // })

        

    } 
    saveChanges = () => {
        let newData = this.state.changedData
        this.setState({mainUserData:newData})
   
          //i get need data from state and send for editing
    
            //request(url,method,data)
        //     .then(response => {
        //      this.setState({mainUserData:response})
                
        //     })
        //     .catch(error => error);
    }
    cancelData = (toggle) => {
        this.setState({newData:{},files:{}})
        toggle()
    }
    style = {
        control: (base, state) => ({
          ...base,
          background: "#e4e4e4",
          margin:"0",
          border: state.isFocused ? 0 : 0,
          // This line disable the blue border
          boxShadow: state.isFocused ? 0 : 0,
          "&:hover": {
            border: state.isFocused ? 0 : 0
          }
        }),
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
              ...styles,
              backgroundColor:isSelected ? '#71A9B0':  "#e4e4e4",
              margin:"0",
              color: 'black',
              cursor:'pointer',
              "&:hover": {
                backgroundColor: '#71A9B0',
                color:"white",
              } 
            };
          },
          menu: (styles) => {
            return {
              ...styles,
              backgroundColor:"#e4e4e4",
              marginTop:"3px",
              paddingTop:"0px",
              width:"calc(100% + 3px)",
              color: 'black',
              cursor:'pointer',
              border:"2px solid #B1B1B1",
               borderTop:'none',
               borderRadius:"0",
               transform: "translate3d(-1.5px, 0px, 0px)"


                
            };
          },
      };
    render(){
        const {mainUserData,files} = this.state
        return(
            <div className='userProfileEditContainer'>
                <p className="mainUserInformationHeader" >Your Account: Profile</p>
                <div className='editUserInformationsContainer'>
                    <div>
                        <p className="userData">Company Name</p>
                        <div className="dataAmdEditContainer">
                            <span>{mainUserData.companyName}</span>
                            < MainModal
                                toggleClassName='modalToggle'
                                editData={this.editData}
                                toggleName='Edit'
                                save="Save"
                                cancel="Cancel"
                                mainDivClassName='editButtonContainer'
                                className='editModalContainer'
                                cancelButton="cancelButton"
                                saveButtonClassName="saveButton"
                                cancelData = {this.cancelData}
                                backdrop={"static"}
                       
                            >
                                <div>
                                    <p className="editModalHeader">
                                    Company Name
                                    </p>
                                    <Input   
                                        // placeholder = {mainUserData.contactName} 
                                        className="editDataForm"
                                        inputName="companyName"
                                        type = 'text'
                                        defaultState = {mainUserData.companyName}
                                        changeCallback = { this.handleChange}
                                    />
                                </div>
                            </ MainModal>
                        </div>
                    </div>
                    <div >
                        <p className="userData">Contact Person</p>
                        <div className="dataAmdEditContainer">
                            <span>{mainUserData.contactName}</span>
                            < MainModal
                                toggleClassName='modalToggle'
                                editData={this.editData}
                                toggleName='Edit'
                                save="Save"
                                cancel="Cancel"
                                mainDivClassName='editButtonContainer'
                                className='editModalContainer'
                                cancelButton="cancelButton"
                                saveButtonClassName="saveButton"
                                cancelData = {this.cancelData}
                                backdrop={"static"}
                            >
                                <div>
                                    <p className="editModalHeader">
                                    Contact Person
                                    </p>
                                    <Input
                                    className="editDataForm"
                                    inputName="contactName"
                                    type = 'text'
                                    defaultState = {mainUserData.contactName}
                                    changeCallback = { this.handleChange}  
                                 
                                />
                                     
                                </div>
                                
                            </ MainModal>

                        </div>
                    </div>
                    <div>
                     <p className="userData">Phone Number</p>
                         <div className="dataAmdEditContainer">
                             <span>{mainUserData.phoneNumber}</span>
                            < MainModal
                               toggleClassName='modalToggle'
                               editData={this.editData}
                               toggleName='Edit'
                               save="Save"
                               cancel="Cancel"
                               mainDivClassName='editButtonContainer'
                               className='editModalContainer'
                               cancelButton="cancelButton"
                               saveButtonClassName="saveButton"
                               cancelData = {this.cancelData}
                               backdrop={"static"}
                            >
                                 <div>
                                    <p className="editModalHeader">
                                        Phone Number
                                    </p>
                                    <Input
                                        className="editDataForm" 
                                        inputName="phoneNumber"
                                        type = 'text'
                                        defaultState = {mainUserData.phoneNumber}
                                        changeCallback = { this.handleChange}  
                                    />
                                    
                                </div>
                           
                            </ MainModal>
                        </div>
                    </div>
                    <div>
                        <p className="userData">Web Site</p>
                        <div className="dataAmdEditContainer">
                            <span>{mainUserData.website && mainUserData.website.length >0 ? mainUserData.website : "http://"}</span>
                            < MainModal
                                toggleClassName='modalToggle'
                                editData={this.editData}
                                toggleName='Edit'
                                save="Save"
                                cancel="Cancel"
                                mainDivClassName='editButtonContainer'
                                className='editModalContainer'
                                cancelButton="cancelButton"
                                saveButtonClassName="saveButton"
                                cancelData = {this.cancelData}
                                backdrop={"static"}
                            >
                                <div>
                                    <p className="editModalHeader">
                                        Web Site
                                    </p>
                                    <Input
                                        className="editDataForm" 
                                        inputName="website"
                                        type = 'text'
                                        defaultState = {mainUserData.website && mainUserData.website.length >0 ? mainUserData.website : "http://"}
                                        changeCallback = { this.handleChange}
                                    />
                                     
                                </div>
                            </ MainModal>
                        </div>
                    </div>
                    <div>
                        <p className="userData">Login Email</p>
                        <div className="dataAmdEditContainer">
                            <span>{mainUserData.loginEmail}</span>
                        </div>
                    </div>
                    <div>
                        <p className="userData">Additional Email</p>
                        <div className="dataAmdEditContainer">
                            <span>{mainUserData.additionalEmail}</span>
                            <div className='modalsContainer'>
                                < MainModal
                                    toggleClassName='modalToggle'
                                    editData={this.editData}
                                    toggleName='Edit'
                                    save="Save"
                                    cancel="Cancel"
                                    mainDivClassName='editButtonContainer'
                                    className='editModalContainer'
                                    cancelButton="cancelButton"
                                    saveButtonClassName="saveButton"
                                    cancelData = {this.cancelData}
                                    backdrop={"static"}
                                >
                                    <div>
                                        <p className="editModalHeader">
                                        Additional Email
                                        </p>
                                        <Input
                                            className="editDataForm" 
                                            inputName="additionalEmail" 
                                            type = 'text'
                                            defaultState = {mainUserData.additionalEmail}
                                            changeCallback = { this.handleChange}  
                                        
                                        />
                                </div>
                                    
                                </ MainModal>
                                 <span className="divider">/</span>
                                 < MainModal
                                    toggleClassName='modalToggle'
                                    editData={ this.removeAdditionalMale }
                                    toggleName="Remove"
                                    save="Delete"
                                    cancel="Cancel"
                                    mainDivClassName='editButtonContainer'
                                    className='editModalContainer'
                                    cancelButton="cancelButton"
                                    saveButtonClassName="saveButton"
                                    cancelData = {this.cancelData}
                                    backdrop={"static"}
                                >
                                    <div>
                                        <p className="editModalHeader">
                                        Do You Want To Delete This Additional Email?
                                        </p>
                                        
                                    </div>
                                </ MainModal>
                                <span style={{cursor:'pointer'}} onClick = {()=>this.removeAdditionalMale(mainUserData.additionalEmail)}> </span>   
                            </div>
                        </div>
                    </div>
                    
                    <div >
                        <p className="userData">Company Address</p>
                         <div className="dataAmdEditContainer">
                             <span>{mainUserData.companyAddress}</span>
                            < MainModal 
                                toggleClassName='modalToggle'
                                editData={this.editData}
                                toggleName='Edit'
                                save="Save"
                                cancel="Cancel"
                                mainDivClassName='editButtonContainer '
                                className='editModalContainer addresModal'
                                cancelButton="cancelButton"
                                saveButtonClassName="saveButton"
                                cancelData = {this.cancelData}
                                backdrop={"static"}
                            >
                                <div>
                                    <p className="editModalHeader">
                                    Company Address
                                    </p>
                                    <div className="adressContainer">
                                        <span className="addressFieldsHeader">Street address</span>
                                        <Input 
                                            className="editDataForm" 
                                            inputName="companyAddress" 
                                            type = 'text'
                                            defaultState = {mainUserData.companyAddress}
                                            changeCallback = { this.handleChange}  
                                            
                                        />
                                        <Input 
                                            className="editDataForm" 
                                            inputName="companyAddress" 
                                            type = 'text'
                                            defaultState = {mainUserData.companyAddress}
                                            changeCallback = { this.handleChange}  
                                            
                                        />
                                        <span className="addressFieldsHeader">City</span>
                                        <MultipleSelectInput
                                            formClassName = 'modalInputClassName'
                                            selectName = "country"
                                            multiple = {false}
                                            productData = {Data} 
                                            styles={this.style}
                                            handleClick = {(value,name)=>this.props.addData(value,name)}
                                        />
                                        <span className="addressFieldsHeader">State / Province / Region</span>
                                        <MultipleSelectInput
                                            formClassName = 'modalInputClassName'
                                            selectName = "state"
                                            multiple = {false}
                                            styles={this.style}
                                            productData = {Data} 
                                            handleClick = {(value,name)=>this.props.addData(value,name)}
                                        />
                                        <span className="addressFieldsHeader">ZIP</span>
                                        <Input 
                                            className="editDataForm" 
                                            inputName="companyAddress" 
                                            type = 'text'
                                            defaultState = {mainUserData.zip}
                                            changeCallback = { this.handleChange}  
                                            
                                        />
                                        <span className="addressFieldsHeader">Phone Number</span>
                                        <Input 
                                            className="editDataForm" 
                                            inputName="companyAddress" 
                                            type = 'text'
                                            defaultState = {mainUserData.phoneNumber}
                                            changeCallback = { this.handleChange}  
                                            
                                        />
                                    </div>

                                </div>
                            </ MainModal>
                        </div>
                    </div>
                   
                    <div>
                        <p className="userData">Password</p>
                        <div className="dataAmdEditContainer">
                            <span>{mainUserData.Changepassword}</span>
                            < MainModal
                               toggleClassName='modalToggle'
                               editData={this.editData}
                               toggleName='Edit'
                               save="Save"
                               cancel="Cancel"
                               mainDivClassName='editButtonContainer'
                               className='editModalContainer '
                               cancelButton="cancelButton"
                               saveButtonClassName="saveButton"
                               cancelData = {this.cancelData}
                               backdrop={"static"}
                            >
                                <div>
                                    <p className="editModalHeader">
                                        Password
                                    </p>
                                    <Input
                                        className="editDataForm" 
                                        inputName="changepassword"
                                        type = 'text'
                                        defaultState = {mainUserData.Changepassword}
                                        changeCallback = { this.handleChange}
                                    />
                                     
                                </div>
                            </ MainModal>
                        </div>
                    </div>
                    <div>
                        <p className="userData">Seller's Permit</p>
                        <div className="dataAmdEditContainer">
                        {files.length > 0
                                            ? <div className='files-list'>
                                            <div>{this.state.files.map((file,index) =>
                                                <div className='files-list-item' key={file.id}>
                                                <div className='file-list-item-content'>
                                                    
                                                    <div  className='files-list-item-name files-list-item-content-item-1'>
                                                    <span >{file.name}</span></div>
                                                    
                                                </div>
                                                </div>
                                            )}</div>
                                            </div>
                                            : <span></span>
                                        }
                            < MainModal
                                editData={this.editData}
                                toggleClassName='modalToggle'
                                toggleName="Update Seller's Permit"
                                save="save"
                                cancel='cancel'
                                mainDivClassName='editButtonContainer'
                                className='editModalContainer '
                                cancelButton="cancelButton"
                                saveButtonClassName="saveButton"
                                cancelData = {this.cancelData}
                                backdrop={"static"}
                            >
                                <div className="sellersPermitContainer">
                                    <p className="editModalHeader">
                                    Seller's Permit
                                    </p>
                                    <textarea name='notesAboutPermition' className="notesAboutPermition" defaultValue={mainUserData.notesAboutPermition} onChange={(event)=>this.handleChange(event.target.value,event.target.name)}></textarea>
                                    
                                    <div>
                                        <div className='containerFile'>
                                        {files.length > 0
                                            ? <div className='files-list'>
                                            <div>{this.state.files.map((file,index) =>
                                                <div className='files-list-item' key={file.id}>
                                                <div className='file-list-item-content'>
                                                    
                                                    <div  className='files-list-item-name files-list-item-content-item-1'>
                                                    <span className='file-list-item-content-name'>{file.name}</span></div>
                                                    
                                                </div>
                                                </div>
                                            )}</div>
                                            </div>
                                            : null
                                        }
                                        <Files
                                            ref='files'
                                            className='files-dropzone files-name'
                                            // style={{ height: '100px' }}
                                            multiple={false}
                                            onChange={this.onFilesChange}
                                            onError={this.onFilesError}
                                            maxFiles={10000000}
                                            maxFileSize={10000000}
                                            minFileSize={0}
                                            clickable
                                        >
                                            Choose File
                                        </Files>
                                    
                                        {files.length > 0 ? <div className='files-dropzone' onClick={()=>this.filesRemoveOne(files)}><FaTrashAlt className="removeIcon"/></div> : null}
                                        </div>
                                        {/* <button onClick={this.filesUpload}>Upload</button> */}
                                        
                                        {/* <button onClick={this.filesRemoveAll}>Remove All Files</button> */}
                                        </div>
                                </div>
                            </ MainModal>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    userInformation:state.userInformation ? state.userInformation : {}
   
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            UserInformation
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(UserProfileEdit);