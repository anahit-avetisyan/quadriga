import React,{Component} from 'react'
import Input from '../../helper/input'
import './userStyle.scss'
import Button from '../../helper/button';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {UserInformation} from "../../../reducers/action"
import history from '../../../routers/history';

class UserInformationEdit extends Component {
    state = {
        mainUserData:{},
        newData:[]
       
    }
    componentDidMount=()=>{
        // let url='sss'
        // getRequest(url)
        // .then(response => {
        //  this.setState({mainUserData:response})
        //  })
        // .catch(error => (error));
        //for tasting
         const {userInformation} = this.props
        this.setState({mainUserData:userInformation})
    }
    
    handleChange = (value,name) => {
        let changedData = Object.assign({},this.state.newData)
        changedData[name]=value
        this.setState({newData: changedData })
    }
    saveChanges = () => {
        let changedUserData={
            companyName:this.state.newData.companyName ? this.state.newData.companyName: this.state.mainUserData.companyName,
            contactName:this.state.newData.contactName ? this.state.newData.contactName : this.state.mainUserData.contactName,
            loginEmail:this.state.newData.loginEmail ? this.state.newData.loginEmail  : this.state.mainUserData.loginEmail,
            additionalEmail:this.state.newData.additionalEmail  ? this.state.newData.additionalEmail  : this.state.mainUserData.additionalEmail,
            phoneNumber:this.state.newData.phoneNumber ? this.state.newData.phoneNumber : this.state.mainUserData.phoneNumber,
            companyAddress:this.state.newData.companyAddress  ? this.state.newData.companyAddress : this.state.mainUserData.companyAddress,
            website:this.state.newData.website  ? this.state.newData.website  : this.state.mainUserData.website,
            password:this.state.newData.password  ? this.state.newData.password  : this.state.mainUserData.password,
            sellersPermit:this.state.newData.sellersPermit  ? this.state.newData.sellersPermit  : this.state.mainUserData.sellersPermit,
        }
        this.props.UserInformation(changedUserData)
        history.push('/user-profile')
    }
    render(){
        const {mainUserData} = this.state
        return(
            <div className='userProfileEditContainer'>
            <p className="mainUserInformationHeader" >Your Account: Profile</p>
            <div className='editUserInformationsContainer'>
                <div>
                    <p className="userData">
                        Company Name 
                    </p>
                    <Input
                        className="editInputData"
                        inputName="companyName"
                        type = 'text'
                        defaultState = {mainUserData.companyName}
                        changeCallback = { this.handleChange}
                    />            
                </div>
                <div>
                    <p className="userData">
                        Contact Person
                    </p>
                    <Input
                        className="editInputData"
                        inputName="contactName"
                        type = 'text'
                        defaultState = {mainUserData.contactName}
                        changeCallback = { this.handleChange}  
                    />           
                </div>
                <div>
                    <p className="userData">
                        Phone Number
                    </p>
                    <Input
                        className="editInputData" 
                        inputName="phoneNumber"
                        type = 'text'
                        defaultState = {mainUserData.phoneNumber}
                        changeCallback = { this.handleChange}  
                    />     
                </div>
                <div>
                    <p className="userData">
                        Web Site
                    </p>
                    <Input
                        className="editInputData" 
                        inputName="website"
                        type = 'text'
                        defaultState = {mainUserData.website && mainUserData.website.length >0 ? mainUserData.website : "http://"}
                        changeCallback = { this.handleChange}
                    />        
                </div>
                <div>
                    <p className="userData">Login Email</p>
                    <Input
                        className="editInputData" 
                        inputName="additionalEmail"
                        type = 'text'
                        defaultState = {mainUserData.additionalEmail}
                        changeCallback = { this.handleChange}
                    />   
                </div>
                <div>
                    <p className="userData">
                        Additional Email
                    </p>
                    <Input
                        className="editInputData" 
                        inputName="additionalEmail" 
                        type = 'text'
                        defaultState = {mainUserData.additionalEmail}
                        changeCallback = { this.handleChange} 
                    />
                </div>
                <div className="adressContainer">
                    <p className="userData">Street address</p>
                    <Input 
                        className="editInputData" 
                        inputName="companyAddress" 
                        type = 'text'
                        defaultState = {mainUserData.companyAddress}
                        changeCallback = { this.handleChange}   
                    />
                </div>
                <div>
                    <p className="userData">
                        Password
                    </p>
                    <Input
                        className="editInputData" 
                        inputName="changepassword"
                        type = 'text'
                        defaultState = {mainUserData.Changepassword}
                        changeCallback = { this.handleChange}
                    />     
                </div>
                <div>
                    <p className="userData">Seller's Permit</p>
                    <Input
                        className="editInputData" 
                        inputName="changepassword"
                        type = 'text'
                        defaultState = {mainUserData.sellersPermit}
                        changeCallback = { this.handleChange}
                    /> 
                </div>
                <div>
                    <Button
                        name="Save"
                        className="saveButton"
                        callback={this.saveChanges}
                    />
                </div>
            </div>
        </div> 
        )
    }
}
function mapStateToProps(state) {
    return {
    state,
    userInformation:state.userInformation ? state.userInformation : {}
   
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            UserInformation
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(UserInformationEdit);