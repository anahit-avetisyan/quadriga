import React,{Component} from 'react';
import './userStyle.scss'
import Button from '../../helper/button';
import history from '../../../routers/history'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {UserInformation} from "../../../reducers/action"

class UserProfileMain extends Component {
    
    userData={
        companyName:'QuadrigaUSA',
        contactName:'Mher Mehrabyan',
        loginEmail:'mher@quadrigausa.com',
        additionalEmail:'mher.m@quadrigausa.com',
        phoneNumber:'8189677466',
        companyAddress:'28410 WITHERSPOON PKWY, VALENCIA, CA, 91355',
        website:'http://',
        password:'******',
        sellersPermit:""
    }
    componentDidMount = () => {
        const {userInformation} = this.props
        if(Object.values(userInformation).length===0){
            this.props.UserInformation(this.userData)
        }
        
    }
    editUserData = () => {
        history.push('/user-profile/edit' )
    }
    editUserNewData = () => {
        history.push('/user-profile/edit-information' )
    }
    render(){
        const {userInformation} = this.props
        return(
            <div className="profileMainContainer">
                <p className="mainUserInformationHeader">Your Account: Profile</p>
                <div className="profileMainInformation">
                    <div className="firstInformationContent">
                        <div className="userInformationContent">
                            <span>Company Name</span>
                            <span>{userInformation.companyName}</span>
                        </div>
                        <div className="userInformationContent">
                            <span >Contact Person</span>
                            <span>{userInformation.contactName}</span>
                        </div>
                        <div className="userInformationContent">
                            <span>Phone Number</span>
                            <span>{userInformation.phoneNumber}</span>
                        </div>
                    </div>
                    <div className="secondInformationContent">
                        <div className="userInformationContent">
                            <span>Web Site</span>
                            <span>{userInformation.website}</span>
                        </div>
                        <div className="userInformationContent">
                            <span>Login Email</span>
                            <span>{userInformation.loginEmail}</span>
                        </div>
                        <div className="userInformationContent">
                            <span>Additional Email  (optional)</span>
                            <span>{userInformation.additionalEmail}</span>
                        </div>
                    </div>
                    <div className="lastInformationContent">
                        <div className="userInformationContent">
                            <span>Company Adress</span>
                            <span>{userInformation.companyAddress}</span>
                        </div>
                        <div className="userInformationContent">
                            <span>Password</span>
                            <span>{userInformation.password}</span>
                        </div>
                    </div>
                </div>
                <div className="editButtonContainer">
                    <Button
                        name="Edit"
                        className="userProfileEditButton"
                        callback={this.editUserData}
                    />
                    {/* its must delete */}
                    {/* <Button
                        name="Edit(2)"
                        className="userProfileEditButton"
                        callback={this.editUserNewData}
                    /> */}
                </div>
            </div>
        )
    }
};
function mapStateToProps(state) {
    return {
    state,
    userInformation:state.userInformation ? state.userInformation : {}
   
    };
};
function mapDispatchToProps(dispatch) { 
    return bindActionCreators(
        {
            UserInformation
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(UserProfileMain);