import React,{Component} from 'react'
import './privecyPolicy.scss'


class PrivecyPolicy extends Component {
    render(){
        return(
            <div className="privacyPolicyContainer">
                <p className="privacyPolicyHeader"> Privecy Policy</p>
                <div>
                    <p className="maintext">
                    Quadriga is committed to protecting the privacy of its customers. Your privacy is important to us. 
                    Quadriga’s goal is to provide you with a personalized online experience that provides you with the 
                    information, resources, and services that are most relevant and helpful to you. This Privacy Policy 
                    has been written to describe the conditions under which this web site is being made available to you. 
                    The Privacy Policy discusses, among other things, how data obtained during your visit to this web site 
                    may be collected and used. Quadriga strongly recommend that you read the Privacy Policy carefully. 
                    By using this web site, you agree to be bound by the terms of this Privacy Policy. If you do not accept 
                    the terms of the Privacy Policy, you are directed to discontinue accessing or otherwise using the web 
                    site or any materials obtained from it. If you are dissatisfied with the web site, by all means contact
                     Quadriga’s Customer Service for assistance; otherwise, your only recourse is to disconnect from this 
                     site and refrain from visiting the site in the future.
                    </p>

                    <p className="textHeader">Sites Covered by this Privacy Policy</p>
                    <p  className="maintext">This Privacy Policy applies to all the Quadriga USA
                     Enterprises-maintained web sites, domains, information portals, and registries.</p>
                </div>
            </div>
        )
    }
}
export default PrivecyPolicy;