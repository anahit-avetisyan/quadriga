import React,{Component} from 'react'
import OrderFilter from './components/orderFilter';
import './openOrdersStyle.scss'
import OpenOrderDetail from './components/openOrderDetail';
import ls from 'local-storage'
import history from '../../../routers/history'

class OpenOrdersContainer extends Component {
    state={
        orderDataFromLs:{},
        prices: [],
    }
    data=[
        {   
            value:'30',
            jobName:'aa',
            date:'8/29/2019'
        },
        {   
            value:'60',
            jobName:'dd',
            date:'7/29/2019'
        },
        {   
            value:'100',
            jobName:'as',
            date:'8/24/2019'
        },
        {
            value:'50',
            jobName:'ag',
            date:'8/26/2019'
        }
    ]

    
    componentDidMount = () => {
       let lsData=ls.get('selectedProduct') ? ls.get('selectedProduct') : {}
       this.setState({orderDataFromLs:this.data,prices:lsData})

    }
    sortData = (value,name) =>{
        const { orderDataFromLs } = this.state;
        console.log(orderDataFromLs,'lll',value)
        
        if(value==='Job name'){
        this.setState({orderDataFromLs: Object.values(orderDataFromLs).sort((a,b)=>(a.jobName>b.jobName)?1:-1)})
        }
        if(value==='Order Number'){
            this.setState({orderDataFromLs: Object.values(orderDataFromLs).sort((a,b)=>(a.value>b.value )?1:-1)})
            }
        if(value==='Status'){
            this.setState({orderDataFromLs: Object.values(orderDataFromLs).sort((a,b)=>(a.id>b.id)?1:-1)})
        }
        if(value==="Order Date"){
            this.setState({orderDataFromLs: Object.values(orderDataFromLs).sort((a,b)=>(a.date>b.date)?1:-1)})
            }
     
    }
    handleChangeLink = (id) => {
         //must set id in local storage
         history.push(`/order-details/${id}` )
    }
    render(){
        const {orderDataFromLs} = this.state
     
        return(
            <div className='openOrderContainer'>
            <p className='headerOpenOrders'>In process</p>
            <OrderFilter
                name='sortBy'
                filterData={this.sortData}
            />
            {!orderDataFromLs ? [] :Object.values(orderDataFromLs).map((values,index)=>{
                    return(
                    <OpenOrderDetail 
                        key={index}
                        handleChangeLink={this.handleChangeLink}
                        buttonName='Order Details'
                        data={values}
                        index={index}
                    />
                    )
            })}
            
        </div>
        )
    }
}
export default OpenOrdersContainer