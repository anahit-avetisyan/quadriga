import React,{Component} from 'react';
import Button from '../../../helper/button';
import { Progress } from 'reactstrap';



class OpenOrderDetail extends Component {
    state ={
        progressValue:'',
        value:'60',
        className:''
    }
    componentDidMount = () => {
        const {data}=this.props
        if(data.value<50){
            this.setState({className:'progressLess50'})
        }
        if(data.value>=50){
            this.setState({className:'progressMore50'})
        }
        if(data.value>=100){
            this.setState({className:'progressEqual100'})  
        }
    }
    componentDidUpdate = (prevProps,prevState) =>{
        if(this.props.data!==prevProps.data){
            const {data}=this.props
            if(data.value<50){
                this.setState({className:'progressLess50'})
            }
            if(data.value>=50){
                this.setState({className:'progressMore50'})
            }
            if(data.value>=100){
                this.setState({className:'progressEqual100'})  
            }
        }
    }
    render(){
        const {data}=this.props
        return(
            <div key = {this.props.index} className='openOrderContent'>
                <div className='productInformation'>
                    <span>QUADRIGAUSA</span> 
                    <div className='line'></div>
                    <span>Job name: {data.jobName}</span>
                    <div className='line'></div>
                    <span>Order #: Q-011906100001</span>
                    <div className='line'></div>
                    <span>Ordered on: {data.date}</span>
                     
                </div>
                <div className='progressContainer'>
         
                    <Progress className={this.state.className} value={data.value} max={100}>{ data.value>30 ? 'Loading ':null}{data.value} % </Progress>
                    <Button 
                        className='changeButton'
                        name={this.props.buttonName}
                        callback={()=>this.props.handleChangeLink(data.id)}    
                    />
                </div>
                
            </div>
        )
    }
}
export default OpenOrderDetail;