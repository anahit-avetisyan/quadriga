import React ,{Component}  from 'react'

 

class OrderFilter extends Component {
    state={
        value:'aaaaaaa'
    }
    data=['Order last update','Order Number','Customer','Order Date','Status','Price','Job name']
  
    render(){
        return(
            <div className='orderFilterField'>
                <span className='headerForFilter'>Orders</span>
                <div className='filterInputContainer'>
                    
                    <form onSubmit={this.handleSubmit} className={this.props.formClassName}>
                        <label>
                        Sort by  
                        <select name={this.props.selectName}  multiple={this.props.multiple} className = {this.props.selectClassName} onChange={event=>this.props.filterData(event.target.value,event.target.name)}  > 
                        
                        { this.data.map((data,index) => {          
                                    return( 
                                
                            <option key = {index} value={data}>{data}</option>
                        )})
                        }   
                        </select>
                        </label>
                    </form>
                </div>
            </div>
        )
    }
}
export default OrderFilter;