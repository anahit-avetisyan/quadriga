import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
 

class MainModal  extends React.Component {
    state = {
        modal: false
    };
 

  toggle= () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
   
  render() {
   
    return (
      <div className={this.props.mainDivClassName}>
        <p style={{cursor:'pointer'}}className = {this.props.toggleClassName}  onClick={this.props.toggle? this.props.toggle :this.toggle}>{this.props.toggleName}</p>
        <Modal 
            backdrop={this.props.backdrop} 
            isOpen={this.props.toggle? this.props.modal :this.state.modal} 
            toggle={this.props.toggle? this.props.toggle :this.toggle} 
            className={this.props.className}   
            wrapClassName={this.props.wrapClassName}
            modalClassName={this.props.modalClassName}
            backdropClassName= {this.props.backdropClassName}
            contentClassName={this.props.contentClassName}
             
        >
          <ModalHeader toggle={this.props.toggle? this.props.toggle :this.toggle}>{this.props.modalTitle}</ModalHeader>
          <ModalBody>
              {this.props.children}
          </ModalBody>
          <ModalFooter>
            {this.props.cancel ?<Button  className='cancelButton' onClick={this.props.toggle? this.props.toggle :this.props.cancelData ? ()=>this.props.cancelData(this.props.toggle? this.props.toggle :this.toggle):this.toggle}>{this.props.cancel}</Button> : null}
            {this.props.save ?<Button   className={this.props.saveButtonClassName}   onClick={()=>this.props.editData(this.props.toggle? this.props.toggle :this.toggle)}>{this.props.save}</Button>:null}
   
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default MainModal;