import React, {Component} from 'react'


class Input extends Component {
    handleSubmit(e) {
        e.preventDefault();
    }
    render(){
        return(
            <form className ={this.props.className} onSubmit={this.handleSubmit}>
                <label>
                {this.props.name}
                    <input style={this.props.style} type={this.props.type? this.props.type : "text"}  ng-hide="true" autoComplete={this.props.autocomplete} name={this.props.inputName}  defaultValue={this.props.defaultState} placeholder={this.props.placeholder}   onChange={event => this.props.changeCallback(event.target.value,event.target.name)}  />
                <span>{this.props.error}</span>
                {this.props.nameAfterInput}
                </label>
            </form>
        )
    }
}
export default Input;