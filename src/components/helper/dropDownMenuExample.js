import React,{Component} from 'react'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';



class DropDownMenuExample extends Component {
     
    state = {
        dropdownOpen: false
    };
      
    toggle=()=>{
        this.setState(prevState => ({
          dropdownOpen: !prevState.dropdownOpen
        }));
    }
    
    onMouseEnter=()=> {
        this.setState({dropdownOpen: true});
    }
    
    //   onMouseLeave=()=> {
    //     this.setState({dropdownOpen: false});
    //   }
    
      render() {
        return (
          <Dropdown style={{backgroundColor:"white"}} className="d-inline-block" onMouseOver={this.onMouseEnter} onMouseLeave={this.onMouseLeave}   isOpen={this.state.dropdownOpen} toggle={this.toggle}>
            <DropdownToggle style={{backgroundColor:"white", color:'black',border:'none'}}>
              {this.props.title}
            </DropdownToggle>
            <DropdownMenu onMouseOver={this.onMouseEnter} >
            
          
              <DropdownItem>{this.props.data}</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        );
      }
}
export default DropDownMenuExample;