import React, {Component} from 'react'
import './selectWithInput.scss'
import { FaChevronDown } from "react-icons/fa";

class SelectWithInput extends Component {
    state = {
        display: true,
         
    }
    openSelectOption =()=>{
        if(this.title&&this.title.value){
            this.title.value=""  
        }
         this.title.value=""
        if (this.state.display) {
            // attach/remove event handler
            document.addEventListener('click', this.handleOutsideClick, false);
          } else {
            document.removeEventListener('click', this.handleOutsideClick, false);
          }
        this.setState(prevState => ({
            display: !prevState.display
          }));
   
    }
    handleOutsideClick=(e)=> {
        // ignore clicks on the component itself
        if(this.node){
        if (this.node!==undefined && this.node.contains(e.target)) {
          return;
        }
    }
        this.openSelectOption();
      }
    handleSubmit(e) {
        e.preventDefault();
    }
    render(){
        let display = this.state.display ? "none" : "flex";
        return(
            
            <div className='inputInsideSelectContainer' ref={node => { this.node = node; }}>
                <span className = 'header'>{this.props.name}</span>
                <div  className='selectDiv'   onClick={this.openSelectOption}>
                    <span style={{marginLeft:'1.5%'}}>{this.props.selectedData }</span>
                    <FaChevronDown className="iconDown "/>
                </div>
                <div style={{display:display}} className={this.props.inputInsideSelect}  >
                    <div className={this.props.inputInsideSelectFormContainer}>
                    {this.props.option ? <span style={{cursor:'pointer'}} onClick={()=>this.props.changeToNotImportent(this.openSelectOption)}>{this.props.option}</span>: null}
                <form className ={this.props.className} onSubmit={this.handleSubmit}>
                    <label>
                        
                        <input ref={(c) => this.title = c} defaultValue={this.props.defaultState} className={this.props.mainInputInsideSelect} onKeyDown={(event)=>this.props.onKeyPress(event.target.value,event.target.name,event,this.openSelectOption)}  name={this.props.inputName}  placeholder={this.props.placeholder} type={this.props.type} onChange={event => this.props.changeCallback(event.target.value,event.target.name)}  />
                        {this.props.nameAfterInput}
                    </label>
                 </form>
                    </div>
                   
                </div>
            </div>
        )
    }
}
export default SelectWithInput;