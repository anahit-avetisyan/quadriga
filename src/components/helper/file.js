import React,{Component} from 'react';
import MainModal from './mainModal.js'
import Files from 'react-files';

class File extends Component {
    render(){
        return(
                               
                <div>
                    <div className='containerFilesButton'>
                        <p className='header'>{this.props.header}</p>
                        <Files 
                            ref='files'
                            className='files-dropzone-list'
                            onChange={(files)=>this.props.onChange(files)}
                            onError={()=>this.props.onError()}
                            multiple={this.props.multiple}
                            maxFiles={ this.props.maxFiles}
                            maxFileSize={this.props.maxFileSize}
                            minFileSize={this.props.minFileSize}
                            clickable
                        >            
                        {this.props.buttonName}
                        </Files>
                    </div>
                    {this.props.files.length > 0
                        ? <div className='files-list'>
                            <div>{this.props.files.map((file,index) =>
                                <div className='files-list-item' key={file.id}>
                                    <div className='files-list-item-content'>
                                        <div  className='files-list-item-content-item files-list-item-content-item-1'>
                                            <span className='files-list-item-content-index'>{index+1}.</span>
                                            <p className='files-list-item-content-name'>{file.name}</p>
                                        </div>
                                        < MainModal
                                            mainDivClassName = "confirmation"
                                            className='deleteModal'
                                            modalTitle="CONFIRMATION"
                                            editData={(toggle,deletigFile=file)=>this.props.filesRemoveOne(toggle,deletigFile)}
                                            toggleName={this.props.removeName}
                                            save="YES"
                                            cancel="NO"
                                            saveButtonClassName="deleteButtonConformation"
                                        >
                                            <span className="textAboutConformation">{this.props.textAboutConformation}</span>
                                        </ MainModal>
                                        {/* <div onClick={()=>this.props.filesRemoveOne(file)}>{this.props.removeName}</div> */}
                                    </div>
                                </div>
                        )}</div>
                        </div>: null
                    }
            </div>
       
     

 
        )
    }
}
export default File;