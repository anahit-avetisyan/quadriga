import React,{Component, Fragment} from 'react'
import Select from 'react-select'
import "mdbreact/dist/css/mdb.css"


class MultipleSelectInput extends Component {
    state = {
        value : '',
      
    }
    render(){
      const {defaultValue} = this.props
        return(
          <Fragment>
                {/* {!this.props.productData ? null :
              <form onSubmit={this.handleSubmit} className={this.props.formClassName}>
                  <label>
                      {this.props.name}
                        <select   name={this.props.selectName}  multiple={this.props.multiple} className = {this.props.selectClassName}   onChange={event=>this.props.handleClick(event.target.value,event.target.name)}>  
                          <option defaultValue={defaultValue}  hidden  >{defaultValue}</option>
                            {!this.props.productData ? [] : Object.values(this.props.productData).map((data,index) => {          
                                return( 
                                    <option   key = {index} value={data.name?data.name:data}>{data.name?data.long_name:data}</option>
                            )})
                            }
                        </select>
                  </label>
            </form>
            }   */}
                <label className='multiselectSHeaderClassName'>{this.props.name}</label>
               <Select
                    // defaultValue={defaultValue ? {value:defaultValue,label:defaultValue}:{value:0,label:"Select..."}}
                   value={defaultValue ? {value:defaultValue,label:defaultValue}:{value:"",label:"Select..."}}
                    styles={this.props.styles} 
                    className={this.props.selectClassName}
                    name={this.props.selectName} 
                    onChange={(value,property)=>this.props.handleClick(value.value,property.name)}
                    options={
                      !this.props.productData ? [] : Object.values(this.props.productData).map((data,index) => {          
                        return( 
                        { value: data.long_name ? data.long_name:data, label: data.name ? data.long_name:data}
                    )})
                }
        
            /> 
          </Fragment>
        )
    }
}
export default MultipleSelectInput;