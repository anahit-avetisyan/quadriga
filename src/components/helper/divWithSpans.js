import React,{Component} from 'react'


class DivWithSpans extends Component {
    render(){
        return(
            <div className={this.props.divClassName}>
                <span className={this.props.firstSpanClassName}>{this.props.firstData}</span>
                <span className={this.props.secondSpanClassName}>{this.props.secondData}</span>
            </div>
        )
    }
}
export default DivWithSpans;