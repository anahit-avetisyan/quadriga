import React,{Component} from 'react';
import {UncontrolledTooltip} from 'reactstrap';
 

class Tooltip extends Component{
  state = {
    tooltipOpen: false
  };
  toggle=()=> {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }
     render(){
        return (
            <div className={this.props.divForTooltip}>
              <p>{this.props.name} <span  style={{backgroundColor:'grey', color:'white'}} className={this.props.tooltipClassName} href="#" id={'Tooltip-' + this.props.id}>{this.props.tooltip}</span></p>
              <UncontrolledTooltip style={{backgroundColor:'white', color:'black'}} placement={this.props.placement}  target={'Tooltip-' + this.props.id}>
                {this.props.tooltipData}
              </UncontrolledTooltip>
            </div>
          );
     }
 }
export default Tooltip;
 