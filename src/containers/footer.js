import React,{Component,Fragment} from 'react'
import FooterNavBar from '../components/footer/footerNavBar'
import '../components/footer/footerStyle.scss'
import Contacts from '../components/main/homePage/component/contacts';

class Footer extends Component {
    render(){
        return(
        <Fragment>
            <Contacts/>
            <FooterNavBar/>
        </Fragment>
        )
    }
}
export default Footer;