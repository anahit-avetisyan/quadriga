import React,{Component} from 'react'
import '../components/header/headerStyle.scss'
import HeaderContainer from '../components/header/headerContainer'


class Header extends Component {
    render(){
        return(
            <div className='headerContainer'>
                <HeaderContainer/>
            </div>
        )
    }
}
export default Header;