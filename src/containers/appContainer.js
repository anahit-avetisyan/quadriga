import React,{Component,Fragment} from 'react';
import Header from './header'
import Footer from './footer'
import Main from './main'


 class AppContainer extends Component{

   render(){
    const pathName = window.location.pathname
    
    return (
      <Fragment>
      {pathName==='/register-account'||pathName==='/signin-account'||pathName==='/forgot-password'?null:
      <div className="App">
        <Header/>
        <Main/>
        <Footer/>
      </div>}
      </Fragment>
    );
   }
}

export default AppContainer;