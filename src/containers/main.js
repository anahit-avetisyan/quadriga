import React, {Component} from 'react';
import '../components/main/mainStyle.scss'
import Routers from '../routers/routersForMain'
import SideBar from '../components/sideBar/sideBar'
import TopButton from '../assets/icons/topButton.png'
// import UpButton from '../assets/icons/upButton.png'

class Main extends Component {
    state = {
        intervalId: 0
    };
    scrollStep = () => {
        if (window.pageYOffset === 0) {
            clearInterval(this.state.intervalId);
        }
        window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
    }
    scrollToTop() {
        let intervalId = setInterval(this.scrollStep.bind(this), this.props.delayInMs);
        this.setState({ intervalId: intervalId });
    }
    render(){
        const pathName = window.location.pathname
      
        return(
            <div className="mainContainer">
                <div className='mainContent'>
                    <SideBar/>
                    <div className='routersContainer'>
                    <Routers/>  
                    </div>
                    <div className='upButtonContainer'>
                        {/* {pathName==='/' ?
                    <div  onClick={ () => { this.scrollToTop(); }} className='topButton'>
                        <img src={TopButton} alt='button'/>
                    </div> : pathName==='/products' ?
                    <div  onClick={ () => { this.scrollToTop(); }} className='upButton'>  
                        <img src={UpButton} alt='button' />
                    </div>:null
                        } */}
                        {pathName ==="/contact" ? null :
                    <div  onClick={ () => { this.scrollToTop(); }} className='topButton'>
                        <img src={TopButton} alt='button'/>
                    </div>
                        }
                </div>
                </div>
               
            </div>
        )
    }
}
export default Main;